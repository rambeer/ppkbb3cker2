<?php
/**
*
* acp_board [Russian]
*
* @package language
* @version $Id: ppkbb3cker_cssjs.php, v 1.000 2012-06-14 12:12:39 PPK Exp $
* @copyright (c) 2012 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_TRACKER_CSSJS'				=> 'CSS и JS файлы',
	'ACP_TRACKER_CSSJS_EXPLAIN'				=> 'Настройки подключаемых css и js файлов трекера. Указанные файлы будут для подключения к трекеру собираться в один файл.',

	'ACP_TRACKER_CSSJS_SETTINGS'				=> 'CSS и JS файлы',

	'CSSJS_COUNT' => 'Количество',
	'CSSJS_TYPE' => 'Тип файлов',
	'CSSJS_CSS' => 'Файлы CSS',
	'CSSJS_JS' => 'Файлы JS',
	'CSSJS_SORT_SUCCESS' => 'Порядок успешно изменён.',

	'PATH' => 'Путь',
	'PATH_EXPLAIN' => 'Путь до файла относительно корневой директории форума, в качестве пути также можно указывать URL ссылки',

	'VARIABLE' => 'Переменная',
	'VARIABLE_EXPLAIN' => 'При указании переменной файл будет подключаться только если в файл будет передана соответствующая переменная со значением не равным 0, если значение переменной будет равно -1, файл будет исключён из подключения, пример: css[var1]=1&amp;css[var2]=1, js[var1]=-1',

	'INCLUDE' => 'Подключение',
	'ORDER' => 'Порядок',

	'INCLUDE_EVERYWHERE' => 'Везде',
	'INCLUDE_NONADMIN' => 'НЕ в адм. разделе',
	'INCLUDE_ADMIN' => 'Только в адм. разделе',
));
?>
