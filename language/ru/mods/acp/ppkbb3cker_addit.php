<?php
/**
*
* acp_board [Russian]
*
* @package language
* @version $Id: ppkbb3cker_addit.php, v 1.000 2021/03/29 09:25:00 PPK Exp $
* @copyright (c) 2021 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_ADDIT_SETTINGS_EXPLAIN' => '',

	'ACP_SPOILER_SETTINGS' => 'Спойлер',

	'PPKBB_SUBJECT_TEXTLENGTH' => 'Максимальная длина поля Тема при добавлении сообщения',
	'PPKBB_SUBJECT_TEXTLENGTH_EXPLAIN' => 'Не более 250 символов',

	'TRACKER_HICONS_FIELDS'	=> 'Иконка сайта',
	'TRACKER_HICONS_FIELDS_EXPLAIN'	=> 'Абсолютный URL иконки для сайта, пример:<br />http://адрес_сайта/путь_до_иконки/favicon.ico',

	'PPKBB_DISABLE_FPQUOTE' => 'Запрет цитирования первого сообщения',
	'PPKBB_DISABLE_FPQUOTE_EXPLAIN' => 'Запретить цитировать первое сообщение темы если оно является сообщением на форуме-трекере и содержит торрент файл',

	'TRACKER_FORUMS_QR' => 'Форма быстрого ответа во всех форумах',
	'TRACKER_FORUMS_QR_ASIS' => 'оставить как есть',
	'TRACKER_FORUMS_QR_ON' => 'включить',
	'TRACKER_FORUMS_QR_OFF' => 'выключить',
	'TRACKER_FORUMS_QR_EXPLAIN' => 'Включить быстрый ответ во всех форумах',

	'PPKBB_FPEP_ENABLE' => 'Первое сообщение темы на всех страницах',
	'PPKBB_FPEP_ENABLE_EXPLAIN' => 'Включить на всех форумах, вручную - отображать в зависимости от опции в сообщении',
	'PPKBB_FPEP_ENABLE_ASIS' => 'оставить как есть',
	'PPKBB_FPEP_ENABLE_ON' => 'включить',
	'PPKBB_FPEP_ENABLE_OFF' => 'отключить',
	'PPKBB_FPEP_ENABLE_OPTION' => 'вручную',

	'TRACKER_SUBFORUMLIST' => 'Число колонок для списка подфорумов',
	'TRACKER_SUBFORUMLIST_EXPLAIN' => 'Установить число колонок для всех форумов в данное значение, 0 - отключить вывод подфорумов в колонки (список выводится в строку), пустая строка - не менять текущие значения',

	'PPKBB_MAX_MFU' => 'Мультизагрузка файлов',
	'PPKBB_MAX_MFU_EXPLAIN' => 'Максимальное количество файлов для одновременной загрузки (0 - без ограничений)',

	'PPKBB_CSSJS_CACHE' => 'Управление css и js файлами',
	'PPKBB_CSSJS_CACHE_MINIFICATION' => 'Минифицировать',
	'PPKBB_CSSJS_CACHE_COMPRESS' => 'Минифицировать и сжимать',
	'PPKBB_CSSJS_CACHE_EXPLAIN' => 'Две опции,
		<br /><strong>опция 1</strong> кэшировать css и js файлы,
		<br /><strong>опция 2</strong> не используется',

	'PPKBB_THANKS_ENABLE' => '"Спасибо" на торрентах',
	'PPKBB_THANKS_ENABLE_EXPLAIN' => 'Включить функцию "Спасибо" на торрентах',

	'SPOILER_DEPTH_LIMIT' => 'Максимальное количество вложенных спойлеров в сообщении',
	'SPOILER_DEPTH_LIMIT_EXPLAIN' => 'Максимальное количество вложенных в сообщение спойлеров. Введите 0 для снятия ограничений.',

	'SPOILER_OPTIONS' => 'Опции спойлера',
	'SPOILER_OPTIONS_EXPLAIN' => 'Семь опций,
		<br /><strong>опция 1</strong> ширина изображений при отображении в спойлере или вне его (в пикселях),
		<br /><strong>опция 2</strong> скрывать все изображения обрабатываемые спойлером,
		<br /><strong>опция 3</strong> скрывать все изображения в подписях пользователей обрабатываемые спойлером,
		<br /><strong>опция 4</strong> отображать для пользователей с отключённым javascript предупреждение о необходимости включения javascript для просмотра изображения,
		<br /><strong>опция 5</strong> метод открытия полноразмерных изображений,
		<br /><strong>опция 6</strong> отображать все изображения на одной странице в режиме галереи, иначе в режиме одиночных изображений (только если опция 5 равна <em>через prettyPhoto</em>),
		<br /><strong>опция 7</strong> стиль prettyPhoto (только если опция 5 равна <em>через prettyPhoto</em>)',
	'SPOILER_OPTIONS_PP' => 'через prettyPhoto',
	'SPOILER_OPTIONS_NEW' => 'в новом окне',
	'SPOILER_OPTIONS_CURRENT' => 'в текущем окне',

	'SPOILER_BANNED_IMGHOSTS' => 'Забаненные хостинги изображений для спойлера',
	'SPOILER_BANNED_IMGHOSTS_EXPLAIN' => 'Скрывать изображения обрабатываемые спойлером с указанных хостингов, пример:<br />/imagebanana|hidebehind/i',

	'TRACKER_ADDFIELDS_TYPE' => 'Дополнительные поля',
	'TRACKER_ADDFIELDS_TYPE_EXPLAIN' => 'Четыре опции,
		<br /><strong>опция 1</strong> не используется,
		<br /><strong>опция 2</strong> отображать дополнительные поля вместе с формой сообщения, иначе отображать дополнительные поля до формы сообщения (<span style="color:#FF0000;">при значении опции равным <em>Да</em> будет невозможно определять дополнительные поля как обязательные для заполнения</span>),
		<br /><strong>опция 3</strong> отображать в шапке трекера отдельную ссылку для загрузки торрента, если назначен шаблон - ссылка будет отображаться, но в форме можно будет выбрать только форум с назначенным шаблоном,
		<br /><strong>опция 4</strong> добавлять значения дополнительных полей в конец сообщения, иначе в начало сообщения (будет действовать только для вновь добавленных/отредактированных сообщений)',
	'TRACKER_ADDFIELDS_TYPE_STANDART' => 'стандартный',
	'TRACKER_ADDFIELDS_TYPE_ONLY' => 'Если назначен шаблон',

	'PPKBB_FORUMID_REMOVE' => 'Удалять ID форумов',
	'PPKBB_FORUMID_REMOVE_EXPLAIN' => 'Удалять ID форумов из URL (<span style="color:#FF0000;">переключение данной опции  может создать определённые проблемы с поисковыми службами, биржами ссылок и прочими сервисами</span>)',

));
?>
