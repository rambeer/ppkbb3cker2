<?php
/**
*
* acp_board [Russian]
*
* @package language
* @version $Id: ppkbb3cker_trestricts.php, v 1.000 2018-04-08 10:49:00 PPK Exp $
* @copyright (c) 2018 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_TRACKER_TRESTRICTS'				=> 'Ограничения трекера',
	'ACP_TRACKER_TRESTRICTS_EXPLAIN'				=> '',
	'PPKBB_TCWAIT_TIME'				=> 'Значения должны располагаться по ратио от меньшего к большему, пример ограничения: 0.75, 80530636800, 43200 - если ратио меньше 0.75 и аплоад меньше 80530636800 байт (75Гб) время ожидания на скачивание торрента 43200 секунд (12 часов, с даты добавления торрента)',
	'PPKBB_TCWAIT_TIME2'				=> 'Значения должны располагаться по ратио от меньшего к большему, пример ограничения: 0.75, 80530636800, 0 - если ратио меньше 0.75 и скачано больше 80530636800 байт (75Гб) скачивание торрентов невозможно (применяется только к новым торрентам, т.е. уже скачиваемые докачать можно)',
	'PPKBB_TCMAXLEECH_RESTR'				=> 'Значения должны располагаться по ратио от меньшего к большему, если ни одно из значений не подходит, будет использоваться значение из: Максимальное количество торрентов для одновременного скачивания (на одного пользователя), пример ограничения: 0.75, 80530636800, 10 - если ратио меньше 0.75 и скачано больше 80530636800 байт (75Гб) максимальное количество торрентов для скачивания 10',
	'ACP_TRACKER_TRESTRICTS_SETTINGS'				=> 'Ограничения трекера',

	'TRESTRICTS_COUNT' => 'Количество',
	'TRESTRICTS_TYPE' => 'Тип ограничения',
	'TRESTRICTS_PPKBB_TCWAIT_TIME' => 'Время ожидания или запрет на скачивание торрента при низком ратио и/или низком значении аплоада',
	'TRESTRICTS_PPKBB_TCWAIT_TIME2' => 'Время ожидания или запрет на скачивание торрента при низком ратио и/или большом значении скачанного',
	'TRESTRICTS_PPKBB_TCMAXLEECH_RESTR' => 'Ограничение на максимальное количество скачиваемых торрентов при низком ратио и/или большом значении скачанного',
	'TRESTRICTS_SORT_SUCCESS' => 'Порядок успешно изменён.',

	'USER_RATIO' => 'Ратио',
	'USER_RATIO_EXPLAIN' => 'Ратио в единицах (от 0.001) или (если ратио пользователя имеет одно из следующих значений, ограничения также будут работать только если ограничение по ратио является одним из этих значений): Inf. (нет скачанного и загруженного), Seed. (есть загруженное, нет скачанного), Leech. (нет загруженного, есть скачанное), None. (если активна настройка и ратио подпадает под значение &quot;Начало учёта ратио&quot;, независимо от скачанного и загруженного будет отображаться это значение ратио)',
	'USER_UPDOWN_UP' => 'Загружено',
	'USER_UPDOWN_DOWN' => 'Скачано',
	'USER_UPDOWN_EXPLAIN' => '',
	'TIME_COUNT_TIME' => 'Время',
	'TIME_COUNT_TIME_EXPLAIN' => 'Время ожидания на скачивание торрента (0 - запрет скачивания торрентов)',
	'TIME_COUNT_COUNT' => 'Количество торрентов',
	'TIME_COUNT_COUNT_EXPLAIN' => 'Ограничение на максимальное количество скачиваемых торрентов (0 - запрет скачивания торрентов)',
	'ORDER' => 'Порядок',


));
?>
