<?php
/**
*
* common [Russian]
*
* @package ppkBB3cker
* @version $Id: ppkbb3cker_losttorrents.php, v 1.000 2015-10-01 10:34:33 PPK Exp $
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
   exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//


$lang = array_merge($lang, array(
	'ACP_TRACKER_LOSTTORRENTS'				=> 'Потерянные торренты',
	'ACP_TRACKER_LOSTTORRENTS_EXPLAIN'				=> 'В этом разделе можно удалить незарегистрированные на трекере торренты, потерянные торренты можно прикреплять к сообщениям в разделе "Потерянные вложения"',

	'ACP_TRACKER_LOSTTORRENTS_SETTINGS'				=> 'Потерянные торренты',

	'SORT_TITLE' => 'Заголовок',
	'SORT_DATE_UPLOAD' => 'Дата загрузки',
	'SORT_FILENAME' => 'Имя файла',
	'SORT_USER_ID' => 'ID пользователя',

	'TORRENT_DATE' => 'Дата загрузки',
	'ERRORS' => 'Ошибка',
	'TOPIC_TITLE' => 'Заголовок',

	'FIX_LOSTTORRENTS_RESULT' => '%s<br /><a href="%s">Вернуться назад</a>',
	'FIX_LOSTTORRENTS_FINISH' => 'Завершено<br /><br /><a href="%s">Вернуться назад</a>',
	'FIX_LOSTTORRENTS_SUCCESS' => 'Успех: ',
	'FIX_LOSTTORRENTS_ERROR' => '<span style="font-weight:bold;">Ошибка</span>: ',
	'FIX_LOSTTORRENTS_WAIT' => 'Подождите ..<br /><br />',

	'NO_VALID_DATA' => 'Нет идентификаторов: ',
	'NO_VALID_DATA_POST' => 'сообщения',
	'NO_VALID_DATA_TOPIC' => 'темы',
	'NO_VALID_DATA_FORUM' => 'форума',
	'NO_VALID_DATA_POSTER' => 'пользователя',

	'DELETED_TORRENTS' => 'Удалено торрентов: %d',

	'FLT_ERRORS_EXPLAIN' => '
		<strong>Пользователь удалён или не существует</strong> - торрент загружен пользователем которого не существует<br />
		<strong>Сообщение не существует</strong> - торрент прикреплён к сообщению которого не существует<br />
		<strong>Тема не существует</strong> - торрент находится в теме которой не существует<br />
		<strong>Торрент не в форуме трекере</strong> - торрент находится в обычном форуме (не трекере)<br />
		<strong>Торрент не в первом сообщении темы</strong> - торрент находится в комментарии<br />
		<strong>Торрент потерян</strong> - торрент не прикрплён ни к одному сообщению и теме<br />
		<strong>Торрент не зарегистрирован</strong> - торрент загружен как обычное вложение
	',

	'TRACKER_ANONYMOUS' => 'Гость',
	'USER_NOTEXISTS' => 'Пользователь удалён или не существует',
	'POST_NOTEXISTS' => 'Сообщение не существует',
	'TOPIC_NOTEXISTS' => 'Тема не существует',
	'FORUM_NOTTRACKER' => 'Торрент не в форуме трекере',
	'NOT_FIRSTPOST' => 'Торрент не в первом сообщении темы',
	'IS_ORPHAN' => 'Торрент потерян',
	'IS_NULL' => 'Торрент не зарегистрирован',

	'TOTAL_LOGS' => 'Всего записей: <strong>%d</strong>',
));

?>
