<?php
/**
*
* acp_board [Russian]
*
* @package language
* @version $Id: ppkbb3cker_announce.php, v 1.000 2021/03/29 09:25:00 PPK Exp $
* @copyright (c) 2021 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_ANNOUNCE_SETTINGS_EXPLAIN' => '',

	'TRACKER_RIGHTS_TCACHE' => 'Кэширование прав доступа',
	'TRACKER_RIGHTS_TCACHE_EXPLAIN' => 'Кэшировать права доступа на трекере на указанное время, кэширование уменьшает "время выполнения" announce.php',

	'TRACKER_ANNOUNCE_URL' => 'Анонс URL трекера',
	'TRACKER_ANNOUNCE_URL_EXPLAIN'	=> 'Путь от корневой директории форума до файла announce.php (включая слэш в начале и строку запроса "passkey={PASSKEY}" в конце, строка {PASSKEY} будет заменяться пасскеем пользователя), в этом случае для корректного определения анонс URL так же необходимо проверить правильность настроек в опциях блока <u>Настройки URL сервера</u> (Администраторский раздел - Настройки сервера - Настройки URL сервера)<br /><i>Пример: /tracker/announce.php?passkey={PASSKEY}</i><br />Так же можно указать полный URL, если анонс URL указывает на внешний трекер или отсутствует строка с пасскеем, статистика по трекеру учитываться не будет<br /><i>Пример: http://domain.tld/tracker/announce.php?passkey={PASSKEY}</i>',

	'TRACKER_ANNOUNCE_INTERVAL' => 'Интервал обращений к трекеру',
	'TRACKER_ANNOUNCE_INTERVAL_EXPLAIN' => 'Данное значение должно быть меньше значения в опции <u>Время жизни пиров</u>',

	'TRACKER_MINANNOUNCE_INTERVAL' => 'Минимальный интервал обращений к трекеру',
	'TRACKER_MINANNOUNCE_INTERVAL_EXPLAIN' => 'Данное значение должно быть <em>меньше или равно</em> значению <u>Интервал обращений к трекеру</u> (0 - без ограничений)',

	'TRACKER_CHECK_FEXT' => 'Проверять разрешение на расширение в форуме',
	'TRACKER_CHECK_FEXT_EXPLAIN' => 'При анонсе проверяет является ли файл торрента разрешённым в форуме, практически не нужно, если форум является трекером - торрент файлы должны быть разрешены, если форум не является трекером - "анонсировать" торрент файл с такого форума не получится, отключение уменьшает время выполнения announce.php',

	'TRACKER_MAX_SEED' => 'Максимальное количество торрентов для одновременной раздачи',
	'TRACKER_MAX_SEED_EXPLAIN' => 'На одного пользователя (0 - без ограничений)',

	'TRACKER_MAX_LEECH' => 'Максимальное количество торрентов для одновременного скачивания',
	'TRACKER_MAX_LEECH_EXPLAIN' => ' На одного пользователя (0 - без ограничений)',

	'TRACKER_MAXPEERS_LIMIT' => 'Ограничение на количество возвращаемых подсоединённых клиентов',
	'TRACKER_MAXPEERS_LIMIT_EXPLAIN' => 'Если клиентская программа не устанавливает ограничение на возвращаемое количество подключённых к торренту клиентов, используется это значение (0 - использовать данные программы, если не переопределено ниже)',

	'TRACKER_TPRIVATE_FLAG' => 'Управление флагом приватности',
	'TRACKER_TPRIVATE_FLAG_EXPLAIN' => 'Изменение флага приватности при загрузке торрента, определяет в торрентах использование DHT и обмен пирами (некоторые программы могут "обходить" эти ограничения), <br /><span style="color:#FF0000;">при каждом изменении этой настройки для вступления изменений в силу потребуется повторное закачивание и скачивание всех раздаваемых и скачиваемых торрентов</span>',
	'TRACKER_TPRIVATE_FLAG_ASIS' => 'оставлять как есть',
	'TRACKER_TPRIVATE_FLAG_PRIVATE' => 'принудительно как приватный',
	'TRACKER_TPRIVATE_FLAG_NONPRIVATE' => 'принудительно как не приватный',
	'TRACKER_TPRIVATE_FLAG_UPLOADPRIVATE' => 'принимать только приватные',
	'TRACKER_TPRIVATE_FLAG_UPLOADNONPRIVATE' => 'принимать только не приватные',

	'TRACKER_IGNORE_CONNECTABLE' => 'Игнорировать проверку на соединяемость',
	'TRACKER_IGNORE_CONNECTABLE_EXPLAIN' => 'Отключает проверку на возможность подключения к пиру, т.е. при анонсе будет выдавать список всех клиентов, а не только имеющих флаг connectable=yes',
	'TRACKER_IGNORE_CONNECTABLE_NOIGNORE' => 'не игнорировать',
	'TRACKER_IGNORE_CONNECTABLE_IGNORENOCHECK' => 'игнорировать и не проверять',
	'TRACKER_IGNORE_CONNECTABLE_IGNOREYESCHECK' => 'игнорировать и проверять',

	'TRACKER_IGNORED_UPLOAD' => 'Игнорировать значение аплоада',
	'TRACKER_IGNORED_UPLOAD_EXPLAIN' => 'Игнорировать аплоад если он превышает указанный здесь, рассчитывается из максимально возможного аплоада за указанный анонс интервал (на один торрент, 0 - отключить)',

	'TRACKER_DISABLED' => 'Отключить функции анонса',
	'TRACKER_DISABLED_EXPLAIN' => 'Отключить возможность анонса торрентов',

	'SCRAPE_ENABLED' => 'Функции скрэйпа',
	'SCRAPE_ENABLED_EXPLAIN' => 'Две опции
		<br /><strong>опция 1</strong> включить функции скрэйпа, скрэйп позволяет не анонсируя торрент узнать количество скачивающих, раздающих и скачавших пиров на конкретном торренте(ах),
		<br /><strong>опция 2</strong> проверять пасскей при скрэйпе, т.е. функция будет работать только для пользователей трекера',

// 	'MINSCRAPE_INTERVAL' => 'Минимальный интервал скрэйпа',
// 	'MINSCRAPE_INTERVAL_EXPLAIN' => '<span style="color:#FF0000;">Не реализовано</span>',

	'TRACKER_CHECK_BAN' => 'Проверка бана по форуму',
	'TRACKER_CHECK_BAN_EXPLAIN' => 'Проверять пользователя при анонсе на бан на форуме по имени, IP адресу, email адресу',

	'TRACKER_MAXPEERS_REWRITE' => 'Переопределение количества возвращаемых подсоединённых клиентов',
	'TRACKER_MAXPEERS_REWRITE_EXPLAIN' => 'Переопределять данные клиентской программы указанным выше значением',

	'TRACKER_MAXIP_PERTORR' => 'Максимальное число соединений с одного IP адреса на один торрент',
	'TRACKER_MAXIP_PERTORR_EXPLAIN' => '(0 - без ограничений)',

	'TRACKER_MAXIP_PERTR' => 'Максимальное число соединений с одного IP адреса',
	'TRACKER_MAXIP_PERTR_EXPLAIN' => '(0 - без ограничений)',

	'TRACKER_GZREWRITE' => 'Управление gz сжатием',
	'TRACKER_GZREWRITE_EXPLAIN' => 'Ответ клиенту при анонсе, автоматически - определять на основе принятых от клиента заголовков',
	'TRACKER_GZREWRITE_AUTO' => 'автоматически',
	'TRACKER_GZREWRITE_GZ' => 'принудительно в сжатом',
	'TRACKER_GZREWRITE_NONGZ' => 'принудительно в несжатом',

	'TRACKER_ZONES_ENABLE'	 => 'Включить ограничения анонса по зонам',
	'TRACKER_ZONES_ENABLE_EXPLAIN' => 'Ограничивает доступ к трекеру (функциям анонса) на основе определённых IP зон (<span style="color:#FF0000;">соответствующие зоны должны быть определены в разделе <strong>IP зоны трекера</strong></span>), две опции,
		<br /><strong>опция 1</strong> включить ограничение доступа,
		<br /><strong>опция 2</strong> включить ограничение анонса используя  блокированные зоны, все анонсы с блокированных зон будут автоматически отклоняться, блокированные зоны имеют приоритет перед всеми остальными, менее гибки в использовании, но более быстрые',
	'TRACKER_ZONES_ENABLE_OFF' => 'выключить',
	'TRACKER_ZONES_ENABLE_BLACK' => 'чёрные списки',
	'TRACKER_ZONES_ENABLE_WHITE' => 'белые списки',
	'TRACKER_ZONES_ENABLE_BLACKWHITE' => 'чёрно-белые списки',
	'TRACKER_ZONES_ENABLE_WHITEBLACK' => 'бело-чёрные списки',
	'TRACKER_ZONES_ENABLE_ACCESS' => 'без списков',

	'TRACKER_CLIENTS_RESTRICTS' => 'Включить ограничения клиентов',
	'TRACKER_CLIENTS_RESTRICTS_EXPLAIN' => 'Три опции (<span style="color:#FF0000;">соответствующие настройки должны быть определены в разделе <strong>Ограничения клиентов</strong></span>),
		<br /><strong>опция 1</strong> включить бан клиентов по порту,
		<br /><strong>опция 2</strong> включить бан клиентов по заголовку USER_AGENT,
		<br /><strong>опция 3</strong> включить бан клиентов по ID пира',

	'TRACKER_TORRENTS_RESTRICTS' => 'Включить ограничения трекера',
	'TRACKER_TORRENTS_RESTRICTS_EXPLAIN' => 'Три опции (<span style="color:#FF0000;">соответствующие настройки должны быть определены в разделе <strong>Ограничения трекера</strong></span>),
		<br /><strong>опция 1</strong> включить время ожидания или запрет на скачивание торрента при низком ратио и/или низком значении аплоада,
		<br /><strong>опция 2</strong> включить время ожидания или запрет на скачивание торрента при низком ратио и/или большом значении скачанного,
		<br /><strong>опция 3</strong> включить ограничение на максимальное количество скачиваемых торрентов при низком ратио и/или большом значении скачанного',

	'TRACKER_IPTYPE' => 'Определение IP адреса',
	'TRACKER_IPTYPE_EXPLAIN' => 'Метод определения IP адреса, (IP адреса в методах <em>по заголовку</em> и <em>по данным от клиента</em> могут быть подделаны пользователем, если эти методы вернут неправильный IP адрес, будет использован стандартный метод определения IP адреса)',
	'TRACKER_IPTYPE_STANDART' => 'стандартный',
	'TRACKER_IPTYPE_HEADER' => 'по заголовку X_FORWARDED_FOR',
	'TRACKER_IPTYPE_CLIENT' => 'по данным от клиента',

	'TRACKER_ALLOW_UNREGTORR' => 'Анонс незарегистрированных торрентов',
	'TRACKER_ALLOW_UNREGTORR_EXPLAIN' => 'Разрешить на трекере анонс незарегистрированных (не загруженных на трекер) торрентов, разрешить с учётом - учитывать ограничения и статистику, разрешить без учёта - НЕ учитывать ограничения и статистику (количество скачанного и розданного на таких торрентах в статистике пользователей), пасскей для анонса будет отображаться в личном разделе пользователя',
	'TRACKER_ALLOW_UNREGTORR_OFF' => 'запретить',
	'TRACKER_ALLOW_UNREGTORR_ONWSTAT' => 'разрешить с учётом',
	'TRACKER_ALLOW_UNREGTORR_ONWOSTAT' => 'разрешить без учёта',

	'TRACKER_ENABLE_SNATCHSTAT' => 'Ведение статистики',
	'TRACKER_ENABLE_SNATCHSTAT_EXPLAIN' => 'Вести статистику данных по торрентам для пользователей, при отключении этой опции на странице торрента будет отображаться статистика торрента только по текущей сессии, не будет отображаться скорость скачивания и раздачи, пользователи скачавшие торрент, не будут работать бонусы, система анализа данных (при большой активности на торрентах отключение этой опции может снизить нагрузку)',

	'TRACKER_DEAD_TIME' => 'Время жизни пиров',
	'TRACKER_DEAD_TIME_EXPLAIN' => 'Данные пиров (сидеров и личеров) старее этого значения будут игнорироваться, а также удаляться, данное значение должно быть <em>больше</em> значения в опции <u>Интервал обращений к трекеру</u>',

	'TRACKER_MULTIPLE_ANNOUNCE' => 'Множественный анонс',
	'TRACKER_MULTIPLE_ANNOUNCE_EXPLAIN' => 'Разрешить множественный анонс торрентов, при включённой опции пользователи каждый торрент могут скачивать или раздавать одновременно с нескольких клиентов, в статистике подключения также будут отображаться раздельно, при отключении - подключения с разных клиентов одного и того же пользователя будут считаться как одно подключение и будут вызывать ошибку анонс интервала, а также отображаться в статистике как одно подключение, общая статистика пользователя при любой из опций будет считаться одинаково',
));
?>
