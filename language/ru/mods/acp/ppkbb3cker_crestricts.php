<?php
/**
*
* acp_board [Russian]
*
* @package language
* @version $Id: ppkbb3cker_crestricts.php, v 1.000 2012-06-14 12:30:28 PPK Exp $
* @copyright (c) 2012 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_TRACKER_CRESTRICTS'				=> 'Ограничения клиентов',
	'ACP_TRACKER_CRESTRICTS_EXPLAIN'				=> 'Управление ограничениями клиентов, порты, клиенты с указанными здесь USER_AGENT или идентификаторами пиров будут запрещены на трекере',

	'ACP_TRACKER_CRESTRICTS_SETTINGS'				=> 'Управление ограничениями клиентов',

	'CRESTRICTS_PORT'=>'Ограничения по портам',
	'CRESTRICTS_PORT_EXPLAIN'=>'Если клиент при анонсе укажет порт из диапазона портов определённых на этой странице - трекер отклонит запрос и вернёт ошибку',

	'CRESTRICTS_UA'=>'Ограничения по USER_AGENT',
	'CRESTRICTS_UA_EXPLAIN'=>'Если анонс будет произведён клиентом с USER_AGENT определённом на этой странице - трекер отклонит запрос и вернёт ошибку.
		<br /><span style="color:#FF0000;">Регулярное выражение должно быть указано в полном виде, с разделителями, модификаторами и т.д., например: <span style="border:1px solid #CCCCCC;">#uTorrent.*#i</span></span>',

	'CRESTRICTS_PEERID'=>'Ограничения по ID пира',
	'CRESTRICTS_PEERID_EXPLAIN'=>'Если анонс будет произведён клиентом с идентификатором пира определённом на этой странице - трекер отклонит запрос и вернёт ошибку.
		<br /><span style="color:#FF0000;">Регулярное выражение должно быть указано в полном виде, с разделителями, модификаторами и т.д., например: <span style="border:1px solid #CCCCCC;">#-UT.*#i</span></span>',

	'CRESTRICTS_NAME'=>'Тип ограничения',
	'CR_PORT_START'=>'Начальный порт',
	'CR_PORT_END'=>'Конечный порт',
	'CR_UA'=>'Строка USER_AGENT',
	'CR_PEERID'=>'Строка ID пира',
	'CR_RTYPE'=>'Регулярное выражение',
	'CR_STYPE'=>'Строка с учётом регистра',
	'CR_ITYPE'=>'Строка без учёта регистра',
	'CRESTRICTS_COUNT'=>'Количество',
	'CR_ENABLED' => 'Включено',
));
?>
