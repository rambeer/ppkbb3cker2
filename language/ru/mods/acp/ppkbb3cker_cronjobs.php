<?php
/**
*
* acp_board [Russian]
*
* @package language
* @version $Id: ppkbb3cker_cronjobs.php, v 1.000 2019-11-20 10:14:00 PPK Exp $
* @copyright (c) 2019 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_TRACKER_CRONJOBS'				=> 'Задания крона',
	'ACP_TRACKER_CRONJOBS_EXPLAIN'				=> 'Управление заданиями крона. С помощью крона происходит фоновое выполнение определённых задач на трекере, крон трекера и форума не запускается реальным кроном, а выполняется только при посещении пользователями страниц форума. В столбце "Задание" отображается количество запущенных и завершённых заданий, а также процент успешно завершённых, выборочное задание означает выполнение задания только с определёнными торрентами или пользователями. Для отключения задания нужно снять галку "Включено", либо не указывать значения в полях "Время запуска" и "Период запуска". При указании только периода запуска задание будет выполняться через указанный промежуток времени, при указании только времени запуска (в формате ЧЧ) задание будет выполняться раз в сутки в указанный промежуток времени, при указании обоих значений задание будет выполняться при совпадении времени запуска и большего или равного промежутка времени прошедшего с последнего запуска задания. Сброс значения последнего запуска обнуляет время последнего запуска, а также заставляет задание запускаться.
		<br />Запуск от гостя - добавляет возможность запуска задания крона гостями
		<br />При анонсе - задание будет автоматически запускаться вне крона при анонсе торрент-файла (при указании только времени запуска период запуска будет считаться равным одному дню)
		<br />В кроне форума - задание будет запускаться в кроне форума, т.к. требуется использование функций форума',

	'TRACKER_DELETE_DEAD_PEERS' => 'Удаление всех мёртвых пиров',
	'TRACKER_DELETE_DEAD_PEERS_EXPLAIN' => 'Мёртвыми будут считаться пиры старее значения из опции "Время жизни пиров"',

	'TORRENT_RECOUNT_SEED_LEECH' => 'Пересчёт количества сидеров и личеров на торрентах (выборочное)',
	'TORRENT_RECOUNT_SEED_LEECH_EXPLAIN' => '',

	'TORRENT_RECOUNT_SEED_LEECH_SPEED' => 'Пересчёт скорости скачивания и раздачи на торрентах (выборочное)',
	'TORRENT_RECOUNT_SEED_LEECH_SPEED_EXPLAIN' => '',

	'TORRENT_ANNOUNCE_REMOTE_TRACKERS' => 'Анонс внешних анонс URL (выборочное)',
	'TORRENT_ANNOUNCE_REMOTE_TRACKERS_EXPLAIN' => '',

	'TORRENT_FIX_TORRENTS_STATUSES' => ' Исправление несуществующих статусов торрентов',
	'TORRENT_FIX_TORRENTS_STATUSES_EXPLAIN' => 'Торренты получат статус "Без статуса", статусы выключенные в разделе статусов не исправляются',

	'TORRENT_DELETE_DEAD_TORRENTS' => 'Удаление устаревших торрентов из форумов',
	'TORRENT_DELETE_DEAD_TORRENTS_EXPLAIN' => 'Удаляются торренты из текущего форума, опция должна быть включена в разделе "Обслуживание, удаление и очистка"',

	'STAT_COUNT_SEED_LEECH' => 'Подсчёт общего количества раздающих и скачивающих',
	'STAT_COUNT_SEED_LEECH_EXPLAIN' => '',

	'STAT_COUNT_UPLOAD_DOWNLOAD' => 'Подсчёт общего размера розданного и скачанного',
	'STAT_COUNT_UPLOAD_DOWNLOAD_EXPLAIN' => '',

	'STAT_COUNT_SESSION_UPLOAD_DOWNLOAD' => 'Подсчёт общего размера розданного и скачанного за сессию',
	'STAT_COUNT_SESSION_UPLOAD_DOWNLOAD_EXPLAIN' => '',

	'STAT_COUNT_TORRENTS_SIZE' => 'Подсчёт общего размера торрентов',
	'STAT_COUNT_TORRENTS_SIZE_EXPLAIN' => '',

	'STAT_COUNT_UPLOAD_DOWNLOAD_SPEED' => 'Подсчёт общей скорости скачивания и раздачи',
	'STAT_COUNT_UPLOAD_DOWNLOAD_SPEED_EXPLAIN' => '',

	'STAT_COUNT_TORRENTS' => 'Подсчёт общего количества торрентов',
	'STAT_COUNT_TORRENTS_EXPLAIN' => '',

	'STAT_COUNT_COMMENTS' => 'Подсчёт общего количества комментариев',
	'STAT_COUNT_COMMENTS_EXPLAIN' => '',

	'STAT_COUNT_THANKS' => 'Подсчёт общего количества спасибо',
	'STAT_COUNT_THANKS_EXPLAIN' => '',

	'STAT_COUNT_TORRENTSSEED_USERSSEED' => 'Подсчёт количества всех раздаваемых торрентов и раздающих пользователей',
	'STAT_COUNT_TORRENTSSEED_USERSSEED_EXPLAIN' => '',

	'STAT_COUNT_TORRENTSLEECH_USERSLEECH' => 'Подсчёт количества всех скачиваемых торрентов и скачивающих пользователей',
	'STAT_COUNT_TORRENTSLEECH_USERSLEECH_EXPLAIN' => '',

	'USER_COUNT_TORRENTS' => 'Подсчёт общего количества торрентов пользователя (выборочное)',
	'USER_COUNT_TORRENTS_EXPLAIN' => '',

	'USER_COUNT_TORRENTS_SIZE' => 'Подсчёт общего размера торрентов пользователя (выборочное)',
	'USER_COUNT_TORRENTS_SIZE_EXPLAIN' => '',

	'USER_COUNT_COMMENTS' => 'Подсчёт общего количества комментариев пользователя (выборочное)',
	'USER_COUNT_COMMENTS_EXPLAIN' => '',

	'USER_COUNT_SEED_LEECH' => 'Подсчёт общего количества раздаваемых и скачиваемых торрентов пользователя (выборочное)',
	'USER_COUNT_SEED_LEECH_EXPLAIN' => '',

	'USER_CJ_EXPLAIN' => 'На странице профиля пользователя запуск задания будет происходить для отображаемого пользователя, на всех остальных страницах для текущего пользователя, при сбросе последнего запуска сброс будет сделан для всех заданий из категории "Пользователь"',

	'CJ_PAGES' => array(
		'index'=>'Список форумов',
		'portal'=>'Портал Board3',
		'viewforum'=>'Список тем',
		'viewtopic'=>'Список сообщений',
		'memberlist'=>'Профиль пользователя',
		'posting'=>'Создание торрента',
		'search'=>'Поиск',
// 		''=>'Любая страница',
	),
	'CJ_CATS'=>array(
		'stat'=>'Статистика',
		'user'=>'Пользователь *',
		'tracker'=>'Трекер',
		'torrent'=>'Торренты',
	),

	'CRON_NAME' => 'Задание',
	'CRONJOB_PAGES' => 'Страницы запуска',
	'START_END_RUN' => 'Время запуска&nbsp;(ЧЧ)',
	'START_RUN' => 'с',
	'END_RUN' => 'до',
	'LAST_RUN' => 'Последний запуск',
	'LAST_RUN_RESET' => 'сбросить',
	'RUN_PERIOD' => 'Период запуска',
	'FROM_GUEST' => 'От гостя',
	'FORUM_CRON' => 'В кроне форума',
	'IN_ANNOUNCE' => 'При анонсе',
	'CJ_FROM' => 'из',
));
?>
