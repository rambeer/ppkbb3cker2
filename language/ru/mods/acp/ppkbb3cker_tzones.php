<?php
/**
*
* acp_board [Russian]
*
* @package language
* @version $Id: ppkbb3cker_tzones.php, v 1.000 2012-06-14 12:38:14 PPK Exp $
* @copyright (c) 2012 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_TRACKER_ZONES'				=> 'IP зоны трекера',
	'ACP_TRACKER_ZONES_EXPLAIN'				=> 'Управление IP зонами трекера.<br />Блокированная зона - анонсы с данной зоны автоматически будут отклоняться.<br />Белая/Чёрная зоны - при включении опции <u>Включить ограничения Анонса по зонам</u> - на основе этих зон будет определяться доступ к функциям анонса.',

	'ACP_TRACKER_ZONES_SETTINGS'				=> 'Управление IP зонами трекера',

	'ACP_TRACKER_ADDR'				=> 'IP зоны трекера',
	'ACP_TRACKER_ADDR_EXPLAIN'				=> '',

	'ACP_TRACKER_RTRACK'				=> 'IP зоны трекера',
	'ACP_TRACKER_RTRACK_EXPLAIN'				=> 'Указанные дополнительные анонс URL будут добавляться в каждый торрент файл в зависимости от IP адреса пользователя.<br />Строка {YOUR_PASSKEY} будет заменяться текущим пасскеем пользователя на трекере.',
	'INVALID_RTRACK_URL' => 'Некорректный URL трекера',

	'ZONE_NAME'	=> 'Имя зоны',
	'VIEW_ADDR'	=> 'IP адреса',
	'VIEW_RTRACK'	=> 'Мультитрекер',
	'ADDR'	=> 'IP адреса',
	'RTRACK'	=> 'Мультитрекер',
	'ZONE_WHITE'	=> 'белая',
	'ZONE_BLACK'	=> 'чёрная',
	'ZONE_BLOCK'	=> 'блокированная',
	'TYPE' => 'Тип',
	'ZONE_ADDR_START' => 'Начало диапазона',
	'ZONE_ADDR_END' => 'Конец диапазона',
	'ZONE_RTRACK_URL' => 'Url трекера',
	'RTRACK_ENABLED' => 'Включён',
	'RTRACK_BACK' => '<br /><br /><a href="%s">Вернуться назад</a>',
	'ZONE_ENABLED' => 'Включено',

));
?>
