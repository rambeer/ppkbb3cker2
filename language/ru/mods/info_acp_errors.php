<?php
/**
*
* Debug Errors and Notices [English]
*
* @package language
* @version $Id: info_acp_errors.php 120 09:43 05/03/2012 Sylver35 Exp $
* @copyright (c) 2010, 2012 Breizh Portal  http://breizh-portal.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
* @translator Sylver35
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

// Adding the permissions
$lang = array_merge($lang, array(
	// Admin permission
	'acl_a_errors'				=> array('lang' => 'Может управлять модом «Ошибки и уведомления отладчика»', 'cat' => 'misc'),
));

$lang = array_merge($lang, array(
	'ACP_CAT_ERRORS'			=> 'Управление ошибками',
	'ACP_ERRORS_SIMPLE'			=> 'Простые ошибки',
	'ACP_ERRORS_SIMPLE_ALT'			=> 'Простые ошибки',
	'ACP_ERRORS_SIMPLE_EXPLAIN'		=> 'На этой странице вы можете просмотреть список самых простых ошибок, выдаваемых скриптами вашего форума. Эти ошибки не включаются в стандартные логи ошибок движка phpBB3. Вы можете отсортировать список ошибок по имени пользователя, IP-адресу, языку и прочим параметрам.',
	'ACP_ERRORS_CONFIG'			=> 'Настройки',
	'ACP_ERRORS_CONFIG_ALT'			=> 'Настройки мода «Ошибки и уведомления отладчика»',
	'ACP_ERRORS_CONFIG_EXPLAIN'		=> 'На этой странице вы можете проверить, является ли установленная на вашем форуме версия мода «Ошибки и уведомления отладчика» самой свежей, а так же обновить версию мода в случае необходимости. Здесь вам так же доступны для настройки несколько простых опций мода.',

	'LOG_ERRORS_CONFIG' => '<strong> Изменение настроек. Ошибки и уведомления отладчика </strong>',
	'LOG_SELECT_ERRORS' => '<strong> Удаление %s сообщения. Ошибки и уведомления отладчика </strong>',
	'LOG_SELECTS_ERRORS' => '<strong> Удаление сообщений %s. Ошибки и уведомления отладчика </strong>',
	'LOG_PURGE_ERRORS' => '<strong> Полная очистка сообщений. Ошибки и уведомления отладчика </strong> <br /> » %s удаленных сообщений',
	'LOG_SELECT_PURGE_ERRORS' => '<strong> Удаление %s информационных сообщений. Ошибки и уведомления отладчика </strong> <br /> » "Cannot modify header informations"',

	'ERRORS_DEBUG_ACTIVE'			=> 'Включить мод',
	'ERRORS_DEBUG_ACTIVE_EXPLAIN'		=> 'Включить/выключить мод и начать/остановить сохранение ошибок отладчика в базе данных форума.',
	'ERRORS_DEBUG_ECHO'			=> 'Отображать ошибки и уведомления',
	'ERRORS_DEBUG_ECHO_EXPLAIN'		=> 'Включить/выключить отображение ошибок и уведомлений отладчика вверху страниц форума (то есть, включить/выключить обычное поведение отладчика). Для работы мода и вывода ошибок отладчика необходимо включить опции DEBUG и EXTRA DEBUG в файле config.php, который находится в корневой директории форума.',
	'ERRORS_DEBUG_ALL'			=> 'Отслеживать все ошибки',
	'ERRORS_DEBUG_ALL_EXPLAIN'		=> 'Включить/выключить отслеживание всех, даже самых мелких ошибок отладчика. Если данная опция установлена в «Да», то мод будет сохранять даже сообщения типа "Cannot modify header informations".',
	'ERRORS_DEBUG_WHOIS'			=> 'Метод WHOIS',
	'ERRORS_DEBUG_WHOIS_EXPLAIN'		=> 'Выберите метод использования команды WHOIS для IP-адресов:<br />&nbsp;&nbsp;► GeoTool FlagFox — расширение для браузера FireFox;<br />&nbsp;&nbsp;► Сайт en.utrace.de.',
	'ERRORS_DEBUG_DOMAINE'			=> 'GeoTool FlagFox',
	'ERRORS_DEBUG_UTRACE'			=> 'http://en.utrace.de',
	'ERRORS_DEBUG_ACP'			=> 'Всего сообщений',
	'ERRORS_DEBUG_ACP_EXPLAIN'		=> 'Выберите количество сообщений об ошибках, которые будут сохраняться для вывода на странице «Простые ошибки».',
	'ERRORS_WHOIS'				=> '► WHOIS',
	'ERRORS_UTRACE'				=> 'Воспользоваться командой WHOIS с помощью сайта Utrace -> ',
	'ERRORS_DOMAINE'			=> 'Воспользоваться командной WHOIS с помощью Domaine Tools -> ',
	'ERRORS_SEARCH'				=> 'Найти все сообщения %s',
	'ERRORS_DELETE'				=> 'Удалить сообщения типа «Cannot modify header information»',
	'ERRORS_DEBUG_DISPLAY'			=> 'Отображать ошибки',
	'ERRORS_DEBUG_OFF'			=> 'Не отображать ошибки',
	'ERRORS_PAGE_LINE'			=> 'страница, файл и строка',
	'ERRORS_PAGE'				=> 'Страница',
	'ERRORS_FILE'				=> 'Файл',
	'ERRORS_LINE'				=> 'Строка',
	'ERRORS_LANGUAGE'			=> 'Язык',
	'ERRORS_DATE'				=> 'дата',
	'ERRORS_USER'				=> 'пользователь',
	'ERRORS_MESSAGE' 			=> '%s сообщение',
	'ERRORS_MESSAGES' 			=> '%s сообщений',
	'ERRORS_NO_MESSAGE'			=> 'Сообщений нет.',

	'CURRENT_VERSION'			=> 'Установленная версия',
	'LATEST_VERSION'			=> 'Самая свежая версия',
	'ERRORS_NO_VERSION'			=> '<span style="color: red">Не удается проверить наличие самой свежей версии мода…</span>',
	'ERRORS_VERSION_COPY'			=> 'Используется мод «<a href="%1$s" target="_blank" title="Открыть в новом окне">Ошибки и уведомления отладчика</a>» версии %2$s. Официальный сайт поддержки мода: <a href="http://breizh-portal.com/" target="_blank" title="Открыть в новом окне">breizh-portal.com</a>.',
	'ERRORS_VERSION_CHECK'			=> 'Проверка новой версии мода',
	'ERRORS_VERSION_CHECK_EXPLAIN'		=> 'Здесь можно проверить, не вышла ли более свежая версия мода «Ошибки и уведомления отладчика».',
	'ERRORS_VERSION_NOT_UP_TO_DATE'		=> 'Установленная на вашем форуме версия мода «Ошибки и уведомления отладчика» устарела.<br />Ниже вы найдете ссылку на страницу с описанием новой версии мода и с инструкцией по обновлению установленной у вас версии.',
	'ERRORS_VERSION_UP_TO_DATE'		=> 'На вашем форуме установлена самая свежая версия мода. Необходимости в обновлении на данный момент нет.',

	'ERRORS_INSTRUCTIONS'			=> '<br /><h1>Использование мода «Ошибки и уведомления отладчика» версии %1$s</h1><br />
		<p>Команда «Breizh Portal» желает вам всего хорошего и надеется, что вам понравился данный мод.<br />
		Вы можете внести добровольные пожертвования на развитие этого мода. Сделать это можно,
		посетив <strong><a href="%2$s" target="_blank" title="Открыть в новом окне">эту
		страницу</a></strong>.</p>
		<p>Если вам необходима поддержка по данному моду, посетите <strong><a href="%3$s" target="_blank"
		title="Открыть в новом окне">официальный форум поддержки</a></strong> мода.</p>
		<p>Трекер по данному моду находится по <strong><a href="%4$s" target="_blank"
		title="Открыть в новом окне">этому адресу</a></strong>. С помощью него вы сможете отслеживать
		появление новых исправлений и новых версий мода.</p>',
	'ERRORS_UPDATE_INSTRUCTIONS'		=> ' <h1>Объявление о новой версии</h1>
		<p>Пожалуйста, прочтите <a href="%1$s" target="_blank" title="%1$s"><strong>объявление
		о выходе новой версии</strong></a> мода перед тем, как запустить процесс обновления на
		вашем форуме. В этой объявлении может содержаться полезная для вас информация. Кроме того,
		в объявлении о выходе новой версии мода содержатся ссылки на скачивание пакета обновления
		и полный список изменений в новой версии.</p>
		<br />
		<h1>Как обновить установленную у вас версию мода «Ошибки и уведомления отладчика»?</h1>
		<p>► Скачайте самую свежую версию мода.</p>
		<p>► Распакуйте архив с инсталляционными файлами мода и откройте в браузере файл <b>install.xml</b>.
		В нем содержится инструкция по обновлению.</p>
		<p>► Официальное объявление о выходе новой версии мода: (%2$s)</p>',

	// Install
	'INSTALL_MOD_NAME'			=> 'Ошибки и уведомления отладчика',
	'INSTALL_CHECK_NO_TITLE'		=> 'Мод «%1$s» еще не установлен на вашем форуме.',
	'INSTALL_CHECK_FINISH_TITLE'		=> 'Мод «%1$s»  версии %2$s установлен на вашем форуме.',
	'INSTALL_CHECK_INSTALL_TITLE'		=> '► Щелкните <a href="%s" title="Установка мода">здесь</a> для запуска процесса установки мода.',
	'INSTALL_CHECK_UPDATE_TITLE'		=> '► Щелкните <a href="%s" title="Обновление версии мода">здесь</a> для запуска процесса обновления мода.',
	'INSTALL_NO_ADMIN'			=> '► У вас нет соответствующих прав доступа для инсталляции или обновления данного мода.', // For the no admins
	'INSTALL_FINISH_MESSAGE'		=> 'Внимание! Если у вас возникли какие-то проблемы при установке или при обновлении мода, посетите
		<a href="http://breizh-portal.com/index.html" target="_blank" title="Открыть в новом окне">Breizh Portal</a>.
		Поддержка по этому моду предоставляется <strong>исключительно</strong> на этом сайте.<br />Вы так же
		можете попробовать в использовании и другие эксклюзивные моды, выпущенные нашей командой.
		<br />Вы так же можете сделать пожертвования на поддержку и развитие этого и других наших модов.<br /><br />► Для
		завершения инсталляции мода, а так же по причинам безопасности, вы <strong>должны</strong> удалить или
		переименовать папку /install/ в корневой директории вашего форума.<br />',
	'INSTALL_PROGRESS'			=> 'В настоящий момент выполняется запрошенная операция…',
	'INSTALL_TABLE_UNDEFINED'		=> 'Ошибка: таблица «<strong>%s</strong>» не определена в файле /includes/constants.php.<br /><br />Пожалуйста, проверьте, сделали ли вы <b>все</b> необходимые правки, указанные в инструкции в файле <b>install.xml</b>.<br />',
	'UNINSTALL_ERRORS_MOD'			=> 'Все записи о моде «%1$s» версии %2$s можно удалить из базы данных форума по <a href="%3$s" title="Удаление мода">этой ссылке</a>.',
	'INSTALL_MOD_UNINSTALED'		=> 'Мод «%1$s» удален из базы данных форума.<br /><br />► <a href="%2$s" title="Вернуться к процессу инсталляции мода">Вернуться к процессу инстялляции мода</a><br /><br />► <a href="%3$s" title="Перейти на главную страницу форума">Перейти на главную страницу форума</a>',

	'LOG_INSTALL_AUTH_ADM'			=> '<strong>Добавляем и активируем администраторские права</strong>',
	'LOG_INSTALL_TABLE_ADD'			=> '<strong>Добавляем таблицу мода в базу данных форума</strong><br />» “%s',
	'LOG_INSTALL_MODULE_ADD'		=> '<strong>Создаем и включаем модуль «%1$s» во вкладке «Модули»</strong><br />» «%2$s»',
	'LOG_INSTALL_MODULE_EDIT'		=> '<strong>Редактируем модуль «%1$s»</strong><br />» «%2$s»',
	'LOG_INSTALL_MOD_ADD'			=> '<strong>Устанавливаем мод</strong><br />» %1$s версии %2$s',
	'LOG_INSTALL_MOD_UPDATE'		=> '<strong>Обновляем мод</strong><br />» %1$s версии %2$s',
	'LOG_INSTALL_CONFIG_ADD'		=> '<strong>Добавляем ссылку на настройки мода</strong><br />» «%s»',
	'LOG_INSTALL_CONFIG_DEL'		=> '<strong>Удаляем ссылку на настройки мода</strong><br />» «%s»',
	'LOG_INSTALL_TABLE_DEL'			=> '<strong>Удаляем таблицу мода из базы данных форума</strong><br />» «%s»',
	'LOG_INSTALL_PERM_DEL'			=> '<strong>Удаляем права доступа</strong><br />» «%s»',
	'LOG_INSTALL_MODULE_DEL'		=> '<strong>Удаляем модуль «%1$s»</strong><br />» «%2$s»',

	// Added in version 1.1.0
	'ERRORS_DEBUG_FOOTER'			=> 'Отладочная информация в «подвале» форума',
	'ERRORS_DEBUG_FOOTER_EXPLAIN'		=> 'Включить или выключить отображение информации отладчика в «подвале» форума для пользователей без администраторских прав доступа.<br />Учтите, что эти данные по-любому будут видны администраторам форума, если в файле <b>config.php</b> включена опция EXTRA DEBUG.',
	'ERRORS_FOOTER_DISPLAY'			=> 'Отображать',
	'ERRORS_FOOTER_OFF'			=> 'Не отображать',

	// Added in version 1.2.0
	'ACP_ERRORS'				=> 'Ошибки',
	'ERRORS_SEE_MESSAGES'			=> 'Просмотр сообщений',
	'UNABLE_CONNECT'    			=> 'Невозможно соединиться с удаленным сервером для проверки свежей версии мода. Код ошибки: %s',
));

?>