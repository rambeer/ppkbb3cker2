<?php
/**
*
* @package acp Debug Errors and Notices
* @version $Id: acp_error.php 120 09:43 05/03/2012 Sylver35 Exp $
* @copyright (c) 2010, 2012 Breizh Portal  http://breizh-portal.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
* @package acp
*/
class acp_errors
{
	var $u_action;
	var $new_config = array();

	function main($id, $mode)
	{
		global $db, $user, $auth, $template, $config;
		global $phpbb_root_path, $phpbb_admin_path, $phpEx;

		$mode = request_var('mode', '');
		$submit = (isset($_POST['submit'])) ? true : false;
		$flag_on = function_exists('get_new_flag') ? true : false;
		$form_key = 'acp_errors';
		$this->tpl_name = 'acp_errors';
		$this->page_title = 'ACP_ERRORS_' .strtoupper($mode). '_ALT';
		add_form_key($form_key);

		switch ($mode)
		{
			case 'simple':
				$purge 		= request_var('purge', '');
				$select 	= request_var('select', '');
				$action 	= request_var('action', '');
				$user_id 	= request_var('u', 0);
				$user_ip 	= request_var('ip', '');
				$langue		= request_var('langue', '');
				$page 		= request_var('page', '');
				$file 		= request_var('file', '');
				$message 	= request_var('message', '');
				$type		= request_var('type', 0);
				$start		= request_var('start', 0);
				$marked		= request_var('mark', array(0));
				$deletemark = (!empty($_POST['delmarked'])) ? true : false;

				if ($deletemark)
				{
					if (confirm_box(true))
					{
						$where_sql = '';
						$del_acp = 0;

						if ($deletemark && sizeof($marked))
						{
							$sql_in = array();
							foreach ($marked as $mark)
							{
								$sql_in[] = $mark;
								$del_acp++;
							}
							$where_sql = ' WHERE ' . $db->sql_in_set('log_id', $sql_in);
							unset($sql_in);
						}

						if ($where_sql)
						{
							$sql = 'DELETE FROM ' . ERROR_LOG_TABLE . " $where_sql";
							$db->sql_query($sql);
							$log_entrie = 'LOG_SELECT' .(($del_acp > 1) ? 'S' : ''). '_ERRORS';
							add_log('admin', $log_entrie, $del_acp);
						}
						redirect($this->u_action);
					}
					else
					{
						confirm_box(false, $user->lang['CONFIRM_OPERATION'], build_hidden_fields(array(
							'i'			=> $id,
							'mode'		=> $mode,
							'start'		=> $start,
							'delmarked'	=> $deletemark,
							'mark'		=> $marked,
						)));
					}
				}

				if ($purge)
				{
					if (!confirm_box(true))
					{
						switch ($purge)
						{
							default:
								$confirm = true;
								$confirm_lang = 'CONFIRM_OPERATION';
						}

						if ($confirm)
						{
							confirm_box(false, $user->lang[$confirm_lang], build_hidden_fields(array(
								'i'			=> $id,
								'mode'		=> $mode,
								'purge'		=> $purge,
							)));
						}
					}
					else
					{
						$sql = 'SELECT COUNT(log_id) as nr
							FROM ' . ERROR_LOG_TABLE;
						$result = $db->sql_query($sql);
						$count = (int)$db->sql_fetchfield('nr');
						$db->sql_freeresult($result);
						if ($count > 0)
						{
							$sql = 'DELETE FROM ' . ERROR_LOG_TABLE;
							$db->sql_query($sql);
							add_log('admin', 'LOG_PURGE_ERRORS', $count);
						}
						redirect($this->u_action);
					}
				}
				if ($select)
				{
					if (!confirm_box(true))
					{
						switch ($select)
						{
							default:
								$confirm = true;
								$confirm_lang = 'CONFIRM_OPERATION';
						}

						if ($confirm)
						{
							confirm_box(false, $user->lang[$confirm_lang], build_hidden_fields(array(
								'i'			=> $id,
								'mode'		=> $mode,
								'select'	=> $select,
							)));
						}
					}
					else
					{
						$sql = 'SELECT COUNT(log_id) as nr
							FROM ' . ERROR_LOG_TABLE . "
							WHERE log_message LIKE 'Cannot modify header information%'";
						$result = $db->sql_query($sql);
						$count = (int)$db->sql_fetchfield('nr');
						$db->sql_freeresult($result);

						if ($count > 0)
						{
							$sql = 'DELETE FROM ' . ERROR_LOG_TABLE . " WHERE log_message LIKE 'Cannot modify header information%'";
							$db->sql_query($sql);
							add_log('admin', 'LOG_SELECT_PURGE_ERRORS', $count);
						}
						redirect($this->u_action);
					}
				}

				$i = 0;
				$in_action = $action ? "&amp;action=$action&amp;" : '';
				$where_sql = $sql_where = '';

				switch ($action)
				{
					case 'user':
						$where_sql = "log_user = $user_id";
						$sql_where = "e.log_user = $user_id";
						$in_action .= "u=$user_id";
					break;
					case 'ip':
						$where_sql = "log_ip = '".$db->sql_escape($user_ip)."'";
						$sql_where = "e.log_ip = '".$db->sql_escape($user_ip)."'";
						$in_action .= "ip=$user_ip";
					break;
					case 'langue':
						$where_sql = "log_lang = '".$db->sql_escape($langue)."'";
						$sql_where = "e.log_lang = '".$db->sql_escape($langue)."'";
						$in_action .= "langue=$langue";
					break;
					case 'page':
						$where_sql = "log_page = '".$db->sql_escape($this->clean_error_out($page))."'";
						$sql_where = "e.log_page = '".$db->sql_escape($this->clean_error_out($page))."'";
						$in_action .= "page=$page";
					break;
					case 'file':
						$where_sql = "log_file = '".$db->sql_escape($file)."'";
						$sql_where = "e.log_file = '".$db->sql_escape($file)."'";
						$in_action .= "file=$file";
					break;
					case 'message':
						$where_sql = "log_message = '".$db->sql_escape($message)."'";
						$sql_where = "e.log_message = '".$db->sql_escape($message)."'";
						$in_action .= "message=$message";
					break;
					case 'type':
						$where_sql = "log_type = $type";
						$sql_where = "e.log_type = $type";
						$in_action .= "type=$type";
					break;
				}

				if ($config['errors_debug_ip'])
				{
					$_whois = '<a href="http://geo.flagfox.net/?ip=%1$s" onclick="popup(this.href,900,650);return false;" title="' .$user->lang['ERRORS_DOMAINE']. '%1$s">' .$user->lang['ERRORS_WHOIS']. '</a>';
				}
				else
				{
					$_whois = '<a href="http://en.utrace.de/?query=%1$s" onclick="popup(this.href,900,650);return false;" title="' .$user->lang['ERRORS_UTRACE']. '%1$s">' .$user->lang['ERRORS_WHOIS']. '</a>';
				}
				$tpl_ip = '<a href="' .$this->u_action. '&amp;action=ip&amp;ip=%1$s" title="%2$s">%1$s</a>';
				$tpl_langue = '<a href="' .$this->u_action. '&amp;action=langue&amp;langue=%1$s" title="%2$s"><strong>%3$s</strong>: %1$s</a>';
				$tpl_page = '<a href="' .$this->u_action. '&amp;action=page&amp;page=%1$s" title="%2$s">%3$s</a>';
				$tpl_file = '<a href="' .$this->u_action. '&amp;action=file&amp;file=%1$s" title="%2$s">%1$s</a>';
				$tpl_message = '<a href="' .$this->u_action. '&amp;action=message&amp;message=%1$s" title="%2$s">%3$s</a>';
				$tpl_type = '<a href="' .$this->u_action. '&amp;action=type&amp;type=%1$s" title="%2$s">%3$s</a>';
				$tpl_name = '<a href="' .$this->u_action. '&amp;action=user&amp;u=1" title="' .sprintf($user->lang['ERRORS_SEARCH'], $user->lang['GUEST']). '" style="color: #%1$s">%2$s</a>';

				$sql_nr = $db->sql_build_query('SELECT', array(
					'SELECT'    => 'COUNT(DISTINCT log_id) as total',
					'FROM'      => array(ERROR_LOG_TABLE => ''),
					'WHERE'		=> $where_sql,
				));
				$result_nr = $db->sql_query($sql_nr);
				$total_posts = $db->sql_fetchfield('total', $result_nr);
				$db->sql_freeresult($result_nr);

				$sql = $db->sql_build_query('SELECT', array(
					'SELECT'    => 'e.*, u.user_id, u.user_colour, u.username',
					'FROM'      => array(ERROR_LOG_TABLE => 'e'),
					'LEFT_JOIN'	=> array(
						array(
							'FROM'	=> array(USERS_TABLE => 'u'),
							'ON'	=> 'u.user_id = e.log_user'
						)
					),
					'WHERE'		=> $sql_where,
					'ORDER_BY'	=> 'e.log_time DESC',
				));
				$sql = $flag_on ? str_replace('u.username', 'u.username, u.user_flag', $sql) : $sql;
				$result = $db->sql_query_limit($sql, $config['errors_debug_acp'], $start);
				while ($row = $db->sql_fetchrow($result))
				{
					$row['log_message']=htmlspecialchars($row['log_message']);
					$_type = ($row['log_type'] == E_WARNING) ? '[PHP Warning]' : '[PHP Notice]'; // this line like in functions.php
					$log_message = str_replace(array('./images', './style', '\\"'), array('./../images', 'adm/style', '"'), $row['log_message']);
					$username = ($row['user_id'] == ANONYMOUS) ? sprintf($tpl_name, $row['user_colour'], get_username_string('no_profile', ANONYMOUS, false, $row['user_colour'])) : ($flag_on ? get_username_string_flag('flag', $row['user_id'], $row['username'], $row['user_colour'], $row['user_flag'], false, $this->u_action. '&amp;action=user') : get_username_string('full', $row['user_id'], $row['username'], $row['user_colour'], false, $this->u_action. '&amp;action=user'));
					$template->assign_block_vars('messages', array(
						'TIME'			=> $user->format_date($row['log_time']),
						'ID'			=> $row['log_id'],
						'POSTER'		=> $username,
						'POSTER_ALT'	=> sprintf($user->lang['ERRORS_SEARCH'], $row['username']),
						'WHOIS'			=> $row['log_ip'] ? sprintf($_whois, $row['log_ip']) : ' - ',
						'IP'			=> sprintf($tpl_ip, $row['log_ip'], sprintf($user->lang['ERRORS_SEARCH'], $row['log_ip'])),
						'LANG'			=> sprintf($tpl_langue, $row['log_lang'], sprintf($user->lang['ERRORS_SEARCH'], $row['log_lang']), $user->lang['ERRORS_LANGUAGE'], $row['log_lang']),
						'PAGE'			=> sprintf($tpl_page, $this->clean_error_in($row['log_page']), sprintf($user->lang['ERRORS_SEARCH'], $row['log_page']), $row['log_page']),
						'FILE'			=> sprintf($tpl_file, $row['log_file'], sprintf($user->lang['ERRORS_SEARCH'], $row['log_file'])),
						'MESSAGE'		=> sprintf($tpl_message, $row['log_message'], sprintf($user->lang['ERRORS_SEARCH'], $row['log_message']), $log_message),
						'TYPE'			=> sprintf($tpl_type, $row['log_type'], sprintf($user->lang['ERRORS_SEARCH'], $_type), $_type),
						'LINE'			=> $row['log_line'],
						'ROW_NUMBER'	=> $i + ($start + 1),
					));
					$i++;
				}
				$db->sql_freeresult($result);

				$template->assign_vars(array(
					'S_DISPLAY_MESSAGES'	=> ($i > 0) ? true : false,
					'S_ON_PAGE'				=> ($total_posts > $config['errors_debug_acp']) ? true : false,
					'TOTAL_MESSAGES'		=> sprintf($user->lang['ERRORS_MESSAGE' .(($total_posts > 1) ? 'S' : '')], $total_posts),
					'PAGE_NUMBER' 			=> on_page($total_posts, $config['errors_debug_acp'], $start),
					'PAGINATION' 			=> generate_pagination($this->u_action . $in_action, $total_posts, $config['errors_debug_acp'], $start),
					'TOTAL_POSTS'			=> $total_posts,
					'S_OVERVIEW'			=> true,
					'U_ACTION'				=> $this->u_action,
					'TITLE'					=> $user->lang['ACP_ERRORS_SIMPLE_ALT'],
					'TITLE_EXPLAIN'			=> $user->lang['ACP_ERRORS_SIMPLE_EXPLAIN'],
				));
			break;

			case 'config':
				global $db, $user, $auth, $template, $config;
				global $phpbb_admin_path, $phpbb_root_path, $phpEx;

				$update = isset($_POST['update']) ? true : false;

				if ($update)
				{
					$settings = array (
						'errors_debug_active'	=> request_var('errors_debug_active', 1),
						'errors_debug_echo'		=> request_var('errors_debug_echo', 1),
						'errors_debug_footer'	=> request_var('errors_debug_footer', 1),
						'errors_debug_all'		=> request_var('errors_debug_all', 0),
						'errors_debug_ip'		=> request_var('errors_debug_ip', 0),
						'errors_debug_acp'		=> request_var('errors_debug_acp', 25),
					);

					foreach ($settings as $config_name => $config_value)
					{
						set_config($config_name, $config_value, false);
					}
					add_log('admin', 'LOG_ERRORS_' . strtoupper($mode));
					trigger_error($user->lang['CONFIG_UPDATED'] . adm_back_link($this->u_action));
				}
				else
				{
					// Get current and latest version
					$errstr = $announcement_url = $latest_version = '';
					$errno = 0;
					$info = get_remote_file('breizh-portal.com', '/updatecheck', 'errors_debug.txt', $errstr, $errno);

					if ($info === false)
					{
						$template->assign_vars(array(
							'S_ERROR'   => true,
							'ERROR_MSG' => sprintf($user->lang['UNABLE_CONNECT'], $errstr),
						));
					}
					else
					{
						$info = explode("\n", $info);
						if(is_array($info) && isset($info[3]))
						{
							$latest_version 	= trim($info[0]);
							$announcement_url 	= trim($info[1]);
							$up_to_date			= phpbb_version_compare($config['errors_debug_version'], $latest_version, '<') ? false : true;

							if (!$up_to_date)
							{
								$template->assign_vars(array(
									'S_ERROR'   			=> true,
									'S_UP_TO_DATE'			=> false,
									'ERROR_MSG' 			=> sprintf($user->lang['ERRORS_VERSION_NOT_UP_TO_DATE'], $config['errors_debug_version'], $latest_version),
									'UPDATE_INSTRUCTIONS'	=> sprintf($user->lang['ERRORS_UPDATE_INSTRUCTIONS'], $announcement_url, $latest_version),
								));
							}
							else
							{
								$template->assign_vars(array(
									'S_ERROR'   			=> false,
									'S_UP_TO_DATE'			=> true,
									'UP_TO_DATE_MSG'		=> sprintf($user->lang['ERRORS_VERSION_UP_TO_DATE'], $config['errors_debug_version']),
									'UPDATE_INSTRUCTIONS'	=> sprintf($user->lang['ERRORS_INSTRUCTIONS'], $config['errors_debug_version'], $announcement_url, trim($info[2]), trim($info[3])),
								));
							}
						}
					}

					$template->assign_vars(array(
						'S_CONFIG'				=> true,
						'U_ACTION'				=> $this->u_action,
						'TITLE'					=> $user->lang['ACP_ERRORS_CONFIG_ALT'],
						'TITLE_EXPLAIN'			=> $user->lang['ACP_ERRORS_CONFIG_EXPLAIN'],
						'ERRORS_VERSION'		=> sprintf($user->lang['ERRORS_VERSION_COPY'], $announcement_url, $config['errors_debug_version']),
						'S_NO_VERSION'			=> $latest_version ? false : true,
						'LATEST_VERSION'		=> $latest_version ? $latest_version : $user->lang['ERRORS_NO_VERSION'],
						'CURRENT_VERSION'		=> $config['errors_debug_version'],
						'ERRORS_DEBUG_ACTIVE'	=> isset($config['errors_debug_active']) ? (((bool)$config['errors_debug_active'] == 1) ? true : false) : '',
						'ERRORS_DEBUG_ECHO'		=> isset($config['errors_debug_echo']) ? (((bool)$config['errors_debug_echo'] == 1) ? true : false) : '',
						'ERRORS_DEBUG_FOOTER'	=> isset($config['errors_debug_footer']) ? (((bool)$config['errors_debug_footer'] == 1) ? true : false) : '',
						'ERRORS_DEBUG_ALL'		=> isset($config['errors_debug_all']) ? (((bool)$config['errors_debug_all'] == 1) ? true : false) : '',
						'ERRORS_DEBUG_IP'		=> isset($config['errors_debug_ip']) ? (((bool)$config['errors_debug_ip'] == 1) ? true : false) : '',
						'ERRORS_DEBUG_ACP'		=> isset($config['errors_debug_acp']) ? (int)$config['errors_debug_acp'] : '',
					));
				}
			break;

			default:
				trigger_error('NO_MODE', E_USER_ERROR);
			break;
		}
	}

	function clean_error_in($page)
	{
		$page = str_replace(array('?', '&amp;', '&', ' '), array('||', '&', '|', '%20'), $page);
		return $page;
	}

	function clean_error_out($page)
	{
		$page = str_replace(array('||', '&', '|', '%20'), array('?', '&amp;', '&', ' '), $page);
		return $page;
	}
}

?>