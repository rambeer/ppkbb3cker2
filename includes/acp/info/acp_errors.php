<?php
/**
*
* Debug Errors and Notices
*
* @package acp
* @version $Id: acp_errors.php 120 09:43 05/03/2012 Sylver35 Exp $ 
* @copyright (c) 2010, 2012 Breizh Portal  http://breizh-portal.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @package module_install
*/
class acp_errors_info
{
	function module()
	{
		return array(
			'filename'	=> 'acp_errors',
			'title'		=> 'ACP_ERRORS_SIMPLE',
			'version'	=> '1.2.0',
			'modes'		=> array(
				'config'		=> array('title' => 'ACP_ERRORS_CONFIG', 	'auth' => 'acl_a_errors', 'cat' => array('ACP_CAT_ERROR')),
				'simple'		=> array('title' => 'ACP_ERRORS_SIMPLE', 	'auth' => 'acl_a_errors', 'cat' => array('ACP_CAT_ERROR')),
			),
		);
	}

	function install()
	{
	}

	function uninstall()
	{
	}
}

?>