<?php
/**
*
* @package ppkBB3cker
* @version $Id: topdown_torrents.php 1.000 2010-10-30 18:39:00 PPK $
* @copyright (c) 2010 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/

if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']))
{
	exit();
}

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);

include($phpbb_root_path . 'common.' . $phpEx);

// Start session management
$user->session_begin(false);
$auth->acl($user->data);
$user->setup();

$dt=time();
$topdown_torrents_fid=request_var('fid', 0);
$topdown_torrents_pid=request_var('pid', 0);
$topdown_torrents_id=request_var('id', '');
$topdown_torrents='';
$tooltip_tdt='';

if(!$auth->acl_get('u_canviewtopdowntorrents') || (!$config['ppkbb_topdown_torrents'][0] && $topdown_torrents_pid && !$topdown_torrents_fid) || (!$config['ppkbb_topdown_torrents'][1] && !$topdown_torrents_fid && !$topdown_torrents_pid) || (!$config['ppkbb_topdown_torrents'][2] && $topdown_torrents_fid && !$topdown_torrents_pid))
{
	$topdown_torrents=false;
}
else
{
	$disallow_access=array_unique(array_keys($auth->acl_getf('!f_read', true)));
	$disallow_access=count($disallow_access) ? ' AND '.$db->sql_in_set('t.forum_id', $disallow_access, true) : '';

	if(!$config['ppkbb_topdown_torrents'][9])
	{
		$posters_array=array(101, 110, 111);
		$thumbnails_only='';
	}
	else
	{
		$posters_array=array(110);
		$thumbnails_only=' AND a.thumbnail=1';
	}
	$post_time=$config['ppkbb_topdown_torrents'][10] ? ' AND tr.added > '.($dt-$config['ppkbb_topdown_torrents'][10]) : '';
	$topdown_torrents_exclude=count($config['ppkbb_topdown_torrents_exclude']) ? ' AND '.$db->sql_in_set('t.forum_id', $config['ppkbb_topdown_torrents_exclude'], ($config['ppkbb_topdown_torrents_trueexclude'] ? true : false)) : '';
	$topdown_torrents_fid=$topdown_torrents_fid ? ' AND t.forum_id='.$topdown_torrents_fid :'';
	$topdown_torrents_seeders=$config['ppkbb_topdown_torrents'][7] ? ' AND tr.seeders'.($config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_seeders' : '').' >= '.$config['ppkbb_topdown_torrents'][7] : '';

	$sql_array = array(
		'SELECT'	=> 't.topic_title, t.topic_id, t.forum_id,
			tr.times_completed'.($config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_times_completed' : '').' times_completed,
			tr.leechers'.($config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_leechers' : '').' leechers,
			tr.seeders'.($config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_seeders' : '').' seeders,
			tr.id torrent_id,
			tr.post_msg_id post_id',

		'FROM'		=> array(
			TOPICS_TABLE		=> 't',
			TRACKER_TORRENTS_TABLE		=> 'tr',
		),

		'WHERE'		=> 't.topic_id=tr.topic_id
			AND t.topic_torrent=1
			AND t.forb < 1
			AND '.$db->sql_in_set('t.topic_posters', $posters_array)
				.$disallow_access
				.'%1$s'
				.$topdown_torrents_exclude
				.$topdown_torrents_fid
				.$topdown_torrents_seeders,

		'ORDER_BY' => ($config['ppkbb_topdown_torrents'][11] ? 'tr.added' : 'tr.times_completed'.($config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_times_completed' : '')).' DESC',
	);

	$sql=$db->sql_build_query('SELECT', $sql_array).' LIMIT 0, '.$config['ppkbb_topdown_torrents'][3];

	$md5_sql=md5($sql);
	$topdown_torrents=$cache->get('_ppkbb3cker_tdt_'.$md5_sql);
	if(!$topdown_torrents)
	{
		$result=$db->sql_query(sprintf($sql, $post_time), $config['ppkbb_topdown_torrents'][6], $md5_sql);
		$torrents_posters=$post_ids=array();
		while($row=$db->sql_fetchrow($result))
		{
			$torrents_posters[$row['torrent_id']]=$row;
			$post_ids[$row['torrent_id']]=$row['post_id'];
		}
		$db->sql_freeresult($result);

		if($config['ppkbb_topdown_torrents'][8] && count($torrents_posters) < $config['ppkbb_topdown_torrents'][8])
		{
			$topdown_torrents=false;
		}
		else
		{
			$attachments=array();
			if($post_ids)
			{
				$sql =
					'SELECT
						a.attach_id,
						a.post_msg_id post_id,
						a.extension,
						a.real_filename,
						a.i_width,
						a.i_height,
						a.i_external
					FROM
						'.ATTACHMENTS_TABLE	.' a
					WHERE
						'.$db->sql_in_set('a.post_msg_id', $post_ids).'
						AND a.i_poster=1'
				.$thumbnails_only;
				$result=$db->sql_query($sql, $config['ppkbb_topdown_torrents'][6]);
				while($row=$db->sql_fetchrow($result))
				{
					$attachments[$row['post_id']]=$row;
				}
				$db->sql_freeresult($result);
			}
			$i=0;
			foreach($torrents_posters as $k => $v)
			{
				if(isset($attachments[$v['post_id']]))
				{
					$v=array_merge($v, $attachments[$v['post_id']]);
				}
				else
				{
					continue;
				}
				$v['i_height'] ? $i_factor=my_float_val($v['i_width']/$v['i_height']) : $i_factor=0.675;
				$i_width=my_int_val($config['ppkbb_topdown_torrents'][5]*$i_factor);

				$t_title="{$v['topic_title']} - {$user->lang['TORRENT_COMPLETED']}: {$v['times_completed']}, {$user->lang['TORRENT_SEEDERS']}: {$v['seeders']}, {$user->lang['TORRENT_LEECHERS']}: {$v['leechers']}";

				$tdt_image='<img class="tdt_image" rel="tdt'.$i.'" src="'.($v['i_external'] ? $v['real_filename'] : append_sid("{$phpbb_root_path}download/file.$phpEx", 'id=' . $v['attach_id']).($config['ppkbb_topdown_torrents'][9] ? '&amp;t=1' : '')).'" alt="'.$t_title.'" height="'.$config['ppkbb_topdown_torrents'][5].'" width="'.$i_width.'" onError="this.onerror=null;this.src=\''.$phpbb_root_path.'images/tracker/file.png'.'\';" />';

				$topdown_torrents.='
					<div class="panel">
						<a href="'.append_sid($phpbb_root_path.'viewtopic.' . $phpEx . '?f=' . $v['forum_id'] . '&amp;t=' . $v['topic_id']).'" title = "'.$t_title.'">'.$tdt_image.'</a>
					</div>
				';
				$i+=1;
			}
		}
		$cache->put('_ppkbb3cker_tdt_'.$md5_sql, array($topdown_torrents, $tooltip_tdt), $config['ppkbb_topdown_torrents'][6]);
	}
	else
	{
		$tooltip_tdt=$topdown_torrents[1];
		$topdown_torrents=$topdown_torrents[0];
	}

}

header('Content-type: text/html; charset=UTF-8');

if($topdown_torrents)
{
	echo $topdown_torrents;
}
else
{
	echo '
		<div id="panel" style="margin-left:10px;white-space:nowrap;">'.$user->lang['NO_TDT'].'</div>
		<script type="text/javascript">
		// <![CDATA[
		jQuery(document).ready(
			function($)
			{
				//$(".tdtnav_buttons").remove();
				$("#topdown_torrents").animate({\'height\': \'15px\'});
				$("#gallerya'.htmlspecialchars($topdown_torrents_id).'").animate({\'height\': \'15px\'});
			}
		);
		stepcarousel.resetsettings();
		// ]]>
		</script>
	';
}

exit();

?>
