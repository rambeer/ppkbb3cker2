<?php

/**
*
* @package ppkBB3cker
* @version $Id: cssjs.php 1.000 2010-12-18 18:52:00 PPK $
* @copyright (c) 2010 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

error_reporting(0);
@ini_set('register_globals', 0);
@ini_set('magic_quotes_runtime', 0);
@ini_set('magic_quotes_sybase', 0);

function_exists('date_default_timezone_set') && function_exists('date_default_timezone_get') ? date_default_timezone_set(@date_default_timezone_get()) : '';

define('IN_PHPBB', true);

$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);

$no_cache=isset($_GET['no_cache']) && $_GET['no_cache'] ? true : false;
$type=isset($_GET['type']) ? $_GET['type'] : '';
$from_admin=isset($_GET['from_admin']) ? $_GET['from_admin'] : '';

$import_cssjs=isset($_GET['import']) ? true : false;

$tincludedir="{$phpbb_root_path}tracker/tinc/";
$tcachedir="{$phpbb_root_path}cache/";

$ppkbb3cker_addons="{$phpbb_root_path}tracker/addons/";
$data['css'][0]=array(
	$ppkbb3cker_addons.'css/spoiler.css',
	//$ppkbb3cker_addons.'css/easySlider.css',
	$ppkbb3cker_addons.'css/ppkbb3cker.css',
	$ppkbb3cker_addons.'css/prettyPhoto.css',
	$ppkbb3cker_addons.'css/jquery.tooltip.css',
	$ppkbb3cker_addons.'css/tabs.css',
	$ppkbb3cker_addons.'css/fixedMenu.css',
);

$data['css'][1]=array(
	$ppkbb3cker_addons.'css/spoiler.css',
	//$ppkbb3cker_addons.'css/easySlider.css',
	$ppkbb3cker_addons.'css/ppkbb3cker.css',
	$ppkbb3cker_addons.'css/prettyPhoto.css',
	$ppkbb3cker_addons.'css/jquery.tooltip.css',
	$ppkbb3cker_addons.'css/tabs.css',
	$ppkbb3cker_addons.'css/fixedMenu.css',
	$ppkbb3cker_addons.'css/admin.css',
);

$data['js'][0]=array(
	$ppkbb3cker_addons.'js/jquery.tooltip.js',
	$ppkbb3cker_addons.'js/jquery.prettyPhoto.js',
	$ppkbb3cker_addons.'js/jquery.fixedMenu.js',
	$ppkbb3cker_addons.'js/spoiler.js',
	$ppkbb3cker_addons.'js/easySlider.js',
	$ppkbb3cker_addons.'js/tabs.js',
	$ppkbb3cker_addons.'js/ppkbb3cker.js',
);

$data['js'][1]=array(
	$ppkbb3cker_addons.'js/jquery.tooltip.js',
	$ppkbb3cker_addons.'js/jquery.prettyPhoto.js',
	$ppkbb3cker_addons.'js/jquery.fixedMenu.js',
	$ppkbb3cker_addons.'js/spoiler.js',
	//$ppkbb3cker_addons.'js/easySlider.js',
	$ppkbb3cker_addons.'js/tabs.js',
	$ppkbb3cker_addons.'js/ppkbb3cker.js',
);

$addit_cssjs['css']['sm']=array(
	'core' => $ppkbb3cker_addons.'css/smartmenus/sm-core-css.css',
	'blue' => $ppkbb3cker_addons.'css/smartmenus/sm-blue/sm-blue.css',
	'clean' => $ppkbb3cker_addons.'css/smartmenus/sm-clean/sm-clean.css',
	'mint' => $ppkbb3cker_addons.'css/smartmenus/sm-mint/sm-mint.css',
	'simple' => $ppkbb3cker_addons.'css/smartmenus/sm-simple/sm-simple.css',

	'prosilver' => $ppkbb3cker_addons.'css/smartmenus/sm-prosilver/sm-prosilver.css',
	'subsilver2' => $ppkbb3cker_addons.'css/smartmenus/sm-subsilver2/sm-subsilver2.css',
);
$addit_cssjs['js']['sm']=array(
	$ppkbb3cker_addons.'js/jquery.smartmenus.min.js',
);

if(!in_array($type, array('css', 'js')))
{
	if(!$import_cssjs)
	{
		exit();
	}
}

if($import_cssjs)
{
	$import_file=$phpbb_root_path.'install/cssjs_import.'.$phpEx;
	if(@file_exists($import_file))
	{
		include($import_file);
	}
	exit();
}

$cssjs_data=array();
$cssjs_cache=t_getcache('cssjs_cache');
if($cssjs_cache===false)
{
	require($phpbb_root_path . 'config.'.$phpEx);

	if(!in_array($dbms, array('mysql', 'mysqli')))
	{
		err('Only mysql(i) supported');
	}

	$c=@mysql_connect($dbhost.($dbport ? ":{$dbport}" : ''), $dbuser, $dbpasswd);
	if(!$c)
	{
		err('Error connecting database: '.mysql_error().' ['.mysql_errno().']');
	}

	$s=@mysql_select_db($dbname, $c);
	if(!$s)
	{
		err('Error selecting database: '.mysql_error($c));
	}

	my_sql_query("SET NAMES 'utf8'");

	unset($dbpasswd);

	define('TRACKER_CSSJS_TABLE', $table_prefix . 'tracker_cssjs');
	define('TRACKER_CONFIG_TABLE', $table_prefix . 'tracker_config');

	$cssjs_cache=array();
	$sql="SELECT * FROM ".TRACKER_CSSJS_TABLE.' WHERE cssjs_enabled=1 ORDER BY cssjs_order';
	$result=my_sql_query($sql);
	while($row=mysql_fetch_array($result))
	{
		$cssjs_cache[$row['cssjs_type']][]=array('path'=>$row['cssjs_path'], 'include'=>$row['cssjs_include'], 'variable'=>$row['cssjs_variable']);
	}
	mysql_free_result($result);

	include_once("{$tincludedir}tcache.{$phpEx}");

	t_recache('cssjs_cache', $cssjs_cache);
}

if(!isset($cssjs_cache[$type]) || !$cssjs_cache[$type])
{
	exit();
}

foreach($cssjs_cache[$type] as $data)
{
	if(($from_admin && $data['include']==1) || (!$from_admin && $data['include']==-1))
	{
		continue;
	}
	if($data['variable'])
	{
		if(isset($_GET[$type][$data['variable']]) && $_GET[$type][$data['variable']]==-1)
		{
			continue;
		}
		if(!isset($_GET[$type][$data['variable']]) || (isset($_GET[$type][$data['variable']]) && !$_GET[$type][$data['variable']]))
		{
			continue;
		}
	}

	$cssjs_data[]=(strpos($data['path'], 'http')===0 ? '' : $phpbb_root_path).$data['path'];
}

header("Content-Type: ".($type=='css' ? "text/{$type}" : 'application/x-javascript')."; charset=UTF-8");
if($minify==2)
{
	header("Content-Encoding: gzip");
}

$cssjs_code=get_tracker_cssjs($cssjs_data, $type, $no_cache);

// if($minify)
// {
// 	$cssjs_code=minify_cssjs($cssjs_code, $type, $minify);
// }
echo $cssjs_code;

exit();

################################################################################
function get_tracker_cssjs($flist, $type, $no_cache=false)
{
	global $phpbb_root_path, $phpEx, $tincludedir;

	if(is_array($flist) && sizeof($flist))
	{
		$md5_flist=md5(implode('|', $flist));
		$cache_name=$type.'_'.$md5_flist;
		$cache_file="{$phpbb_root_path}cache/data_ppkbb3cker_{$cache_name}.{$phpEx}";
		if(!@file_exists($cache_file) || !$no_cache)
		{
			return write_tracker_cssjs($cache_name, $cache_file, $flist, $type, $no_cache);
		}
		else
		{
			$last_modified=@filemtime($cache_file);

			$etag=md5($cssjs_code);

			$if_none_match=isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : '';
			$if_modified_since=isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? trim($_SERVER['HTTP_IF_MODIFIED_SINCE']) : '';

			if(@strtotime($if_modified_since)>=$last_modified || $if_none_match==$etag)
			{
				header('HTTP/1.0 304 Not Modified');
				header('Cache-Control: max-age=86400, must-revalidate');
				exit();
			}

			header("Last-Modified: ".gmdate("D, d M Y H:i:s", $last_modified)." GMT");
			header("Etag: {$etag}");
			header('Cache-Control: max-age=86400, must-revalidate');

			$cssjs_code=t_getcache($cache_name, 0, true);

			return $cssjs_code;

		}
	}

	return '';
}

function write_tracker_cssjs($cache_name, $cache_file, $flist, $type, $no_cache=false)
{
	global $phpbb_root_path, $phpEx, $tincludedir;

	$cssjs_code='';
	foreach($flist as $fname)
	{
		if(strpos($fname, 'http')===0)
		{
			$cssjs=@file_get_contents($fname);
			$cssjs ? $cssjs_code.=$cssjs."\n" : '';
		}
		else if(@file_exists($fname))
		{
			$cssjs=@file($fname);
			$cssjs ? $cssjs_code.=implode('', $cssjs)."\n" : '';
		}
	}

	if(!$no_cache)
	{
		include_once("{$tincludedir}tcache.{$phpEx}");

		t_recache($cache_name, $cssjs_code, true);

		$last_modified=@filemtime($cache_file);

		$etag=md5($cssjs_code);

		$if_none_match=isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : '';
		$if_modified_since=isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? trim($_SERVER['HTTP_IF_MODIFIED_SINCE']) : '';

		if(@strtotime($if_modified_since)>=$last_modified || $if_none_match==$etag)
		{
			header('HTTP/1.0 304 Not Modified');
			header('Cache-Control: max-age=86400, must-revalidate');
			exit();
		}

		header("Last-Modified: ".gmdate("D, d M Y H:i:s", $last_modified)." GMT");
		header("Etag: {$etag}");
		header('Cache-Control: max-age=86400, must-revalidate');

	}

	return $cssjs_code;

}

function my_sql_query($query, $log=true)
{
	global $c;

	$result=@mysql_query($query, $c);

	if(!$result)
	{
		if($log)
		{
			global $tincludedir, $userid, $phpEx;

			include($tincludedir.'tlog.'.$phpEx);
		}

		err('Unknown sql error');
// 		err('Unknown sql error: ('.mysql_errno($c).') '.mysql_error($c));

		mysql_close($c);
	}

	return $result;
}

function err($str)
{
	global $c;

	header('Content-type: text/html; charset=UTF-8');
	echo $str;

	if($c)
	{
		mysql_close($c);
	}

	exit();
}
function t_getcache($cname, $ctime=0, $non_php=false, $var='')
{
	global $tcachedir, $phpEx;

	$cache_data=array();

	$f_name="{$tcachedir}data_ppkbb3cker_{$cname}.{$phpEx}";
	if(@file_exists($f_name))
	{
		if($ctime)
		{
			$m_time=@filemtime($f_name);
			if($m_time && time()-$m_time > $ctime)
			{
				return false;
			}
		}

		if($non_php)
		{
			$cache_data=@file($f_name);
			if(sizeof($cache_data))
			{
				unset($cache_data[0]);
			}
			return implode("\n", $cache_data);
		}
		else
		{
			include($f_name);

			return $var ? $$var : $cache_data;
		}

	}

	return false;
}

?>
