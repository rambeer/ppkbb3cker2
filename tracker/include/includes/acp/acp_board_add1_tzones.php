<?php
/**
*
* @package ppkBB3cker
* @version $Id: acp_board_add1_tzones.php 1.000 2009-08-13 12:03:00 PPK $
* @copyright (c) 2009 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$user->add_lang('mods/acp/ppkbb3cker_tzones');
$zones_title='ACP_TRACKER_ZONES';

if(request_var('submit', '') && @$_POST['z_name'])
{
	$d_zones=array();
	$updated=0;
	foreach($_POST['z_name'] as $k=>$v)
	{
		if(STRIP)
		{
			$_POST['z_name'][$k]=stripslashes($_POST['z_name'][$k]);
		}

		$_POST['z_name'][$k]=utf8_normalize_nfc($_POST['z_name'][$k]);
		$zone_type=(@$_POST['z_type'][$k]=='w' || @$_POST['z_type'][$k]=='b' || @$_POST['z_type'][$k]=='r') ? @$_POST['z_type'][$k] : 'a';
		if(@$_POST['z_delete'][$k])
		{
			$d_zones[]=my_int_val($k);
		}
		else if ($k==0 && @$_POST['z_name'][$k]!='')
		{
			$sql='INSERT INTO '.TRACKER_ZONES_NAME." (zone_name, zone_type, zenabled) VALUES('".$db->sql_escape($_POST['z_name'][$k])."', '{$zone_type}', '".(@$_POST['zenabled'][$k] ? 1 : 0)."')";
			$result=$db->sql_query($sql);
		}
		else if(@$_POST['z_name'][$k]!='')
		{
			$sql='UPDATE '.TRACKER_ZONES_NAME." SET zone_name='".$db->sql_escape($_POST['z_name'][$k])."', zone_type='{$zone_type}', zenabled='".(@$_POST['zenabled'][$k] ? 1 : 0)."' WHERE id='".my_int_val($k)."'";
			$result=$db->sql_query($sql);
			$updated=1;
		}
	}

	if($d_zones)
	{
		$where_zone_id=$db->sql_in_set('zone_id', $d_zones);

		$sql='DELETE FROM '.TRACKER_ZONES_NAME.' WHERE '.$db->sql_in_set('id', $d_zones);
		$result=$db->sql_query($sql);

		$sql='DELETE FROM '.TRACKER_ZONES_ADDR." WHERE {$where_zone_id}";
		$result=$db->sql_query($sql);

		$sql='DELETE FROM '.TRACKER_RTRACK_TABLE." WHERE {$where_zone_id}";
		$result=$db->sql_query($sql);
	}
}

if(request_var('submit', '') && @$_POST['addr_start']  && ($tzone=request_var('tzone', 0)))
{
	$d_addr=array();
	foreach($_POST['addr_start'] as $k=>$v)
	{
		if(@$_POST['addr_delete'][$k])
		{
			$d_addr[]=my_int_val($k);
		}
		else if ($k==0 && @$_POST['addr_start'][$k]!='' && @$_POST['addr_end'][$k]!='')
		{
			$addr_start=check_ip(@$_POST['addr_start'][$k]);
			$addr_end=check_ip(@$_POST['addr_end'][$k]);
			if($addr_start && $addr_end)
			{
				$sql='INSERT INTO '.TRACKER_ZONES_ADDR."
					(zone_id, addr_start, addr_end, zenabled)
						VALUES('{$tzone}', '{$addr_start}', '{$addr_end}', '".(@$_POST['zenabled'][$k] ? 1 : 0)."')";
				$result=$db->sql_query($sql);
			}
		}
		else if(@$_POST['addr_start'][$k]!='' && @$_POST['addr_end'][$k]!='')
		{
			$addr_start=check_ip(@$_POST['addr_start'][$k]);
			$addr_end=check_ip(@$_POST['addr_end'][$k]);
			if($addr_start && $addr_end)
			{
				$sql='UPDATE '.TRACKER_ZONES_ADDR." SET addr_start='{$addr_start}', addr_end='{$addr_end}', zenabled='".(@$_POST['zenabled'][$k] ? 1 : 0)."' WHERE id='".my_int_val($k)."'";
				$result=$db->sql_query($sql);
			}
		}
	}
	if($d_addr)
	{
		$sql='DELETE FROM '.TRACKER_ZONES_ADDR.' WHERE '.$db->sql_in_set('id', $d_addr);
		$result=$db->sql_query($sql);
	}
	$this->u_action=append_sid("{$phpbb_admin_path}index.$phpEx", 'i=board&amp;mode=tzones&amp;view_addr=1&amp;tzone='.$tzone);
}

$rtracker_id=request_var('rtrack_id', array(0=>''));

if(request_var('submit', '') && $rtracker_id && ($tzone=request_var('tzone', 0)))
{

	$rtracker_url=request_var('rtrack_url', array(0=>''), true);
	$rtracker_delete=request_var('rtrack_delete', array(0=>''));
	$rtracker_enabled=request_var('rtrack_enabled', array(0=>''));

	$this->u_action=append_sid("{$phpbb_admin_path}index.$phpEx", 'i=board&amp;mode=tzones&amp;add_new=1&amp;view_rtrack=1&amp;tzone='.$tzone);

	$exists_tracker=array();
	$sql="SELECT rs.id, rs.rtracker_md5, rt.rtracker_id rtrack FROM ".TRACKER_RTRACKERS_TABLE." rs LEFT JOIN ".TRACKER_RTRACK_TABLE." rt ON(rs.id=rt.rtracker_id AND rt.rtracker_type='s' AND rt.user_torrent_zone='{$tzone}') WHERE ".$db->sql_in_set('rs.rtracker_md5', array_map('md5', $rtracker_url));
	$result=$db->sql_query($sql);
	while($row=$db->sql_fetchrow($result))
	{
		$exists_tracker[$row['rtracker_md5']]=array('id'=>$row['id'], 'rtrack'=>$row['rtrack']);
	}
	$db->sql_freeresult($result);

	$d_rtrack=array();
	foreach($rtracker_id as $k=>$v)
	{
		$rtrack_url=isset($rtracker_url[$k]) ? $rtracker_url[$k] : '';
		$rtrack_delete=isset($rtracker_delete[$k]) ? $rtracker_delete[$k] : '';
		$rtrack_enabled=isset($rtracker_enabled[$k]) ? $rtracker_enabled[$k] : '';
		$rtrack_id=my_int_val($k);

		if($rtrack_url==='')
		{
			continue;
		}

		if($rtrack_delete)
		{
			$d_rtrack[]=$rtrack_id;
		}
		else
		{

			$rtrack_url=utf8_normalize_nfc($rtrack_url);
			if((!preg_match('#^(https?|udp):\/\/(\w+|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})#', $rtrack_url) || strlen($rtrack_url) > 255))
			{
				trigger_error($user->lang['INVALID_RTRACK_URL'].': '.htmlspecialchars($rtrack_url).sprintf($user->lang['RTRACK_BACK'], $this->u_action));
			}

			$rtrack_md5=md5($rtrack_url);
			if(isset($exists_tracker[$rtrack_md5]))
			{
				$rtrack_id=$exists_tracker[$rtrack_md5]['id'];

				$rtrack_new=false;
			}
			else
			{
				$sql="INSERT INTO ".TRACKER_RTRACKERS_TABLE."(rtracker_url, rtracker_md5) VALUES('".$db->sql_escape($rtrack_url)."', '{$rtrack_md5}')";
				$result=$db->sql_query($sql);
				$rtrack_id=$db->sql_nextid();

				$rtrack_new=true;
			}

			if($rtrack_new || !$exists_tracker[$rtrack_md5]['rtrack'])
			{
				$sql='INSERT INTO '.TRACKER_RTRACK_TABLE."
					(rtracker_id, rtracker_enabled, rtracker_type, user_torrent_zone)
						VALUES(
						'".$rtrack_id."',
						'".($rtrack_enabled ? 1 : 0)."',
						's',
						'{$tzone}'
				)";
				$result=$db->sql_query($sql);
			}
			else
			{
				$sql='UPDATE '.TRACKER_RTRACK_TABLE." SET
					rtracker_enabled='".($rtrack_enabled ? 1 : 0)."'
					WHERE rtracker_id='".$rtrack_id."' AND rtracker_type='s' AND user_torrent_zone='{$tzone}'";
				$result=$db->sql_query($sql);
			}
		}
	}
	if($d_rtrack)
	{
		$sql='DELETE FROM '.TRACKER_RTRACK_TABLE.' WHERE '.$db->sql_in_set('rtracker_id', $d_rtrack);
		$result=$db->sql_query($sql);
	}

}

if(request_var('view_addr', 0))
{
	$tzone=request_var('tzone', 0);
	$zones_title='ACP_TRACKER_ADDR';

	if($tzone)
	{
		$sql='SELECT zone_name, zone_type, zenabled FROM '.TRACKER_ZONES_NAME." WHERE id='{$tzone}'";
		$result=$db->sql_query($sql);
		$zone_name=$db->sql_fetchrow($result);
		$db->sql_freeresult($result);
		if($zone_name['zone_type']=='w')
		{
			$l_zone_type=$user->lang['ZONE_WHITE'];
		}
		else if($zone_name['zone_type']=='b')
		{
			$l_zone_type=$user->lang['ZONE_BLACK'];
		}
		else if($zone_name['zone_type']=='r')
		{
			$l_zone_type=$user->lang['ZONE_BLOCK'];
		}
		else
		{
			$l_zone_type='';
		}
	}
	if(request_var('add_new', ''))
	{
		$template->assign_block_vars('addrs', array(
			'COUNT'	=> 0,
			'START'	=> '',
			'END'	=> '',
			)
		);
		$template->assign_vars(array(
			'ZONE_NAME' => htmlspecialchars($zone_name['zone_name']),
			'ZONE_TYPE' => $l_zone_type,
			'S_HIDDEN_FIELDS'=>'<input type="hidden" name="view_addr" value="1" />
						<input type="hidden" name="tzone" value="'.$tzone.'" />',
			)
		);
		$template->assign_vars(array(
			'S_NEW_ADDR'	=> true,
			)
		);
	}
	else
	{
		if($tzone)
		{
			$sql='SELECT
				id, addr_start, addr_end, zenabled
					FROM '.TRACKER_ZONES_ADDR."
						WHERE zone_id='{$tzone}'";
			$result=$db->sql_query($sql);
			while($row=$db->sql_fetchrow($result))
			{
				$template->assign_block_vars('addrs', array(
					'COUNT'	=> $row['id'],
					'START'	=> $row['addr_start'],
					'END'	=> $row['addr_end'],
					'ENABLED' => $row['zenabled'] ? ' checked="checked"' : '',
					)
				);
			}
			$db->sql_freeresult($result);
		}
		$template->assign_vars(array(
			'ZONE_NAME' => htmlspecialchars($zone_name['zone_name']),
			'ZONE_TYPE' => $l_zone_type,
			'S_HIDDEN_FIELDS'=>'<input type="hidden" name="view_addr" value="1" />
						<input type="hidden" name="tzone" value="'.$tzone.'" />',
			)
		);
		$template->assign_vars(array(
			'S_VIEW_ADDR'	=> true,
			)
		);
	}
}
else if(request_var('view_rtrack', 0))
{
	$tzone=request_var('tzone', 0);
	$zones_title='ACP_TRACKER_RTRACK';
	if($tzone)
	{
		$sql='SELECT zone_name, zone_type, zenabled FROM '.TRACKER_ZONES_NAME." WHERE id='{$tzone}'";
		$result=$db->sql_query($sql);
		$zone_name=$db->sql_fetchrow($result);
		$db->sql_freeresult($result);
		if($zone_name['zone_type']=='w')
		{
			$l_zone_type=$user->lang['ZONE_WHITE'];
		}
		else if($zone_name['zone_type']=='b')
		{
			$l_zone_type=$user->lang['ZONE_BLACK'];
		}
		else if($zone_name['zone_type']=='r')
		{
			$l_zone_type=$user->lang['ZONE_BLOCK'];
		}
		else
		{
			$l_zone_type='';
		}
	}
	if(request_var('add_new', ''))
	{
		$template->assign_block_vars('rtracks', array(
			'COUNT'	=> 0,
			'URL'	=> '',
			'ENABLED' => '',

			)
		);
		$template->assign_vars(array(
			'ZONE_NAME' => htmlspecialchars($zone_name['zone_name']),
			'ZONE_TYPE' => $l_zone_type,
			'S_HIDDEN_FIELDS'=>'<input type="hidden" name="view_rtrack" value="1" />
						<input type="hidden" name="tzone" value="'.$tzone.'" />',
			)
		);
		$template->assign_vars(array(
			'S_NEW_RTRACK'	=> true,
			)
		);
	}
	else
	{
		if($tzone)
		{
			$sql='SELECT rt.rtracker_remote rtrack_remote, rt.rtracker_forb rtrack_forb, rt.forb_type, rt.rtracker_enabled rtrack_enabled, rs.id, rs.rtracker_url rtrack_url FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND rt.user_torrent_zone='{$tzone}' AND rt.rtracker_type='s' ORDER BY rt.user_torrent_zone";
			$result=$db->sql_query($sql);
			while($row=$db->sql_fetchrow($result))
			{
				$template->assign_block_vars('rtracks', array(
					'COUNT'	=> $row['id'],
					'URL'	=> htmlspecialchars($row['rtrack_url']),
					'ENABLED' => $row['rtrack_enabled'] ? ' checked="checked"' : '',
					)
				);
			}
			$db->sql_freeresult($result);
		}
		$template->assign_vars(array(
			'ZONE_NAME' => htmlspecialchars($zone_name['zone_name']),
			'ZONE_TYPE' => $l_zone_type,
			'S_HIDDEN_FIELDS'=>'<input type="hidden" name="view_rtrack" value="1" />
						<input type="hidden" name="tzone" value="'.$tzone.'" />',
			)
		);
		$template->assign_vars(array(
			'S_VIEW_RTRACK'	=> true,
			)
		);
	}
}
else
{
	if(request_var('add_new', ''))
	{
		$template->assign_block_vars('zones', array(
			'COUNT'	=> 0,
			'NAME'	=> '',
			)
		);
		$template->assign_vars(array(
			'S_NEW_ZONES'	=> true,
			'S_HIDDEN_FIELDS'=>'<input type="hidden" name="add_zones" value="1" />',
			)
		);
	}
	else
	{
		$sql='SELECT * FROM '.TRACKER_ZONES_NAME.' ORDER BY zone_type';
		$result=$db->sql_query($sql);
		while($row=$db->sql_fetchrow($result))
		{
			$template->assign_block_vars('zones', array(
				'COUNT'	=> $row['id'],
				'NAME'	=> htmlspecialchars($row['zone_name']),
				'A_VIEW_ADDR'	=> append_sid("{$phpbb_admin_path}index.$phpEx", 'i=board&amp;mode=tzones&amp;view_addr=1&amp;tzone='.$row['id']),
				'A_VIEW_RTRACK' => append_sid("{$phpbb_admin_path}index.$phpEx", 'i=board&amp;mode=tzones&amp;view_rtrack=1&amp;tzone='.$row['id']),
				'TYPE'=>'
					<select name="z_type['.$row['id'].']">
						<option value="a"></option>
						<option value="w"'.($row['zone_type']=='w' ? ' selected="selected"' : '').'>'.$user->lang['ZONE_WHITE'].'</option>
						<option value="b"'.($row['zone_type']=='b' ? ' selected="selected"' : '').'>'.$user->lang['ZONE_BLACK'].'</option>
						<option value="r"'.($row['zone_type']=='r' ? ' selected="selected"' : '').'>'.$user->lang['ZONE_BLOCK'].'</option>
					</select>
					',
				'ENABLED' => $row['zenabled'] ? ' checked="checked"' : '',
				)
			);
		}
		$db->sql_freeresult($result);
		$template->assign_vars(array(
			'S_VIEW_ZONES'	=> true,
			'S_HIDDEN_FIELDS'=>'<input type="hidden" name="add_zones" value="1" />',
			)
		);
	}
}

$display_vars = array(
	'title'	=> $zones_title,
	'vars'	=> array(
		'legend1'				=> 'ACP_TRACKER_ZONES_SETTINGS',
	)
);

$template->assign_vars(array(
	'S_ZONES_INC'	=> true,
	)
);
?>
