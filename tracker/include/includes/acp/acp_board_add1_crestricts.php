<?php
/**
*
* @package ppkBB3cker
* @version $Id: acp_board_add1_crestricts.php 1.000 2009-11-15 12:34:00 PPK $
* @copyright (c) 2009 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$user->add_lang('mods/acp/ppkbb3cker_crestricts');
$cresricts_types=array(1=>$user->lang['CRESTRICTS_PORT'], 2=>$user->lang['CRESTRICTS_UA'], 3=>$user->lang['CRESTRICTS_PEERID']);
$crselect=request_var('cr_select', 0);
$crestricts_title='ACP_TRACKER_CRESTRICTS';

if($crselect && @$cresricts_types[$crselect])
{
	$result='';
	$add_crs=request_var('add_crs', '');
	if(request_var('add_new', ''))
	{
		if($crselect==1)
		{
			$template->assign_block_vars('crs', array(
				'ID'	=> 0,
				'RSTART' => '',
				'REND'	=> '',
				)
			);
			$template->assign_vars(array(
				'NEW_CR'	=> true,
				)
			);
		}
		else if($crselect==2 || $crselect==3)
		{
			$template->assign_block_vars('crs', array(
				'ID'	=> 0,
				'RSTART' => '',
				'TYPE'	=> '',
				'CHECKED'=>'',
				)
			);
			$template->assign_vars(array(
				'NEW_CR'	=> true,
				)
			);
		}
	}
	else
	{
		$sql='SELECT * FROM '.TRACKER_CRESTRICTS_TABLE." WHERE rid='{$crselect}'";
		$result=$db->sql_query($sql);
	}
	if($crselect==1)
	{
		$crestricts_title='CRESTRICTS_PORT';
		if($add_crs && sizeof(@$_POST['cr_start']))
		{
			$d_cr=array();
			foreach($_POST['cr_start'] as $k=>$v)
			{
				if(@$_POST['cr_delete'][$k])
				{
					$d_cr[]=my_int_val($k);
				}
				else if ($k==0 && @$_POST['cr_start'][$k]!='' && @$_POST['cr_end'][$k]!='')
				{
					$cr_start=@$_POST['cr_start'][$k] > 0 && @$_POST['cr_start'][$k] < 65536 ? intval(@$_POST['cr_start'][$k]) : 0;
					$cr_end=@$_POST['cr_end'][$k] > 0 && @$_POST['cr_end'][$k] < 65536 ? intval(@$_POST['cr_end'][$k]) : 0;
					if($cr_start && $cr_end)
					{
						$sql='INSERT INTO '.TRACKER_CRESTRICTS_TABLE."
							(rid, rstart, rend, renabled)
								VALUES('{$crselect}', '{$cr_start}', '{$cr_end}', '".(@$_POST['renabled'][$k] ? 1 : 0)."')";
						$result=$db->sql_query($sql);
					}
				}
				else if(@$_POST['cr_start'][$k]!='' && @$_POST['cr_end'][$k]!='')
				{
					$cr_start=@$_POST['cr_start'][$k] > 0 && @$_POST['cr_start'][$k] < 65536 ? intval(@$_POST['cr_start'][$k]) : 0;
					$cr_end=@$_POST['cr_end'][$k] > 0 && @$_POST['cr_end'][$k] < 65536 ? intval(@$_POST['cr_end'][$k]) : 0;
					if($cr_start && $cr_end)
					{
						$sql='UPDATE '.TRACKER_CRESTRICTS_TABLE." SET rstart='{$cr_start}', rend='{$cr_end}', renabled='".(@$_POST['renabled'][$k] ? 1 : 0)."' WHERE id='".my_int_val($k)."'";
						$result=$db->sql_query($sql);
					}
				}
			}
			if($d_cr)
			{
				$sql='DELETE FROM '.TRACKER_CRESTRICTS_TABLE.' WHERE '.$db->sql_in_set('id', $d_cr);
				$result=$db->sql_query($sql);
			}
			$this->u_action=append_sid("{$phpbb_admin_path}index.$phpEx", 'i=board&amp;mode=crestricts&amp;cr_select='.$crselect);
			@unlink($phpbb_root_path.'cache/data_ppkbb3cker_crestrport.'.$phpEx);
		}
		else if($result)
		{
			while($row=$db->sql_fetchrow($result))
			{
				$template->assign_block_vars('crs', array(
					'ID'	=> $row['id'],
					'RSTART' => $row['rstart'],
					'REND'	=> $row['rend'],
					'ENABLED' => $row['renabled'] ? ' checked="checked"' : '',
					)
				);
			}
			$db->sql_freeresult($result);
		}
		$template->assign_vars(array(
			'CR_PORT'	=> true,
			)
		);
	}
	else if($crselect==2 || $crselect==3)
	{
		$crestricts_title=$crselect==2 ? 'CRESTRICTS_UA' : 'CRESTRICTS_PEERID';
		if($add_crs && sizeof(@$_POST['cr_start']))
		{
			$d_cr=array();
			foreach($_POST['cr_start'] as $k=>$v)
			{
				if(STRIP)
				{
					$_POST['cr_start'][$k]=stripslashes($_POST['cr_start'][$k]);
				}

				$_POST['cr_start'][$k]=utf8_normalize_nfc($_POST['cr_start'][$k]);
				@$_POST['cr_type'][$k]=in_array(@$_POST['cr_type'][$k], array('r', 's', 'i')) ? $_POST['cr_type'][$k] : 's';
				if(@$_POST['cr_delete'][$k])
				{
					$d_cr[]=my_int_val($k);
				}
				else if ($k==0 && @$_POST['cr_start'][$k]!='')
				{
					$cr_start=$db->sql_escape(@$_POST['cr_start'][$k]);
					$cr_type=@$_POST['cr_type'][$k];
					$sql='INSERT INTO '.TRACKER_CRESTRICTS_TABLE."
							(rid, rstart, rtype, renabled)
								VALUES('{$crselect}', '{$cr_start}', '{$cr_type}', '".(@$_POST['renabled'][$k] ? 1 : 0)."')";
					$result=$db->sql_query($sql);
				}
				else if(@$_POST['cr_start'][$k]!='')
				{
					$cr_start=$db->sql_escape(@$_POST['cr_start'][$k]);
					$cr_type=@$_POST['cr_type'][$k];
					$sql='UPDATE '.TRACKER_CRESTRICTS_TABLE." SET rstart='{$cr_start}', rtype='{$cr_type}', renabled='".(@$_POST['renabled'][$k] ? 1 : 0)."' WHERE id='".my_int_val($k)."'";
					$result=$db->sql_query($sql);
				}
			}
			if($d_cr)
			{
				$sql='DELETE FROM '.TRACKER_CRESTRICTS_TABLE.' WHERE '.$db->sql_in_set('id', $d_cr);
				$result=$db->sql_query($sql);
			}
			$this->u_action=append_sid("{$phpbb_admin_path}index.$phpEx", 'i=board&amp;mode=crestricts&amp;cr_select='.$crselect);
			if($crselect==2)
			{
				@unlink($phpbb_root_path.'cache/data_ppkbb3cker_crestrua.'.$phpEx);
			}
			else if($crselect==3)
			{
				@unlink($phpbb_root_path.'cache/data_ppkbb3cker_crestrpeerid.'.$phpEx);
			}
		}
		else if($result)
		{
			while($row=$db->sql_fetchrow($result))
			{
				$template->assign_block_vars('crs', array(
					'ID'	=> $row['id'],
					'RSTART' => $row['rstart'],
					'TYPE'	=> $row['rend'],
					'RCHECKED'=>$row['rtype']=='r' ? ' checked="checked"' : '',
					'SCHECKED'=>$row['rtype']=='s' ? ' checked="checked"' : '',
					'ICHECKED'=>$row['rtype']=='i' ? ' checked="checked"' : '',
					'ENABLED' => $row['renabled'] ? ' checked="checked"' : '',

					)
				);
			}
			$db->sql_freeresult($result);
		}
		$template->assign_vars(array(
			'CR_UA'	=> $crselect==2 ? true : false,
			'CR_PEERID'	=> $crselect==3 ? true : false,
			)
		);
	}

	$template->assign_vars(array(
		'CR_NAME'	=> htmlspecialchars(@$cresricts_types[$crselect]),
		'S_HIDDEN_FIELDS'=>'<input type="hidden" name="add_crs" value="1" >
					<input type="hidden" name="cr_select" value="'.$crselect.'" >',
		'S_VIEW_CRS'	=> true,
		)
	);
}
else
{
	$sql='SELECT rid, COUNT(rid) count FROM '.TRACKER_CRESTRICTS_TABLE." GROUP BY rid";
	$result=$db->sql_query($sql);
	$cresricts_count=array();
	while($row=$db->sql_fetchrow($result))
	{
		$cresricts_count[$row['rid']]=$row['count'];
	}
	$db->sql_freeresult($result);
	foreach($cresricts_types as $cr_key=>$cr_name)
	{
		$template->assign_block_vars('crestricts', array(
			'NAME'	=> htmlspecialchars($cr_name),
			'A_VIEW_CR'	=> append_sid("{$phpbb_admin_path}index.$phpEx", 'i=board&amp;mode=crestricts&amp;cr_select='.$cr_key),
			'COUNT'	=> intval(@$cresricts_count[$cr_key]),
			)
		);
	}

	$template->assign_vars(array(
		'S_VIEW_CRESTRICTS'	=> true,
		'S_TRACKER_NOBUTT' => true,

		)
	);
}

$display_vars = array(
	'title'	=> $crestricts_title,
	'vars'	=> array(
		'legend1'				=> 'ACP_TRACKER_CRESTRICTS_SETTINGS',
	)
);

$template->assign_vars(array(
	'S_CRESTRICTS_INC'	=> true,
	)
);
?>
