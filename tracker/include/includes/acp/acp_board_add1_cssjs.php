<?php
/**
*
* @package ppkBB3cker
* @version $Id: acp_board_add1_cssjs.php 1.000 2018-03-06 13:54:00 PPK $
* @copyright (c) 2018 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$user->add_lang('mods/acp/ppkbb3cker_cssjs');

$cssjs_title='ACP_TRACKER_CSSJS';

$cssjs_path=request_var('cssjs_path', array(0=>''), true);
$cssjs_variable=request_var('cssjs_variable', array(0=>''), true);
$cssjs_order=request_var('cssjs_order', array(0=>0));
$cssjs_include=request_var('cssjs_include', array(0=>0));
$cssjs_enabled=request_var('cssjs_enabled', array(0=>0));
$cssjs_delete=request_var('cssjs_delete', array(0=>0));

$submit=request_var('submit', '');
$add_cssjs=request_var('add_cssjs', '');
$view_cssjs=request_var('view_cssjs', '');

$valid_cssjs=array('css', 'js');
in_array($view_cssjs, $valid_cssjs) ? '' : $view_cssjs='';

if($submit && $add_cssjs)
{
	$delete_cssjs=array();
	foreach($cssjs_path as $k=>$v)
	{
		if(isset($cssjs_delete[$k]))
		{
			$delete_cssjs[]=my_int_val($k);
		}
		else
		{
			$cssjs_variable[$k]=preg_replace('#[^a-zA-Z0-9_-]+#', '', $cssjs_variable[$k]);
			if($k==0 && $cssjs_path[$k]!=='')
			{
				$sql='INSERT INTO '.TRACKER_CSSJS_TABLE." (cssjs_path, cssjs_include, cssjs_order, cssjs_variable, cssjs_enabled, cssjs_type) VALUES(
					'".$db->sql_escape($cssjs_path[$k])."',
					'".(in_array($cssjs_include[$k], array(0, 1, -1)) ? $cssjs_include[$k] : 0)."',
					'".($cssjs_order[$k] < 256 ? $cssjs_order[$k] : 0)."',
					'".$db->sql_escape($cssjs_variable[$k])."',
					'".(isset($cssjs_enabled[$k]) ? 1 : 0)."',
					'".$view_cssjs."'
				)";
				$result=$db->sql_query($sql);
			}
			else if($cssjs_path[$k]!=='')
			{
				$sql='UPDATE '.TRACKER_CSSJS_TABLE." SET
					cssjs_path='".$db->sql_escape($cssjs_path[$k])."',
					cssjs_include='".(in_array($cssjs_include[$k], array(0, 1, -1)) ? $cssjs_include[$k] : 0)."',
					cssjs_order='".($cssjs_order[$k] < 256 ? $cssjs_order[$k] : 0)."',
					cssjs_variable='".$db->sql_escape($cssjs_variable[$k])."',
					cssjs_enabled='".(isset($cssjs_enabled[$k]) ? 1 : 0)."'
					WHERE id='{$k}'";
				$result=$db->sql_query($sql);
			}
		}
	}
	if($delete_cssjs)
	{
		$sql='DELETE FROM '.TRACKER_CSSJS_TABLE.' WHERE '.$db->sql_in_set('id', $delete_cssjs);
		$result=$db->sql_query($sql);
	}
}

$sort_cssjs=request_var('sort_cssjs', 0);
$cssjs=request_var('cssjs', array(0=>0));
if($view_cssjs && $sort_cssjs && $cssjs)
{

	$sql='SELECT cssjs_type FROM '.TRACKER_CSSJS_TABLE." WHERE cssjs_type='{$view_cssjs}'";
	$result=$db->sql_query($sql);
	if($db->sql_fetchrow($result))
	{
		foreach($cssjs as $k => $v)
		{
			$k=my_int_val($k);
			$v=my_int_val($v);
			if($k && $v)
			{
				$db->sql_query("UPDATE ".TRACKER_CSSJS_TABLE." SET cssjs_order='{$k}' WHERE id='{$v}' AND cssjs_type='{$view_cssjs}'");
			}
		}
		exit($user->lang['CSSJS_SORT_SUCCESS']);
	}

}
else if($view_cssjs)
{

	$cssjs_title='ACP_TRACKER_CSSJS';
	$form_cssjs=request_var('form_cssjs', '');
	if($form_cssjs)
	{
		$sql='SELECT cssjs_path FROM '.TRACKER_CSSJS_TABLE." WHERE cssjs_type='{$view_cssjs}'";
		$result=$db->sql_query($sql);
		$cssjs_exists=array();
		while($row=$db->sql_fetchrow($result))
		{
			$cssjs_exists[$row['cssjs_path']]=1;
		}
		$db->sql_freeresult($result);

		$template->assign_block_vars('cssjs', array(
			'ID'	=> 0,
			'PATH'	=> '',
			'VARIABLE'	=> '',
			'ORDER'	=> 255,
			'INCLUDE'	=> cssjs_include(0),
			'ENABLED'	=> false,
			)
		);

		$template->assign_vars(array(
			'CSSJS_TYPE' => $user->lang['CSSJS_'.strtoupper($view_cssjs)],
			'S_HIDDEN_FIELDS'=>'<input type="hidden" name="add_cssjs" value="1" /><input type="hidden" name="view_cssjs" value="'.$view_cssjs.'" />',

			'S_NEW_CSSJS'	=> true,
			'S_FORM_CSSJS'	=> true,

			)
		);

	}
	else
	{

		$sql='SELECT * FROM '.TRACKER_CSSJS_TABLE." WHERE cssjs_type='{$view_cssjs}' ORDER BY cssjs_order";
		$result=$db->sql_query($sql);
		while($row=$db->sql_fetchrow($result))
		{

			$template->assign_block_vars('cssjs', array(
				'ID'	=> $row['id'],
				'PATH'	=> $row['cssjs_path'],
				'VARIABLE'	=> $row['cssjs_variable'],
				'ORDER'	=> $row['cssjs_order'],
				'INCLUDE'	=> cssjs_include($row['id'], $row['cssjs_include']),
				'ENABLED'	=> $row['cssjs_enabled'] ? true : false,
				)
			);
		}
		$db->sql_freeresult($result);

		$template->assign_vars(array(
			'CSSJS_TYPE' => isset($user->lang['CSSJS_'.strtoupper($view_cssjs)]) ? $user->lang['CSSJS_'.strtoupper($view_cssjs)] : $view_cssjs,
			'S_HIDDEN_FIELDS'=>'<input type="hidden" name="add_cssjs" value="1" /><input type="hidden" name="view_cssjs" value="'.$view_cssjs.'" />',

			'S_NEW_CSSJS'	=> true,
			'S_SORT_CSSJS' => append_sid("{$phpbb_admin_path}index.$phpEx", "i=board&mode=cssjs&sort_cssjs=1&view_cssjs={$view_cssjs}", false),
			)
		);
	}
}
else
{

	$cssjs_count=array();

	$sql='SELECT COUNT(*) cssjs_count, cssjs_type FROM '.TRACKER_CSSJS_TABLE.' GROUP BY cssjs_type';
	$result=$db->sql_query($sql);
	while($row=$db->sql_fetchrow($result))
	{
		$cssjs_count[$row['cssjs_type']]=(int) $row['cssjs_count'];

	}
	$db->sql_freeresult($result);

	foreach($valid_cssjs as $v)
	{
		$template->assign_block_vars('cssjs_type', array(
			'CSSJS_NAME' => isset($user->lang['CSSJS_'.strtoupper($v)]) ? $user->lang['CSSJS_'.strtoupper($v)] : $v,
			'U_CSSJS_EDIT' => append_sid("{$phpbb_admin_path}index.$phpEx", 'i=board&amp;mode=cssjs&amp;view_cssjs='.$v),
			'CSSJS_COUNT' => isset($cssjs_count[$v]) ? $cssjs_count[$v] : 0,
			)
		);
	}

	$template->assign_vars(array(
		'S_VIEW_CSSJS'	=> false,

		'S_TRACKER_NOBUTT' => true,
		)
	);

}

$display_vars = array(
	'title'	=> $cssjs_title,
	'vars'	=> array(
		'legend1'				=> 'ACP_TRACKER_CSSJS_SETTINGS',
	)
);

$template->assign_vars(array(
	'S_CSSJS_INC'	=> true,
	)
);

//##############################################################################
function cssjs_include($key, $value=0)
{
	global $user;

	$include_options=array(0=>$user->lang['INCLUDE_EVERYWHERE'], 1=>$user->lang['INCLUDE_NONADMIN'], -1=>$user->lang['INCLUDE_ADMIN']);

	$cssjs_include='<select name="cssjs_include['.$key.']">';
	foreach($include_options as $k=>$v)
	{
		$cssjs_include.='<option value="'.$k.'"'.($value==$k ? ' selected="selected"' : '').'>'.$v.'</option>';
	}
	$cssjs_include.='</select>';

	return $cssjs_include;

}
?>
