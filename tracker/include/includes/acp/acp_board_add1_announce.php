<?php
/**
*
* @package ppkBB3cker
* @version $Id: acp_board_add1_announce.php 1.000 2021-03-29 09:24:00 PPK $
* @copyright (c) 2021 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$user->add_lang('mods/acp/ppkbb3cker_announce');

$display_vars = array(
	'title'	=> 'ACP_ANNOUNCE_SETTINGS',
	'vars'	=> array(
		'legend1'				=> 'ACP_ANNOUNCE_SETTINGS',
		'ppkbb_tctracker_disabled'		=> array('lang' => 'TRACKER_DISABLED', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
		'ppkbb_scrape_enabled'		=> array('lang' => 'SCRAPE_ENABLED', 'validate' => 'array', 'type' => 'array:5:5', 'method' => false, 'explain' => true,),
// 		'ppkbb_minscrape_interval'		=> array('lang' => 'MINSCRAPE_INTERVAL', 'validate' => 'array', 'type' => 'array:4:4', 'method' => false, 'explain' => true,),
		'ppkbb_tcannounce_interval'		=> array('lang' => 'TRACKER_ANNOUNCE_INTERVAL', 'validate' => 'array', 'type' => 'array:4:4', 'method' => false, 'explain' => true,),
		'ppkbb_tcminannounce_interval'		=> array('lang' => 'TRACKER_MINANNOUNCE_INTERVAL', 'validate' => 'array', 'type' => 'array:4:4', 'method' => false, 'explain' => true,),
		'ppkbb_tcdead_time'		=> array('lang' => 'TRACKER_DEAD_TIME', 'validate' => 'array', 'type' => 'array:20:20', 'method' => false, 'explain' => true,),
		'ppkbb_tccheck_fext'		=> array('lang' => 'TRACKER_CHECK_FEXT', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
		'ppkbb_tccheck_ban'		=> array('lang' => 'TRACKER_CHECK_BAN', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
		'ppkbb_tcrights_tcache'		=> array('lang' => 'TRACKER_RIGHTS_TCACHE', 'validate' => 'array:0', 'type' => 'array:5:5', 'method' => false, 'explain' => true,),
		'ppkbb_tcmax_seed'		=> array('lang' => 'TRACKER_MAX_SEED', 'validate' => 'int:0', 'type' => 'text:4:4', 'method' => false, 'explain' => true,),
		'ppkbb_tcmax_leech'		=> array('lang' => 'TRACKER_MAX_LEECH', 'validate' => 'int:0', 'type' => 'text:4:4', 'method' => false, 'explain' => true,),
		'ppkbb_tcmaxpeers_limit'		=> array('lang' => 'TRACKER_MAXPEERS_LIMIT', 'validate' => 'int:0', 'type' => 'text:3:3', 'method' => false, 'explain' => true,),
		'ppkbb_tcmaxpeers_rewrite'		=> array('lang' => 'TRACKER_MAXPEERS_REWRITE', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
		'ppkbb_tcmaxip_pertr'		=> array('lang' => 'TRACKER_MAXIP_PERTR', 'validate' => 'int:0', 'type' => 'text:5:5', 'method' => false, 'explain' => true,),
		'ppkbb_tcmaxip_pertorr'		=> array('lang' => 'TRACKER_MAXIP_PERTORR', 'validate' => 'int:0', 'type' => 'text:4:4', 'method' => false, 'explain' => true,),
		'ppkbb_tprivate_flag'		=> array('lang' => 'TRACKER_TPRIVATE_FLAG', 'validate' => 'array', 'type' => 'array:1:1', 'method' => false, 'explain' => true,),
		'ppkbb_tcclients_restricts'		=> array('lang' => 'TRACKER_CLIENTS_RESTRICTS', 'validate' => 'array', 'type' => 'array:3:3', 'method' => false, 'explain' => true,),
		'ppkbb_tctracker_restricts'		=> array('lang' => 'TRACKER_TORRENTS_RESTRICTS', 'validate' => 'array', 'type' => 'array:3:3', 'method' => false, 'explain' => true,),
		'ppkbb_tcignore_connectable'		=> array('lang' => 'TRACKER_IGNORE_CONNECTABLE', 'validate' => 'array', 'type' => 'array:1:1', 'method' => false, 'explain' => true,),
		'ppkbb_tcgz_rewrite'		=> array('lang' => 'TRACKER_GZREWRITE', 'validate' => 'array', 'type' => 'array:1:1', 'method' => false, 'explain' => true,),
		'ppkbb_tcignored_upload'		=> array('lang' => 'TRACKER_IGNORED_UPLOAD', 'validate' => 'array', 'type' => 'array:1:1', 'method' => false, 'explain' => true,),
		'ppkbb_tczones_enable'		=> array('lang' => 'TRACKER_ZONES_ENABLE', 'validate' => 'array', 'type' => 'array:3:3', 'method' => false, 'explain' => true,),
		'ppkbb_tciptype'	=> array('lang' => 'TRACKER_IPTYPE',	'validate' => 'array',	'type' => 'array:1:1', 'explain' => true),
		'ppkbb_tcallow_unregtorr'		=> array('lang' => 'TRACKER_ALLOW_UNREGTORR', 'validate' => 'array', 'type' => 'array:1:1', 'method' => false, 'explain' => true,),
		'ppkbb_tcenable_snatchstat'		=> array('lang' => 'TRACKER_ENABLE_SNATCHSTAT', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
		'ppkbb_tcmultiple_announce'		=> array('lang' => 'TRACKER_MULTIPLE_ANNOUNCE', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
		'ppkbb_announce_url'	=> array('lang' => 'TRACKER_ANNOUNCE_URL',	'validate' => 'string',	'type' => 'text:40:255', 'explain' => true),

	)
);
?>
