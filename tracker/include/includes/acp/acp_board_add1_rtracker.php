<?php
/**
*
* @package ppkBB3cker
* @version $Id: acp_board_add1_rtracker.php 1.000 2009-12-18 11:56:00 PPK $
* @copyright (c) 2009 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$user->add_lang('mods/acp/ppkbb3cker_rtracker');
$zones_title='ACP_TRACKER_RTRACKER';

$rtracker_id=request_var('rtrack_id', array(0=>''));

if(request_var('submit', '') && $rtracker_id)
{

	$rtracker_url=request_var('rtrack_url', array(0=>''), true);
	$rtracker_forb=request_var('rtrack_forb', array(0=>''));
	$rtracker_remote=request_var('rtrack_remote', array(0=>''));
	$forb_types=request_var('forb_type', array(0=>''));
	$rtracker_delete=request_var('rtrack_delete', array(0=>''));
	$rtracker_enabled=request_var('rtrack_enabled', array(0=>''));

	$this->u_action=append_sid("{$phpbb_admin_path}index.$phpEx", 'i=board&amp;mode=rtracker');

	$exists_tracker=array();
	$sql="SELECT rs.id, rs.rtracker_md5, rt.rtracker_id rtrack FROM ".TRACKER_RTRACKERS_TABLE." rs LEFT JOIN ".TRACKER_RTRACK_TABLE." rt ON(rs.id=rt.rtracker_id AND rt.rtracker_type='s' AND rt.user_torrent_zone='0') WHERE ".$db->sql_in_set('rs.rtracker_md5', array_map('md5', $rtracker_url));
	$result=$db->sql_query($sql);
	while($row=$db->sql_fetchrow($result))
	{
		$exists_tracker[$row['rtracker_md5']]=array('id'=>$row['id'], 'rtrack'=>$row['rtrack']);
	}
	$db->sql_freeresult($result);

	$d_rtrack=array();
	foreach($rtracker_id as $k=>$v)
	{
		$rtrack_url=isset($rtracker_url[$k]) ? $rtracker_url[$k] : '';
		$rtrack_forb=isset($rtracker_forb[$k]) ? $rtracker_forb[$k] : '';
		$rtrack_remote=isset($rtracker_remote[$k]) ? $rtracker_remote[$k] : '';
		$forb_type=isset($forb_types[$k]) ? $forb_types[$k] : '';
		$rtrack_delete=isset($rtracker_delete[$k]) ? $rtracker_delete[$k] : '';
		$rtrack_enabled=isset($rtracker_enabled[$k]) ? $rtracker_enabled[$k] : '';
		$rtrack_id=my_int_val($k);

		if($rtrack_url==='')
		{
			continue;
		}

		if($rtrack_delete)
		{
			$d_rtrack[]=$rtrack_id;
		}
		else
		{

			$rtrack_forb=in_array($rtrack_forb, array(0, 1, 2, 3)) ? $rtrack_forb : 0;

			$rtrack_url=utf8_normalize_nfc($rtrack_url);
			if(!$rtrack_forb && (!preg_match('#^(https?|udp):\/\/(\w+|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})#', $rtrack_url) || strlen($rtrack_url) > 255))
			{
				trigger_error($user->lang['INVALID_RTRACK_URL'].': '.htmlspecialchars($rtrack_url).sprintf($user->lang['RTRACK_BACK'], $this->u_action));
			}

			$rtrack_remote=$rtrack_remote==1 ? 1 : -1;
			$forb_type=in_array($forb_type, array('r', 's', 'i')) ? $forb_type : 's';

			$rtrack_md5=md5($rtrack_url);
			if(isset($exists_tracker[$rtrack_md5]))
			{
				$rtrack_id=$exists_tracker[$rtrack_md5]['id'];

				$rtrack_new=false;
			}
			else
			{
				$sql="INSERT INTO ".TRACKER_RTRACKERS_TABLE."(rtracker_url, rtracker_md5) VALUES('".$db->sql_escape($rtrack_url)."', '{$rtrack_md5}')";
				$result=$db->sql_query($sql);
				$rtrack_id=$db->sql_nextid();

				$rtrack_new=true;
			}

			if($rtrack_new || !$exists_tracker[$rtrack_md5]['rtrack'])
			{
				$sql='INSERT INTO '.TRACKER_RTRACK_TABLE."
					(rtracker_id, rtracker_remote, rtracker_enabled, rtracker_forb, forb_type, rtracker_type)
						VALUES(
						'".$rtrack_id."',
						'".$rtrack_remote."',
						'".($rtrack_enabled ? 1 : 0)."',
						'".$rtrack_forb."',
						'".$forb_type."',
						's'
				)";
				$result=$db->sql_query($sql);
			}
			else
			{
				$sql='UPDATE '.TRACKER_RTRACK_TABLE." SET
					rtracker_remote='".$rtrack_remote."',
					rtracker_enabled='".($rtrack_enabled ? 1 : 0)."',
					rtracker_forb='".$rtrack_forb."',
					forb_type='".$forb_type."'
					WHERE rtracker_id='".$rtrack_id."' AND rtracker_type='s' AND user_torrent_zone='0'";
				$result=$db->sql_query($sql);
			}
		}
	}
	if($d_rtrack)
	{
		$sql='DELETE FROM '.TRACKER_RTRACK_TABLE.' WHERE '.$db->sql_in_set('rtracker_id', $d_rtrack)." AND rtracker_type='s' AND user_torrent_zone='0'";
		$result=$db->sql_query($sql);

	}
	$cache->my_remove_cache('#forb_rtrack', 'sql');
	$cache->destroy('_ppkbb3cker_forb_rtrack');

}

$zones_title='ACP_TRACKER_RTRACKER';

if(request_var('add_new', ''))
{
	$template->assign_block_vars('rtracks', array(
		'COUNT' => 0,
		'URL'	=> '',
		'TYPE'=>'
			<select name="rtrack_remote[0]">
				<option value="-1">'.$user->lang['RTRACKER_UNITED'].'</option>
				<option value="1">'.$user->lang['RTRACKER_REMOTE_UNITED'].'</option>
			</select>
		',
		'FORB'=>'
			<select name="rtrack_forb[0]" style="max-width:200px;">
				<option value="0" selected="selected">'.$user->lang['RTRACK_FORBS'][0].'</option>
				<option value="1">'.$user->lang['RTRACK_FORBS'][1].'</option>
				<option value="2">'.$user->lang['RTRACK_FORBS'][2].'</option>
				<option value="3">'.$user->lang['RTRACK_FORBS'][3].'</option>
			</select>
			<select name="forb_type[0]" style="max-width:200px;">
				<option value="r">'.$user->lang['RTF_TYPE']['r'].'</option>
				<option value="s" selected="selected">'.$user->lang['RTF_TYPE']['s'].'</option>
				<option value="i">'.$user->lang['RTF_TYPE']['i'].'</option>
			</select>
			',
		'ENABLED' => '',
		)
	);
	$template->assign_vars(array(
		'S_NEW_RTRACKER'	=> true,
		'S_HIDDEN_FIELDS'=>'<input type="hidden" name="add_rtrack" value="1" >',
		)
	);
}
else
{
	$rtrack_stat=array();
	$sql='SELECT rt.rtracker_remote rtrack_remote, rt.rtracker_forb rtrack_forb, rt.forb_type, rt.rtracker_enabled rtrack_enabled, rs.id, rs.rtracker_url rtrack_url FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND rt.user_torrent_zone='0' AND rt.rtracker_type='s' ORDER BY rt.rtracker_enabled DESC, rt.rtracker_forb, rt.rtracker_remote DESC, rs.rtracker_url";
	$result=$db->sql_query($sql);
	while($row=$db->sql_fetchrow($result))
	{
		$rtrack_stat[$row['id']]=$row;
	}
	$db->sql_freeresult($result);

	if($rtrack_stat)
	{
		$sql='SELECT ra.tracker, SUM(ra.seeders) seeders, SUM(ra.leechers) leechers, SUM(ra.seeders)+SUM(ra.leechers) peers, SUM(ra.times_completed) completed, SUM(ra.err_count) errors, SUM(1) torrents FROM '.TRACKER_RANNOUNCES_TABLE." ra WHERE ".$db->sql_in_set('ra.tracker', array_keys($rtrack_stat)).' GROUP BY ra.tracker';
		$result=$db->sql_query($sql);
		while($row=$db->sql_fetchrow($result))
		{
			$rtrack_stat[$row['tracker']]+=$row;
		}
		$db->sql_freeresult($result);
	}

	foreach($rtrack_stat as $row)
	{
		$template->assign_block_vars('rtracks', array(
			'COUNT'	=> $row['id'],
			'URL'	=> htmlspecialchars($row['rtrack_url']),
			'TYPE'=>'
				<select name="rtrack_remote['.$row['id'].']">
					<option value="-1"'.($row['rtrack_remote']==-1 ? ' selected="selected"' : '').'>'.$user->lang['RTRACKER_UNITED'].'</option>
					<option value="1"'.($row['rtrack_remote']==1 ? ' selected="selected"' : '').'>'.$user->lang['RTRACKER_REMOTE_UNITED'].'</option>
				</select>
				',
			'FORB'=>'
				<select name="rtrack_forb['.$row['id'].']" style="max-width:200px;">
					<option value="0"'.(!$row['rtrack_forb'] ? ' selected="selected"' : '').'>'.$user->lang['RTRACK_FORBS'][0].'</option>
					<option value="1"'.($row['rtrack_forb']==1 ? ' selected="selected"' : '').'>'.$user->lang['RTRACK_FORBS'][1].'</option>
					<option value="2"'.($row['rtrack_forb']==2 ? ' selected="selected"' : '').'>'.$user->lang['RTRACK_FORBS'][2].'</option>
					<option value="3"'.($row['rtrack_forb']==3 ? ' selected="selected"' : '').'>'.$user->lang['RTRACK_FORBS'][3].'</option>
				</select>
				<select name="forb_type['.$row['id'].']" style="max-width:200px;">
					<option value="r"'.($row['forb_type']=='r' ? ' selected="selected"' : '').'>'.$user->lang['RTF_TYPE']['r'].'</option>
					<option value="s"'.($row['forb_type']=='s' ? ' selected="selected"' : '').'>'.$user->lang['RTF_TYPE']['s'].'</option>
					<option value="i"'.($row['forb_type']=='i' ? ' selected="selected"' : '').'>'.$user->lang['RTF_TYPE']['i'].'</option>
				</select>
				',
			'STAT' => $row['rtrack_enabled'] && $row['rtrack_remote']==1 && !$row['rtrack_forb'] && isset($row['errors']) ? sprintf($user->lang['RTRACKER_STAT'], $row['seeders'], $row['leechers'], $row['peers'], $row['completed'], $row['torrents'], $row['errors'], ($row['torrents'] ? $row['errors']/$row['torrents'] : 0)) : false,
			'ENABLED' => $row['rtrack_enabled'] ? ' checked="checked"' : '',
			'S_FORB' => $row['rtrack_forb'] ? true : false,
			)
		);
	}
	$template->assign_vars(array(
		'S_VIEW_RTRACKER'	=> true,
		'S_HIDDEN_FIELDS'=>'<input type="hidden" name="add_rtrack" value="1" >',
		)
	);
}

$template->assign_vars(array(
	'S_RTRACKER_INC'	=> true,
	)
);

$display_vars = array(
	'title'	=> $zones_title,
	'vars'	=> array(
		'legend1'				=> 'ACP_TRACKER_RTRACKER_SETTINGS',
	)
);

?>
