<?php
/**
*
* @package ppkBB3cker
* @version $Id: acp_board_add1_tracker.php 1.000 2009-08-13 11:45:00 PPK $
* @copyright (c) 2009 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$user->add_lang('mods/acp/ppkbb3cker_tracker');

$display_vars = array(
	'title'	=> 'ACP_TRACKER_SETTINGS',
	'vars'	=> array(
		'legend1'				=> 'ACP_TRACKER_SETTINGS',
		'ppkbb_tcratio_start'		=> array('lang' => 'TRACKER_RATIO_START', 'validate' => 'array', 'type' => 'array:1:1', 'method' => false, 'explain' => true,),
		'ppkbb_append_tfile'		=> array('lang' => 'TRACKER_APPEND_TFILE', 'validate' => 'string', 'type' => 'text:32:64', 'method' => false, 'explain' => true,),
		'ppkbb_poll_options'		=> array('lang' => 'TRACKER_POLL_OPTIONS', 'validate' => 'array', 'type' => 'array:3:3', 'method' => false, 'explain' => true,),
		'ppkbb_ipreg_countrestrict'	=> array('lang' => 'TRIPREG_COUNTRESTRICT', 'validate' => 'array', 'type' => 'array:5:5', 'method' => false, 'explain' => true,),
		'ppkbb_tstatus_notify'		=> array('lang' => 'TRACKER_TSTATUS_NOTIFY', 'validate' => 'array', 'type' => 'array:3:3', 'method' => false, 'explain' => true,),
		'ppkbb_tracker_top'		=> array('lang' => 'TRACKER_TOP', 'validate' => 'array', 'type' => 'array:3:3', 'method' => false, 'explain' => true,),
		'ppkbb_tracker_top_exclude'		=> array('lang' => 'TRACKER_TOP_EXCLUDE', 'validate' => 'array', 'type' => 'custom', 'method' => 'select_tracker_forums', 'explain' => true,),
		'ppkbb_tracker_top_trueexclude'		=> array('lang' => 'TRACKER_TOP_TRUEEXCLUDE', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
		'ppkbb_torrent_magnetlink'		=> array('lang' => 'TRACKER_MAGNET_LINK', 'validate' => 'array', 'type' => 'array:3:3', 'method' => false, 'explain' => true,),
		'ppkbb_tctrestricts_options'		=> array('lang' => 'PPKBB_TCTRESTRICTS_OPTIONS', 'validate' => 'array', 'type' => 'array:2:2', 'method' => false, 'explain' => true,),
		'ppkbb_search_astracker'		=> array('lang' => 'PPKBB_SEARCH_ASTRACKER', 'validate' => 'array', 'type' => 'array:3:3', 'method' => false, 'explain' => true,),
		'ppkbb_addit_options'		=> array('lang' => 'PPKBB_ADDIT_OPTIONS', 'validate' => 'array', 'type' => 'array:3:3', 'method' => false, 'explain' => true,),

		'legend2'				=> 'ACP_PPKBB_BONUSES',
		'ppkbb_tcbonus_value'		=> array('lang' => 'TRACKER_BONUS_VALUE', 'validate' => 'array', 'type' => 'array:5:5', 'method' => false, 'explain' => true,),
		'ppkbb_tcbonus_fsize'		=> array('lang' => 'TRACKER_BONUS_FSIZE', 'validate' => 'array', 'type' => 'array:6:6', 'method' => false, 'explain' => true,),

		'legend3'				=> 'ACP_PPKBB_DISPLAY',
		'ppkbb_torrblock_width'		=> array('lang' => 'TRACKER_TORRBLOCK_WIDTH', 'validate' => 'array', 'type' => 'array:5:5', 'method' => false, 'explain' => true,),
		'ppkbb_torrent_statvt'		=> array('lang' => 'TRACKER_TORRENT_STATVT', 'validate' => 'array', 'type' => 'array:3:3', 'method' => false, 'explain' => true,),
		'ppkbb_torrent_statml'		=> array('lang' => 'TRACKER_TORRENT_STATML', 'validate' => 'array', 'type' => 'array:3:3', 'method' => false, 'explain' => true,),
		'ppkbb_torr_blocks'		=> array('lang' => 'PPKBB_TORR_BLOCKS', 'validate' => 'array', 'type' => 'array:3:3', 'method' => false, 'explain' => true,),
		'ppkbb_noticedisclaimer_blocks'		=> array('lang' => 'PPKBB_NOTICEDISCLAIMER_BLOCKS', 'validate' => 'array', 'type' => 'array:3:3', 'method' => false, 'explain' => true,),
		'ppkbb_mua_countlist'		=> array('lang' => 'PPKBB_MUA_COUNTLIST', 'validate' => 'string', 'type' => 'text:32:64', 'method' => false, 'explain' => true,),
		'ppkbb_smartmenus'		=> array('lang' => 'PPKBB_SMARTMENUS', 'validate' => 'array', 'type' => 'array:6:6', 'method' => false, 'explain' => true,),

		'legend4'				=> 'PPKBB_TOPDOWN_TORRENTS',
		'ppkbb_topdown_torrents'		=> array('lang' => 'PPKBB_TOPDOWN_TORRENTS', 'validate' => 'array', 'type' => 'array:5:5', 'method' => false, 'explain' => true,),
		'ppkbb_topdown_torrents_options'		=> array('lang' => 'PPKBB_TOPDOWN_TORRENTS_OPTIONS', 'validate' => 'array', 'type' => 'array:9:9', 'method' => false, 'explain' => true,),
		'ppkbb_topdown_torrents_exclude'		=> array('lang' => 'PPKBB_TOPDOWN_TORRENTS_EXCLUDE', 'validate' => 'array', 'type' => 'custom', 'method' => 'select_tracker_forums', 'explain' => true,),
		'ppkbb_topdown_torrents_trueexclude'		=> array('lang' => 'PPKBB_TOPDOWN_TORRENTS_TRUEEXCLUDE', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
	)
);
?>
