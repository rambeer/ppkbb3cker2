<?php
/**
*
* @package ppkBB3cker
* @version $Id: acp_board_add1_cronjobs.php 1.000 2019-11-20 10:13:00 PPK $
* @copyright (c) 2019 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$user->add_lang('mods/acp/ppkbb3cker_cronjobs');
$cronjobs_title='ACP_TRACKER_CRONJOBS';

$valid_cj_pages=array(
	'index'=>true,
	'portal'=>true,
	'viewforum'=>true,
	'viewtopic'=>true,
	'memberlist'=>true,
	'posting'=>true,
	'search'=>true,
// 	''=>false,
);
$cj_options=array(
	'tracker_delete_dead_peers'=>array('from_guest'=>true, 'pages'=>array('index', 'portal', 'viewforum', 'viewtopic', 'memberlist', 'posting', 'search')),
	'torrent_recount_seed_leech'=>array('from_guest'=>true, 'pages'=>array('viewforum', 'viewtopic', 'search')),
	'torrent_recount_seed_leech_speed'=>array('from_guest'=>true, 'pages'=>array('viewforum', 'viewtopic', 'search')),
	'torrent_announce_remote_trackers'=>array('from_guest'=>true, 'pages'=>array('viewforum', 'viewtopic', 'search', 'posting')),
	'torrent_fix_torrents_statuses'=>array('from_guest'=>true, 'pages'=>array('index', 'portal', 'viewforum', 'viewtopic', 'memberlist', 'posting', 'search')),
	'torrent_delete_dead_torrents'=>array('from_guest'=>true, 'pages'=>array('viewforum')),
	'stat_count_seed_leech'=>array('from_guest'=>true, 'pages'=>array('index', 'portal', 'viewforum', 'viewtopic', 'memberlist', 'posting', 'search')),
	'stat_count_upload_download'=>array('from_guest'=>true, 'pages'=>array('index', 'portal', 'viewforum', 'viewtopic', 'memberlist', 'posting', 'search')),
	'stat_count_session_upload_download'=>array('from_guest'=>true, 'pages'=>array('index', 'portal', 'viewforum', 'viewtopic', 'memberlist', 'posting', 'search')),
	'stat_count_torrents_size'=>array('from_guest'=>true, 'pages'=>array('index', 'portal', 'viewforum', 'viewtopic', 'memberlist', 'posting', 'search')),
	'stat_count_upload_download_speed'=>array('from_guest'=>true, 'pages'=>array('index', 'portal', 'viewforum', 'viewtopic', 'memberlist', 'posting', 'search')),
	'stat_count_torrents'=>array('from_guest'=>true, 'pages'=>array('index', 'portal', 'viewforum', 'viewtopic', 'memberlist', 'posting', 'search')),
	'stat_count_comments'=>array('from_guest'=>true, 'pages'=>array('index', 'portal', 'viewforum', 'viewtopic', 'memberlist', 'posting', 'search')),
	'stat_count_thanks'=>array('from_guest'=>true, 'pages'=>array('index', 'portal', 'viewforum', 'viewtopic', 'memberlist', 'posting', 'search')),
	'stat_count_torrentsseed_usersseed'=>array('from_guest'=>true, 'pages'=>array('index', 'portal', 'viewforum', 'viewtopic', 'memberlist', 'posting', 'search')),
	'stat_count_torrentsleech_usersleech'=>array('from_guest'=>true, 'pages'=>array('index', 'portal', 'viewforum', 'viewtopic', 'memberlist', 'posting', 'search')),
	'user_count_torrents'=>array('from_guest'=>false, 'pages'=>array('index', 'portal', 'viewforum', 'viewtopic', 'memberlist', 'posting', 'search')),
	'user_count_comments'=>array('from_guest'=>false, 'pages'=>array('index', 'portal', 'viewforum', 'viewtopic', 'memberlist', 'posting', 'search')),
);

if(request_var('submit', ''))
{
	$run_period=request_var('run_period', array(''=>0));
	$run_periodv=request_var('run_periodv', array(''=>''));
	$start_run=request_var('start_run', array(''=>''));
	$end_run=request_var('end_run', array(''=>''));
	$last_run_reset=request_var('lr_reset', array(''=>0));
	$from_guest=request_var('from_guest', array(''=>0));
	$enabled=request_var('cj_enable', array(''=>0));
	$pages=request_var('pages', array(''=>array(''=>0)));

	$update_tracker_config=array('torrent_recount_seed_leech_speed');
	foreach($cj_options as $option=>$data)
	{
		preg_match('/^\d{2}$/', $start_run[$option]) ? '' : $start_run[$option]='';
		preg_match('/^\d{2}$/', $end_run[$option]) ? '' : $end_run[$option]='';
		$from_guest[$option]=isset($from_guest[$option]) ? 1 : 0;
		$enabled[$option]=isset($enabled[$option]) ? 1 : 0;
		$cj_pages='';
		if(isset($pages[$option]))
		{
			foreach($pages[$option] as $page=>$value)
			{
				if(!isset($valid_cj_pages[$page]) || !$valid_cj_pages[$page])
				{
					unset($pages[$option][$page]);
				}
			}
			$cj_pages=implode(',', array_keys($pages[$option]));
		}
		$run_period_value=get_time_value($run_periodv[$option], $run_period[$option]);
		$sql='UPDATE '.TRACKER_CRONJOBS_TABLE." SET run_period='".$run_period_value."', start_run='{$start_run[$option]}', end_run='{$end_run[$option]}', from_guest='{$from_guest[$option]}', enabled='{$enabled[$option]}', pages='{$cj_pages}'" . (isset($last_run_reset[$option]) ? ', runs_count=0, finish_count=0' : '') . " WHERE cron_name='".$option."'";
		$result=$db->sql_query($sql);

		if(isset($last_run_reset[$option]))
		{
			set_config("last_$option", 0, true);
			if($option=='torrent_delete_dead_torrents')
			{
				$db->sql_query('UPDATE '.FORUMS_TABLE.' SET last_dtad=0');
			}
			else if($option=='torrent_announce_remote_trackers')
			{
				$db->sql_query('UPDATE '.TRACKER_TORRENTS_TABLE.' SET lastremote=0');
			}
			else if($option=='torrent_recount_seed_leech')
			{
				$db->sql_query('UPDATE '.TRACKER_TORRENTS_TABLE.' SET lastcleanup=0');
			}
			else if($option=='torrent_recount_seed_leech_speed')
			{
				$db->sql_query('UPDATE '.TRACKER_TORRENTS_TABLE.' SET lastslspeed=0');
			}
			else if(in_array($option, array('user_count_torrents', 'user_count_comments')))
			{
				$db->sql_query('UPDATE '.USERS_TABLE." SET user_tracker_data='0 0 0 0'");
			}
		}

		$insert_value=0;
		if($enabled[$option])
		{
			if($run_period_value && $start_run[$option]!=='' && $end_run[$option]!=='')
			{
				$insert_value=$run_period_value >= 86400 ? 86400 : $run_period_value;
			}
			else if($run_period_value)
			{
				$insert_value=$run_period_value;
			}
			else if($start_run[$option]!=='' && $end_run[$option]!=='')
			{
				$insert_value=86400;
			}
		}
		if(in_array($option, $update_tracker_config))
		{
			set_tracker_config('ppkbb_tc'.$option, $insert_value);
			$option=='torrent_recount_seed_leech_speed' ? set_tracker_config('ppkbb_gtc'.$option, ($from_guest[$option] ? $insert_value : 0)) : '';
		}
		else
		{
			set_tracker_config('ppkbb_'.$option, $insert_value);
		}
	}

	$cache->destroy('_ppkbb3cker_cron_data');
	$cache->destroy('_ppkbb3cker_cache');

	purge_tracker_config('tracker');
}

if(!class_exists('timedelta'))
{
	$user->add_lang('mods/posts_merging');
	require($phpbb_root_path . 'includes/time_delta.'.$phpEx);
}
$td = new timedelta();

$current_cat='';
$sql='SELECT * FROM '.TRACKER_CRONJOBS_TABLE.' ORDER BY cron_name, enabled DESC';
$result=$db->sql_query($sql);
while($row=$db->sql_fetchrow($result))
{
	$cron_options=explode(',', $row['cron_options']);
	$cat_name=explode('_', $row['cron_name']);
	$template->assign_block_vars('cronjobs', array(
		'IS_CAT'	=> $cat_name[0]!=$current_cat ? $user->lang['CJ_CATS'][$cat_name[0]] : false,
		'ID'	=> $row['cron_name'],
		'PAGES'	=> cronjob_pages($row['pages'], $valid_cj_pages, $row['cron_name'], $cj_options[$row['cron_name']]),
		'CRON_NAME'	=> $user->lang[strtoupper($row['cron_name'])],
		'CRON_JOB_EXPLAIN'	=> $user->lang[strtoupper($row['cron_name']).'_EXPLAIN'],
		'START_RUN'	=> $row['start_run'],
		'END_RUN'	=> $row['end_run'],
		'RUN_PERIOD'	=> $row['run_period'],
		'RUN_PERIOD_VALUE'	=> $td->spelldelta(0, $row['run_period']),
		'RUN_PERIOD_V'	=> '<select name="run_periodv['.$row['cron_name'].']">'.select_time_value().'</select>',
		'RUNS_COUNT' => $row['runs_count'],
		'FINISH_COUNT' => $row['finish_count'],
		'PERCENT_COUNT' => $row['runs_count'] ? number_format(100 * $row['finish_count'] / $row['runs_count'], 2, '.', '') : '',
		'ENABLED'	=> $row['enabled'] ? ' checked="checked"' : '',
		'FROM_GUEST'	=> $row['from_guest'] ? ' checked="checked"' : '',
		'FROM_GUEST_DISABLED'	=> !$cj_options[$row['cron_name']]['from_guest'] ? ' disabled="disabled"' : '',
		'FORUM_CRON'	=> in_array('forum_cron', $cron_options) ? true : false,
		'IN_ANNOUNCE'	=> in_array('in_announce', $cron_options) ? true : false,
		'LAST_RUN' => $config['last_'.$row['cron_name']] ? date('Y-m-d H:i:s', $config['last_'.$row['cron_name']]) : '',
		'A_VIEW_JOB'	=> append_sid("{$phpbb_admin_path}index.$phpEx", 'i=board&amp;mode=cronjobs&amp;view_job=1&amp;jobid='.$row['id']),
		)
	);
	$current_cat=$cat_name[0];
}
$db->sql_freeresult($result);

$template->assign_vars(array(
	'S_VIEW_CRONJOBS'	=> true,
	'S_VIEW_CRONJOBS_PERCENT'	=> $config['ppkbb_cron_options'][5] ? true : false,
	)
);

$display_vars = array(
	'title'	=> $cronjobs_title,
	'vars'	=> array(
		'legend1'				=> 'ACP_TRACKER_CRONJOBS',
	)
);

$template->assign_vars(array(
	'S_CRONJOBS_INC'	=> true,
	)
);

function cronjob_pages($current, $valid_cj_pages, $id, $cj_options)
{
	global $user;

	$form_pages='';
	$current=explode(',', $current);

	foreach($valid_cj_pages as $page => $enabled)
	{
		if($enabled)
		{
			$checked=in_array($page, $current) ? ' checked="checked"' : '';
			$disabled=!in_array($page, $cj_options['pages']) ? ' disabled="disabled"' : '';
			$form_pages.='<span'.($disabled ? ' class="cj_disabled"' : '').'><input class="radio" type="checkbox" name="pages['.$id.']['.$page.']" value="1"'.$checked.$disabled.'> - '.$user->lang['CJ_PAGES'][$page].($checked ? " (<strong>$page</strong>)" : " ($page)").'</span><br />';
		}
	}

	return $form_pages;
}
?>
