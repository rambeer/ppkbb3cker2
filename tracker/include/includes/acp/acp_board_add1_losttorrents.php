<?php
/**
*
* @package ppkBB3cker
* @version $Id: acp_board_add1_losttorrents.php 1.000 2015-10-01 10:36:00 PPK $
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$trstat_title='ACP_TRACKER_LOSTTORRENTS';

$user->add_lang('mods/acp/ppkbb3cker_losttorrents');

$user->add_lang('mcp');

$submit = (isset($_POST['submit'])) ? true : false;
$form_key = 'acp_losttorrents';
add_form_key($form_key);

$error = array();

$user->add_lang('posting');

$action		= request_var('action', '');
$forum_id	= request_var('f', 0);
$topic_id	= request_var('t', 0);
$start		= request_var('start', 0);

$deletemarked = request_var('deletemarked', '');
$deleteall	= request_var('deleteall', '');
$marked		= request_var('mark', array(0));
$last_id=request_var('last_id', 0);

$sort_days	= request_var('st', 0);
$sort_key	= request_var('sk', 't');
$sort_dir	= request_var('sd', 'd');

$limit_days = array(
	0 => $user->lang['ALL_ENTRIES'],
	1 => $user->lang['1_DAY'],
	7 => $user->lang['7_DAYS'],
	14 => $user->lang['2_WEEKS'],
	30 => $user->lang['1_MONTH'],
	90 => $user->lang['3_MONTHS'],
	180 => $user->lang['6_MONTHS'],
	365 => $user->lang['1_YEAR'],
);
$sort_by_text = array(
// 	'u' => $user->lang['SORT_USERNAME'],
	'uid' => $user->lang['SORT_USER_ID'],
	't' => $user->lang['SORT_DATE_UPLOAD'],
	'f' => $user->lang['SORT_FILENAME'],
// 	'p' => $user->lang['SORT_TITLE'],
);
$sort_by_sql = array(
// 	'u' => 'u.username_clean',
	'uid' => 'a.poster_id',
	't' => 'a.filetime',
	'f' => 'a.real_filename',
// 	'p'=>'p.post_subject',
);

$s_limit_days = $s_sort_key = $s_sort_dir = $u_sort_param = '';
gen_sort_selects($limit_days, $sort_by_text, $sort_days, $sort_key, $sort_dir, $s_limit_days, $s_sort_key, $s_sort_dir, $u_sort_param);

$sql_where = ($sort_days) ? (time() - ($sort_days * 86400)) : 0;
$sql_sort = $sort_by_sql[$sort_key] . ' ' . (($sort_dir == 'd') ? 'DESC' : 'ASC');
// 	$sql='SELECT COUNT(*) log_count FROM '.POSTS_TABLE.' p, '.TOPICS_TABLE .' t, '.USERS_TABLE.' u, '.ATTACHMENTS_TABLE.' a LEFT JOIN '.TRACKER_TORRENTS_TABLE.' tr ON(a.attach_id=tr.id) WHERE '.$db->sql_in_set('p.forum_id', $forum_trackers)." AND p.post_id=t.topic_first_post_id AND p.post_id=a.post_msg_id AND a.extension='torrent' AND a.in_message='0' AND p.poster_id=u.user_id AND tr.id IS NULL".($sql_where ? ' AND a.filetime > '.$sql_where : '')."";
$log_count = 0;
$sql="SELECT COUNT(*) log_count FROM ".ATTACHMENTS_TABLE." a LEFT JOIN ".TRACKER_TORRENTS_TABLE." tr ON(a.attach_id=tr.id) WHERE a.extension='torrent' AND a.in_message='0' AND tr.id IS NULL".($sql_where ? ' AND a.filetime > '.$sql_where : '');//echo $sql;
$result=$db->sql_query($sql);
$log_count=(int) $db->sql_fetchfield('log_count');
$db->sql_freeresult($result);

$forum_trackers=array();
$sql='SELECT forum_id FROM '.FORUMS_TABLE." WHERE forumas='1'";
$result=$db->sql_query($sql);
while($row=$db->sql_fetchrow($result))
{
	$forum_trackers[]=$row['forum_id'];
}
$db->sql_freeresult($result);

if (($deletemarked || $deleteall) && $forum_trackers && $log_count)
{
	if (confirm_box(true) || ($deleteall && $last_id))
	{
		$torrents_id = array();
		if (($deletemarked) && sizeof($marked))
		{

			foreach ($marked as $mark)
			{
				$torrents_id[] = (int) $mark;
			}
		}

		if($deleteall)
		{
// 			$sql='SELECT a.attach_id FROM '.POSTS_TABLE.' p, '.TOPICS_TABLE .' t, '.USERS_TABLE.' u, '.ATTACHMENTS_TABLE.' a LEFT JOIN '.TRACKER_TORRENTS_TABLE.' tr ON(a.attach_id=tr.id) WHERE '.$db->sql_in_set('p.forum_id', $forum_trackers)." AND p.post_id=t.topic_first_post_id AND p.post_id=a.post_msg_id AND a.extension='torrent' AND a.in_message='0' AND p.poster_id=u.user_id AND tr.id IS NULL AND a.attach_id > $last_id ORDER BY a.attach_id LIMIT $start, {$config['topics_per_page']}";

			$sql="SELECT a.attach_id FROM ".ATTACHMENTS_TABLE." a LEFT JOIN ".TRACKER_TORRENTS_TABLE." tr ON(a.attach_id=tr.id) WHERE a.extension='torrent' AND a.in_message='0' AND tr.id IS NULL AND a.attach_id > $last_id".($sql_where ? ' AND a.filetime > '.$sql_where : '')." ORDER BY a.attach_id LIMIT $start, {$config['topics_per_page']}";//echo $sql;
			$result=$db->sql_query($sql);
			while($row=$db->sql_fetchrow($result))
			{
				$torrents_id[]=$row['attach_id'];
			}
			$db->sql_freeresult($result);
		}

		if($deletemarked || $deleteall)
		{
			if (!function_exists('delete_attachments'))
			{
				include_once($phpbb_root_path . 'includes/functions_admin.' . $phpEx);
			}

			if ($torrents_id || $deleteall)
			{
				if(!$torrents_id)
				{
					trigger_error(sprintf($user->lang['FIX_LOSTTORRENTS_FINISH'], $this->u_action));
				}

				$num_deleted=delete_attachments('attach', $torrents_id);

				add_log('admin', 'LOG_DELETE_LOSTTORRENTS');

				if($deletemarked)
				{
					trigger_error(sprintf($user->lang['FIX_LOSTTORRENTS_FINISH'], $this->u_action));
				}

				if($deleteall && $torrents_id)
				{
					$redirect_url=$this->u_action."&amp;deleteall=1&amp;last_id=1&amp;st=$sort_days&amp;sk=$sort_key&amp;sd=$sort_dir";

					meta_refresh(3, $redirect_url);
				}

				$message=sprintf($user->lang['DELETED_TORRENTS'], (int) $num_deleted);


				trigger_error(($deleteall ? $user->lang['FIX_LOSTTORRENTS_WAIT'] : '').sprintf($user->lang['FIX_LOSTTORRENTS_RESULT'], $message, $this->u_action));
			}
		}
	}
	else
	{
		confirm_box(false, $user->lang['CONFIRM_OPERATION'], build_hidden_fields(array(
			'f'			=> $forum_id,
			'start'		=> $start,

			'deletemarked'	=> $deletemarked,
			'deleteall'	=> $deleteall,
			'mark'		=> $marked,
			'st'		=> $sort_days,
			'sk'		=> $sort_key,
			'sd'		=> $sort_dir,
			'i'			=> $id,
			'mode'		=> $mode,
			'action'	=> $action))
		);
	}
}

$log_data = array();
if($forum_trackers && $log_count)
{
// 	$sql='SELECT u.username, u.user_colour, a.real_filename, a.filetime, a.post_msg_id, a.attach_id, p.post_subject, p.topic_id, p.forum_id, p.poster_id FROM '.POSTS_TABLE.' p, '.TOPICS_TABLE .' t, '.USERS_TABLE.' u, '.ATTACHMENTS_TABLE.' a LEFT JOIN '.TRACKER_TORRENTS_TABLE.' tr ON(a.attach_id=tr.id) WHERE '.$db->sql_in_set('p.forum_id', $forum_trackers)." AND p.post_id=t.topic_first_post_id AND p.post_id=a.post_msg_id AND a.extension='torrent' AND a.in_message='0' AND p.poster_id=u.user_id AND tr.id IS NULL".($sql_where ? ' AND a.filetime > '.$sql_where : '')." ORDER BY $sql_sort LIMIT $start, {$config['topics_per_page']}";

	$attach_ids=array();
	$sql="SELECT a.attach_id FROM ".ATTACHMENTS_TABLE." a LEFT JOIN ".TRACKER_TORRENTS_TABLE." tr ON(a.attach_id=tr.id) WHERE a.extension='torrent' AND a.in_message='0' AND tr.id IS NULL".($sql_where ? ' AND a.filetime > '.$sql_where : '')." ORDER BY $sql_sort LIMIT $start, {$config['topics_per_page']}";
	$result=$db->sql_query($sql);//echo $sql;
	while($row=$db->sql_fetchrow($result))
	{
		$attach_ids[]=$row['attach_id'];
	}
	$db->sql_freeresult($result);

	if($attach_ids)
	{
		$sql='SELECT a.real_filename, a.filetime, a.post_msg_id a_post_id, a.topic_id a_topic_id, a.poster_id a_poster_id, a.attach_id, a.is_orphan, u.user_id, u.username, u.user_colour, p.post_subject, p.topic_id, p.forum_id, p.poster_id, p.post_id, t.topic_first_post_id, t.topic_id, t.topic_title FROM '.ATTACHMENTS_TABLE.' a LEFT JOIN '.POSTS_TABLE.' p ON(a.post_msg_id=p.post_id) LEFT JOIN '.TOPICS_TABLE .' t ON(a.topic_id=t.topic_id) LEFT JOIN '.USERS_TABLE." u ON(a.poster_id=u.user_id) WHERE ".$db->sql_in_set('a.attach_id', $attach_ids)." ORDER BY $sql_sort";//echo $sql;
		$result=$db->sql_query($sql);
		while($row=$db->sql_fetchrow($result))
		{
			$log_data[]=$row;
		}
		$db->sql_freeresult($result);
	}
}

if($log_data)
{
	foreach ($log_data as $row)
	{
		$topic_url=array();
		$row['forum_id'] ? $topic_url[]='f='.$row['forum_id'] : '';
		$row['topic_id'] ? $topic_url[]='t='.$row['topic_id'] : '';
		$row['post_id'] ? $topic_url[]='p='.$row['post_id'] : '';

		$template->assign_block_vars('lost_torrents', array(
			'USERNAME'			=> $row['poster_id']==1 ? $user->lang['TRACKER_ANONYMOUS'] : (empty($row['username']) ? '&nbsp;' : get_username_string('full', $row['user_id'], $row['username'], $row['user_colour'], $row['username'])),

			'TORRENT_DATE'				=> $user->format_date($row['filetime'], 'Y-m-d H:i:s'),
			'TORRENT_URL'			=> !empty($row['real_filename']) ? append_sid("{$phpbb_root_path}download/file.$phpEx", "id={$row['attach_id']}") : '',

			'POST_TITLE'			=> !empty($row['post_subject']) ? $row['post_subject'] : false,
			'TOPIC_TITLE'			=> !empty($row['topic_title']) ? $row['topic_title'] : false,
			'TOPIC_URL'			=> $topic_url ? append_sid("{$phpbb_root_path}viewtopic.$phpEx", implode('&amp;', $topic_url)) : false,

			'ATTACH_ID'				=> $row['attach_id'],
			'POST_ID'				=> $row['a_post_id'],
			'TOPIC_ID'				=> $row['a_topic_id'],
			'FORUM_ID'				=> $row['forum_id'],
			'POSTER_ID'				=> $row['a_poster_id'],

			'FILENAME'			=> urldecode($row['real_filename']),

			'IS_TRACKER' => in_array($row['forum_id'], $forum_trackers) ? true : false,
			'IS_FIRST_POST' => $row['post_id']==$row['topic_first_post_id'] ? true : false,
			'IS_ORPHAN' => $row['is_orphan'] ? true : false,
			'IS_NULL' => true,

			'USER_EXISTS' => $row['user_id'] ? true : false,
			'POST_EXISTS' => $row['post_id'] ? true : false,
			'TOPIC_EXISTS' => $row['topic_id'] ? true : false,
			)
		);
	}
}

$template->assign_vars(array(
	'S_ON_PAGE'		=> on_page($log_count, $config['topics_per_page'], $start),
	'PAGINATION'	=> generate_pagination($this->u_action . "&amp;$u_sort_param", $log_count, $config['topics_per_page'], $start, true),

	'TOTAL_LOGS'	=> $log_count ? sprintf($user->lang['TOTAL_LOGS'], $log_count) : false,

	'S_LIMIT_DAYS'	=> $s_limit_days,
	'S_SORT_KEY'	=> $s_sort_key,
	'S_SORT_DIR'	=> $s_sort_dir,

	'S_LOSTTORRENTS_INC'	=> true,
	'S_TRACKER_NOBUTT' => true,

	'L_TITLE'			=> $user->lang['ACP_TRACKER_LOSTTORRENTS'],
	'L_EXPLAIN'	=> $user->lang['ACP_TRACKER_LOSTTORRENTS_EXPLAIN'],

	'S_ERROR'			=> (sizeof($error)) ? true : false,
	'ERROR_MSG'			=> implode('<br />', $error),

	'U_ACTION'       => $this->u_action,
	)
);


$display_vars = array(
	'title'	=> $trstat_title,
	'vars'	=> array(
		'legend1'				=> 'ACP_TRACKER_LOSTTORRENTS_SETTINGS',
	)
);


?>
