<?php
/**
*
* @package ppkBB3cker
* @version $Id: acp_users_add_rtracker.php 1.000 2010-08-04 14:12:00 PPK $
* @copyright (c) 2010 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if ($submit)
{
	$error=array();
	if (check_form_key($form_name))
	{
		$rtracker_id=request_var('rtrack_id', array(0=>''));

		if(request_var('add_rtrack', 0) && $rtracker_id)
		{
			$forb_rtracks=get_forb_rtrack();

			$rtracker_url=request_var('rtrack_url', array(0=>''), true);
			$rtracker_delete=request_var('rtrack_delete', array(0=>''));
			$rtracker_enabled=request_var('rtrack_enabled', array(0=>''));

			$exists_tracker=array();
			$sql="SELECT rs.id, rs.rtracker_md5, rt.rtracker_id rtrack FROM ".TRACKER_RTRACKERS_TABLE." rs LEFT JOIN ".TRACKER_RTRACK_TABLE." rt ON(rs.id=rt.rtracker_id AND rt.rtracker_type='u' AND rt.user_torrent_zone='{$user_id}') WHERE ".$db->sql_in_set('rs.rtracker_md5', array_map('md5', $rtracker_url));
			$result=$db->sql_query($sql);
			while($row=$db->sql_fetchrow($result))
			{
				$exists_tracker[$row['rtracker_md5']]=array('id'=>$row['id'], 'rtrack'=>$row['rtrack']);
			}
			$db->sql_freeresult($result);

			$user_trackers=0;
			if(sizeof($rtracker_id)==1 && isset($rtracker_id[0]) && !$rtracker_id[0])
			{
				$sql="SELECT COUNT(*) user_trackers FROM ".TRACKER_RTRACK_TABLE." WHERE rtracker_type='u' and user_torrent_zone='{$user_id}'";
				$result=$db->sql_query($sql);
				$user_trackers=(int) $db->sql_fetchfield('user_trackers');
				$db->sql_freeresult($result);
			}

			$rtrack_count=1;
			$d_rtrack=array();
			foreach($rtracker_id as $k=>$v)
			{
				$rtrack_url=isset($rtracker_url[$k]) ? $rtracker_url[$k] : '';
				$rtrack_delete=isset($rtracker_delete[$k]) ? $rtracker_delete[$k] : '';
				$rtrack_enabled=isset($rtracker_enabled[$k]) ? $rtracker_enabled[$k] : '';
				$rtrack_id=my_int_val($k);

				if($rtrack_url==='')
				{
					continue;
				}

				if($rtrack_delete)
				{
					$d_rtrack[]=$rtrack_id;

					$rtrack_count-=1;
				}
				else
				{
					$rtrack_url=utf8_normalize_nfc($rtrack_url);

					if((!preg_match('#^(https?|udp):\/\/(\w+|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})#', $rtrack_url) || strlen($rtrack_url) > 255))
					{
						$message=$user->lang['INVALID_RTRACK_URL'].': '.htmlspecialchars($rtrack_url);
						trigger_error($message . adm_back_link($this->u_action . '&amp;u=' . $user_id.'&amp;add_new=1'));
					}

					$rtrack_forb=0;
					if(sizeof($forb_rtracks))
					{
						foreach($forb_rtracks as $f)
						{
							if($f['forb_type']=='s' && strstr($rtrack_url, $f['rtrack_url']))
							{
								$rtrack_forb=1;
							}
							else if($f['forb_type']=='i' && stristr($rtrack_url, $f['rtrack_url']))
							{
								$rtrack_forb=1;
							}
							else if($f['forb_type']=='r' && preg_match("{$f['rtrack_url']}", $rtrack_url))
							{
								$rtrack_forb=1;
							}
						}
					}
					if($rtrack_forb)
					{
						$error=$user->lang['FORB_RTRACK_URL'].': '.htmlspecialchars($rtrack_url);
						trigger_error($error . adm_back_link($this->u_action . '&amp;u=' . $user_id));
					}
					if($rtrack_count > $config['ppkbb_rtrack_enable'][1] || $user_trackers+$rtrack_count > $config['ppkbb_rtrack_enable'][1])
					{
						$error=sprintf($user->lang['MAXUSERS_ANNOUNCES_LIMIT'], $config['ppkbb_rtrack_enable'][1]);
						trigger_error($error . adm_back_link($this->u_action . '&amp;u=' . $user_id));
					}

					$rtrack_md5=md5($rtrack_url);
					if(isset($exists_tracker[$rtrack_md5]))
					{
						$rtrack_id=$exists_tracker[$rtrack_md5]['id'];

						$rtrack_new=false;
					}
					else
					{
						$sql="INSERT INTO ".TRACKER_RTRACKERS_TABLE."(rtracker_url, rtracker_md5) VALUES('".$db->sql_escape($rtrack_url)."', '{$rtrack_md5}')";
						$result=$db->sql_query($sql);
						$rtrack_id=$db->sql_nextid();

						$rtrack_new=true;
					}

					if($rtrack_new || !$exists_tracker[$rtrack_md5]['rtrack'])
					{
						$sql='INSERT INTO '.TRACKER_RTRACK_TABLE."
							(rtracker_id, rtracker_enabled, rtracker_type, user_torrent_zone)
								VALUES(
								'".$rtrack_id."',
								'".($rtrack_enabled ? 1 : 0)."',
								'u',
								'{$user_id}'
						)";
						$result=$db->sql_query($sql);
					}
					else
					{

						$sql='UPDATE '.TRACKER_RTRACK_TABLE." SET
							rtracker_enabled='".($rtrack_enabled ? 1 : 0)."'
							WHERE rtracker_id='".$rtrack_id."' AND rtracker_type='u' AND user_torrent_zone='{$user_id}'";
						$result=$db->sql_query($sql);
					}

					$rtrack_count+=1;
				}
			}
			if($d_rtrack)
			{
				$sql='DELETE FROM '.TRACKER_RTRACK_TABLE.' WHERE '.$db->sql_in_set('rtracker_id', $d_rtrack)." AND user_torrent_zone='{$user_id}' AND rtracker_type='u'";
				$result=$db->sql_query($sql);
			}
			trigger_error($user->lang['USER_PREFS_UPDATED'] . adm_back_link($this->u_action . '&amp;u=' . $user_id));
		}
		$error ? trigger_error($error . adm_back_link($this->u_action . '&amp;u=' . $user_id)) : '';
	}
	else
	{
		$error[] = 'FORM_INVALID';
	}
	// Replace "error" strings with their real, localised form
	$error = preg_replace('#^([A-Z_]+)$#e', "(!empty(\$user->lang['\\1'])) ? \$user->lang['\\1'] : '\\1'", $error);
}

$user->add_lang('mods/acp/ppkbb3cker_rtracker');

if(request_var('add_new', ''))
{
	$template->assign_block_vars('rtracks', array(
		'COUNT'	=> 0,
		'URL'	=> '',
		'ENABLED' => '',

		)
	);
	$template->assign_vars(array(
		'S_NEW_RTRACK'		=> true,
		)
	);
}
else
{
	$sql_where="rt.user_torrent_zone='{$user_id}' AND rt.rtracker_type='u'";
	$sql='SELECT rs.id, rs.rtracker_url rtrack_url, rt.rtracker_enabled rtrack_enabled FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND {$sql_where}";
	$result=$db->sql_query($sql);
	while($row=$db->sql_fetchrow($result))
	{
		$template->assign_block_vars('rtracks', array(
			'COUNT'	=> $row['id'],
			'URL'	=> htmlspecialchars($row['rtrack_url']),
			'ENABLED' => $row['rtrack_enabled'] ? ' checked="checked"' : '',

			)
		);
	}
	$db->sql_freeresult($result);
	$template->assign_vars(array(
		'S_VIEW_RTRACK'		=> true,
		)
	);
}

$template->assign_vars(array(
	'S_HIDDEN_FIELD' => '<input type="hidden" name="add_rtrack" value="1" />',
	'S_RTRACKER'		=> true,
	'ERROR'			=> (sizeof($error)) ? implode('<br />', $error) : '',
	)
);

?>
