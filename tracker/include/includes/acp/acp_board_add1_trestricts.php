<?php
/**
*
* @package ppkBB3cker
* @version $Id: acp_board_add1_trestricts.php 1.000 2018-04-08 10:48:00 PPK $
* @copyright (c) 2018 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$user->add_lang('mods/acp/ppkbb3cker_trestricts');

$trestricts_title='ACP_TRACKER_TRESTRICTS';

$trestricts_user_ratio=request_var('trestricts_user_ratio', array(0=>''));
$trestricts_user_updown=request_var('trestricts_user_updown', array(0=>0));
$user_updownv=request_var('user_updownv', array(0=>''));
$trestricts_time_count=request_var('trestricts_time_count', array(0=>0));
$time_countv=request_var('time_countv', array(0=>''));
$trestricts_enabled=request_var('trestricts_enabled', array(0=>0));
$trestricts_delete=request_var('trestricts_delete', array(0=>0));

$submit=request_var('submit', '');
$add_trestricts=request_var('add_trestricts', '');
$view_trestricts=request_var('view_trestricts', '');

$valid_trestricts=array('ppkbb_tcwait_time', 'ppkbb_tcwait_time2', 'ppkbb_tcmaxleech_restr');
in_array($view_trestricts, $valid_trestricts) ? '' : $view_trestricts='';

if(!class_exists('timedelta'))
{
	$user->add_lang('mods/posts_merging');
	require($phpbb_root_path . 'includes/time_delta.'.$phpEx);
}
$td = new timedelta();

if($submit && $add_trestricts)
{
	$delete_trestricts=array();
	foreach($trestricts_user_ratio as $k=>$v)
	{
		if(isset($trestricts_delete[$k]))
		{
			$delete_trestricts[]=my_int_val($k);
		}
		else
		{
			if(!in_array($trestricts_user_ratio[$k], array('Inf.', 'Leech.', 'Seed.', 'None.')) && !preg_match('#\d+\.\d+#', $trestricts_user_ratio[$k]))
			{
				$trestricts_user_ratio[$k]='';
			}
			$view_trestricts=='ppkbb_tcmaxleech_restr' ? '' : $trestricts_time_count[$k]=get_time_value($time_countv[$k], $trestricts_time_count[$k]);
			$trestricts_user_updown[$k]=get_size_value($user_updownv[$k], $trestricts_user_updown[$k]);
			if($k==0 && $trestricts_user_ratio[$k]!=='')
			{
				$sql='INSERT INTO '.TRACKER_TRESTRICTS_TABLE." (user_ratio, time_count, user_updown, restrict_enabled, restrict_type) VALUES(
					'".$db->sql_escape($trestricts_user_ratio[$k])."',
					'".$trestricts_time_count[$k]."',
					'".$trestricts_user_updown[$k]."',
					'".(isset($trestricts_enabled[$k]) ? 1 : 0)."',
					'".$view_trestricts."'
				)";
				$result=$db->sql_query($sql);
			}
			else if($trestricts_user_ratio[$k]!=='')
			{
				$sql='UPDATE '.TRACKER_TRESTRICTS_TABLE." SET
					user_ratio='".$db->sql_escape($trestricts_user_ratio[$k])."',
					time_count='".$trestricts_time_count[$k]."',
					user_updown='".$trestricts_user_updown[$k]."',
					restrict_enabled='".(isset($trestricts_enabled[$k]) ? 1 : 0)."'
					WHERE id='{$k}'";
				$result=$db->sql_query($sql);
			}
		}
	}
	if($delete_trestricts)
	{
		$sql='DELETE FROM '.TRACKER_TRESTRICTS_TABLE.' WHERE '.$db->sql_in_set('id', $delete_trestricts);
		$result=$db->sql_query($sql);
	}
}

$sort_trestricts=request_var('sort_trestricts', 0);
$trestricts=request_var('trestricts', array(0=>0));
if($view_trestricts && $sort_trestricts && $trestricts)
{

	$sql='SELECT restrict_type FROM '.TRACKER_TRESTRICTS_TABLE." WHERE restrict_type='{$view_trestricts}'";
	$result=$db->sql_query($sql);
	if($db->sql_fetchrow($result))
	{
		foreach($trestricts as $k => $v)
		{
			$k=my_int_val($k);
			$v=my_int_val($v);
			if($k && $v)
			{
				$db->sql_query("UPDATE ".TRACKER_TRESTRICTS_TABLE." SET restrict_order='{$k}' WHERE id='{$v}' AND restrict_type='{$view_trestricts}'");
			}
		}
		exit($user->lang['TRESTRICTS_SORT_SUCCESS']);
	}

}
else if($view_trestricts)
{
	switch($view_trestricts)
	{
		case 'ppkbb_tcwait_time':
			$user->lang['TIME_COUNT_EXPLAIN']=$user->lang['TIME_COUNT_TIME_EXPLAIN'];
			$user->lang['USER_UPDOWN']=$user->lang['USER_UPDOWN_UP'];
			$user->lang['TIME_COUNT']=$user->lang['TIME_COUNT_TIME'];

		break;
		case 'ppkbb_tcwait_time2':
			$user->lang['TIME_COUNT_EXPLAIN']=$user->lang['TIME_COUNT_TIME_EXPLAIN'];
			$user->lang['USER_UPDOWN']=$user->lang['USER_UPDOWN_DOWN'];
			$user->lang['TIME_COUNT']=$user->lang['TIME_COUNT_TIME'];

		break;
		case 'ppkbb_tcmaxleech_restr':
			$user->lang['TIME_COUNT_EXPLAIN']=$user->lang['TIME_COUNT_COUNT_EXPLAIN'];
			$user->lang['USER_UPDOWN']=$user->lang['USER_UPDOWN_DOWN'];
			$user->lang['TIME_COUNT']=$user->lang['TIME_COUNT_COUNT'];

		break;
	}
	$user->lang['ACP_TRACKER_TRESTRICTS_EXPLAIN']=$user->lang[strtoupper($view_trestricts)];

	$trestricts_title='ACP_TRACKER_TRESTRICTS';
	$form_trestricts=request_var('form_trestricts', '');
	if($form_trestricts)
	{
		$sql='SELECT user_ratio FROM '.TRACKER_TRESTRICTS_TABLE." WHERE restrict_type='{$view_trestricts}'";
		$result=$db->sql_query($sql);
		$trestricts_exists=array();
		while($row=$db->sql_fetchrow($result))
		{
			$trestricts_exists[$row['user_ratio']]=1;
		}
		$db->sql_freeresult($result);

		$template->assign_block_vars('trestricts', array(
			'ID'	=> 0,
			'USER_RATIO'	=> '',
			'USER_UPDOWN'	=> '',
			'USER_UPDOWN_V'	=> '<select name="user_updownv[0]">'.select_size_value().'</select>',
			'ORDER'	=> 255,
			'TIME_COUNT'	=> 0,
			'TIME_COUNT_V'	=> $view_trestricts=='ppkbb_tcmaxleech_restr' ? '' : '<select name="time_countv[0]">'.select_time_value().'</select>',
			'ENABLED'	=> false,
			)
		);

		$template->assign_vars(array(
			'TRESTRICTS_TYPE' => $user->lang['TRESTRICTS_'.strtoupper($view_trestricts)],
			'S_HIDDEN_FIELDS'=>'<input type="hidden" name="add_trestricts" value="1" /><input type="hidden" name="view_trestricts" value="'.$view_trestricts.'" />',

			'S_NEW_TRESTRICTS'	=> true,
			'S_FORM_TRESTRICTS'	=> true,

			)
		);

	}
	else
	{

		$sql='SELECT * FROM '.TRACKER_TRESTRICTS_TABLE." WHERE restrict_type='{$view_trestricts}' ORDER BY restrict_order";
		$result=$db->sql_query($sql);
		while($row=$db->sql_fetchrow($result))
		{

			$template->assign_block_vars('trestricts', array(
				'ID'	=> $row['id'],
				'USER_RATIO'	=> $row['user_ratio'],
				'USER_UPDOWN'	=> $row['user_updown'],
				'USER_UPDOWN_VALUE'	=> get_formatted_filesize($row['user_updown']),
				'USER_UPDOWN_V'	=> '<select name="user_updownv['.$row['id'].']">'.select_size_value().'</select>',
				'ORDER'	=> $row['restrict_order'],
				'TIME_COUNT'	=> $row['time_count'],
				'TIME_COUNT_VALUE'	=> $view_trestricts=='ppkbb_tcmaxleech_restr' ? false : $td->spelldelta(0, $row['time_count']),
				'TIME_COUNT_V'	=> $view_trestricts=='ppkbb_tcmaxleech_restr' ? '' : '<select name="time_countv['.$row['id'].']">'.select_time_value().'</select>',
				'ENABLED'	=> $row['restrict_enabled'] ? true : false,
				)
			);
		}
		$db->sql_freeresult($result);

		$template->assign_vars(array(
			'TRESTRICTS_TYPE' => isset($user->lang['TRESTRICTS_'.strtoupper($view_trestricts)]) ? $user->lang['TRESTRICTS_'.strtoupper($view_trestricts)] : $view_trestricts,
			'S_HIDDEN_FIELDS'=>'<input type="hidden" name="add_trestricts" value="1" /><input type="hidden" name="view_trestricts" value="'.$view_trestricts.'" />',

			'S_NEW_TRESTRICTS'	=> true,
			'S_SORT_TRESTRICTS' => append_sid("{$phpbb_admin_path}index.$phpEx", "i=board&mode=trestricts&sort_trestricts=1&view_trestricts={$view_trestricts}", false),
			)
		);
	}
}
else
{

	$trestricts_count=array();

	$sql='SELECT COUNT(*) trestricts_count, restrict_type FROM '.TRACKER_TRESTRICTS_TABLE.' GROUP BY restrict_type';
	$result=$db->sql_query($sql);
	while($row=$db->sql_fetchrow($result))
	{
		$trestricts_count[$row['restrict_type']]=(int) $row['trestricts_count'];
	}
	$db->sql_freeresult($result);

	foreach($valid_trestricts as $v)
	{
		$template->assign_block_vars('restrict_type', array(
			'TRESTRICTS_NAME' => isset($user->lang['TRESTRICTS_'.strtoupper($v)]) ? $user->lang['TRESTRICTS_'.strtoupper($v)] : $v,
			'U_TRESTRICTS_EDIT' => append_sid("{$phpbb_admin_path}index.$phpEx", 'i=board&amp;mode=trestricts&amp;view_trestricts='.$v),
			'TRESTRICTS_COUNT' => isset($trestricts_count[$v]) ? $trestricts_count[$v] : 0,
			)
		);
	}

	$template->assign_vars(array(
		'S_VIEW_TRESTRICTS'	=> false,

		'S_TRACKER_NOBUTT' => true,
		)
	);

}

$display_vars = array(
	'title'	=> $trestricts_title,
	'vars'	=> array(
		'legend1'				=> 'ACP_TRACKER_TRESTRICTS_SETTINGS',
	)
);

$template->assign_vars(array(
	'S_TRESTRICTS_INC'	=> true,
	)
);

?>
