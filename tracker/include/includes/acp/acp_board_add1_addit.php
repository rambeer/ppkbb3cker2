<?php
/**
*
* @package ppkBB3cker
* @version $Id: acp_board_add1_addit.php 1.000 2021-03-29 09:24:00 PPK $
* @copyright (c) 2021 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$user->add_lang('mods/acp/ppkbb3cker_addit');

$display_vars = array(
	'title'	=> 'ACP_ADDIT_SETTINGS',
	'vars'	=> array(
		'legend1'				=> 'ACP_ADDIT_SETTINGS',
		'ppkbb_disable_fpquote'		=> array('lang' => 'PPKBB_DISABLE_FPQUOTE', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
		'ppkbb_subject_textlength'		=> array('lang' => 'PPKBB_SUBJECT_TEXTLENGTH', 'validate' => 'int:0', 'type' => 'text:3:3', 'method' => false, 'explain' => true,),
		'ppkbb_hicons_fields'		=> array('lang' => 'TRACKER_HICONS_FIELDS', 'validate' => 'string', 'type' => 'text:48:255', 'method' => false, 'explain' => true,),
		'ppkbb_forums_qr'		=> array('lang' => 'TRACKER_FORUMS_QR', 'validate' => 'array', 'type' => 'array:1:1', 'method' => false, 'explain' => true,),
		'ppkbb_subforumslist'		=> array('lang' => 'TRACKER_SUBFORUMLIST', 'validate' => 'string', 'type' => 'text:1:1', 'method' => false, 'explain' => true,),
		'ppkbb_fpep_enable'		=> array('lang' => 'PPKBB_FPEP_ENABLE', 'validate' => 'array', 'type' => 'array:1:1', 'method' => false, 'explain' => true,),
		'ppkbb_max_mfu'		=> array('lang' => 'PPKBB_MAX_MFU', 'validate' => 'int:0', 'type' => 'text:2:2', 'method' => false, 'explain' => true,),
		'ppkbb_thanks_enable'		=> array('lang' => 'PPKBB_THANKS_ENABLE', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
		'ppkbb_cssjs_cache'		=> array('lang' => 'PPKBB_CSSJS_CACHE', 'validate' => 'array', 'type' => 'array:5:5', 'method' => false, 'explain' => true,),
		'ppkbb_forumid_remove'		=> array('lang' => 'PPKBB_FORUMID_REMOVE', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
		'ppkbb_addfields_type'		=> array('lang' => 'TRACKER_ADDFIELDS_TYPE', 'validate' => 'array', 'type' => 'array:4:4', 'method' => false, 'explain' => true,),

		'legend2'				=> 'ACP_SPOILER_SETTINGS',
		'ppkbb_maxspoiler_depth'		=> array('lang' => 'SPOILER_DEPTH_LIMIT', 'validate' => 'int:0', 'type' => 'text:1:1', 'method' => false, 'explain' => true,),
		'ppkbb_spoiler_options'		=> array('lang' => 'SPOILER_OPTIONS', 'validate' => 'array', 'type' => 'array:16:16', 'method' => false, 'explain' => true,),
		'ppkbb_spoiler_banned_imghosts'		=> array('lang' => 'SPOILER_BANNED_IMGHOSTS', 'validate' => 'string', 'type' => 'text:48:1024', 'method' => false, 'explain' => true,),
	)
);
?>
