<?php
/**
*
* @package ppkBB3cker
* @version $Id: tracker_add_cron.php 1.000 2019-11-23 09:08:00 PPK $
* @copyright (c) 2019 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$dt=time();
// $tracker_cron_data=get_tracker_cron();
// $current_page_name=str_replace(".$phpEx", '', $user->page['page_name']);
if(isset($tracker_cron_data[$current_page_name]))
{
	$cron_uid=0;
	if(!$config['ppkbb_cron_options'][2])
	{
		$cron_uid=gen_rand_string();
	}

	$user_data=isset($member) ? array('user_id'=>$member['user_id'], 'user_tracker_data'=>$member['user_tracker_data'], 'current_user_id'=>$user->data['user_id']) : array('user_id'=>$user->data['user_id'], 'user_tracker_data'=>$user->data['user_tracker_data'], 'current_user_id'=>$user->data['user_id']);

	$user_tracker_data=my_split_config($user_data['user_tracker_data'], 4, 'my_int_val');

	isset($torrents_cleanup) ? '' : $torrents_cleanup=array();
	isset($torrents_hashes) ? '' : $torrents_hashes=array();
	isset($torrents_slspeed) ? '' : $torrents_slspeed=array();

	$last_dtad=array();
	$last_dtad['last_dtad']=isset($forum_data['last_dtad']) ? $forum_data['last_dtad'] : 0;
	$last_dtad['forum_id']=isset($forum_data['forum_id']) ? $forum_data['forum_id'] : 0;

	$custom_last_run=array(
		'user_count_torrents'=>isset($user_tracker_data[0]) ? $user_tracker_data[0] : 0,
		'user_count_comments'=>isset($user_tracker_data[1]) ? $user_tracker_data[1] : 0,
		'torrent_recount_seed_leech'=>0,
		'torrent_recount_seed_leech_speed'=>0,
		'torrent_announce_remote_trackers'=>0,
		'torrent_delete_dead_torrents'=>$last_dtad['last_dtad'],
	);

	$data_source=array(
		'torrent_recount_seed_leech'=>$torrents_cleanup,
		'torrent_recount_seed_leech_speed'=>$torrents_slspeed,
		'torrent_announce_remote_trackers'=>$torrents_hashes,
		'torrent_delete_dead_torrents'=>$last_dtad,
		'user_count_torrents'=>$user_data,
		'user_count_comments'=>$user_data,
	);
	$sql_values=array();
	$i=1;
	foreach($tracker_cron_data[$current_page_name] as $cron_data)
	{
		if($config['ppkbb_cron_options'][4] && $i > $config['ppkbb_cron_options'][4])
		{
			break;
		}
		$i+=1;
		$cron_name=$cron_data['cron_name'];
		$start_run=$cron_data['start_run'];
		$end_run=$cron_data['end_run'];
		$cron_options=$cron_data['cron_options'];
		$from_guest=$cron_data['from_guest'];
		$run_period=$cron_data['run_period'];

		$run_by_start_end=false;
		$run_by_period=false;
		$check_start_end=false;
		$check_period=false;

		$cron_name_last_run=isset($custom_last_run[$cron_name]) ? $custom_last_run[$cron_name] : $config['last_'.$cron_name];
		if($start_run!=='' && $end_run!=='' && $start_run!=$end_run)
		{
			$check_start_end=true;
			$current_hour=date('H');
			$start_run=(int) $start_run;
			$end_run=(int) $end_run;
// 			$current_day_end=mktime(23, 59, 59, date('m'), date('d'), date('Y'));
// 			$next_day_start=$current_day_end + 1;
// 			$hourmin_start_run=explode(':', $start_run);
// 			$hourmin_end_run=explode(':', $end_run);
// 			$start_time_run=mktime($hourmin_start_run[0], $hourmin_start_run[1], 0, date('m'), date('d'), date('Y'));
// 			$end_time_run=mktime($hourmin_end_run[0], $hourmin_end_run[1], 0, date('m'), date('d'), date('Y')) + (24 * 3600);
// 			$start_end_period=$end_time_run - $start_time_run;

			$between_start_end=false;
			if($start_run > $end_run)
			{
				$current_hour >= $start_run || $current_hour < $end_run ? $between_start_end=true : '';
			}
			else if($start_run < $end_run)
			{
				$current_hour >= $start_run && $current_hour < $end_run ? $between_start_end=true : '';
			}

			if($between_start_end)
			{
				if(!$cron_name_last_run || $dt - $cron_name_last_run > 86400)
				{
					$run_by_start_end=true;
				}
			}
		}
		if($run_period)
		{
			$check_period=true;
			if(!$cron_name_last_run || $dt - $cron_name_last_run > $run_period)
			{
				$run_by_period=true;
			}
		}
		if(!$from_guest && !$user->data['is_registered'])
		{

		}
		else
		{
			if($config['ppkbb_cron_options'][2])
			{
				$key=$cron_uid=gen_rand_string();
			}
			else
			{
				$key=$i;
			}
			$sql_value='';
			$current_cron_data=isset($data_source[$cron_name]) ? $data_source[$cron_name] : array();
			if($check_period && $check_start_end)
			{
				$sql_value=$run_by_period && $run_by_start_end ? add_sql_data($cron_uid, $cron_name, $current_cron_data) : '';
// 				$sql_value ? $sql_values[$key]="('$cron_name', '$sql_value', $dt, '$cron_uid', '$cron_md5')" : '';
			}
			else if($check_period && !$check_start_end)
			{
				$sql_value=$run_by_period ? add_sql_data($cron_uid, $cron_name, $current_cron_data) : '';
// 				$sql_value ? $sql_values[$key]="('$cron_name', '$sql_value', $dt, '$cron_uid', '$cron_md5')" : '';
			}
			else if(!$check_period && $check_start_end)
			{
				$sql_value=$run_by_start_end ? add_sql_data($cron_uid, $cron_name, $current_cron_data) : '';
// 				$sql_value ? $sql_values[$key]="('$cron_name', '$sql_value', $dt, '$cron_uid', '$cron_md5')" : '';
			}
			if($sql_value)
			{
				$cron_md5=md5($cron_name.':'.$sql_value);
				$sql_values[$key]="('$cron_name', '$sql_value', $dt, '$cron_uid', '$cron_md5')";
			}
		}
	}
	if($sql_values)
	{
		$sql="INSERT IGNORE INTO ".TRACKER_CRON_TABLE.'(type, data, added, uid, md5sum) VALUES';
		$sql.=implode(', ', $sql_values);
		$result=$db->sql_query($sql);
		if($db->sql_nextid())
		{
			if(!$config['ppkbb_cron_options'][2])
			{
				$template->assign_block_vars('tracker_cron', array(
					'CRON_TASK'=>'<img src="' . append_sid($phpbb_root_path . 'tracker/cron.' . $phpEx, 'uid[]='.$cron_uid) . '" alt="tracker_cron" width="1" height="1" />'
					)
				);
			}
			else
			{
				foreach($sql_values as $cron_uid => $value)
				{
					$template->assign_block_vars('tracker_cron', array(
						'CRON_TASK'=>'<img src="' . append_sid($phpbb_root_path . 'tracker/cron.' . $phpEx, 'uid[]='.$cron_uid) . '" alt="tracker_cron" width="1" height="1" />'
						)
					);
				}
			}
		}
	}
}

function add_sql_data($cron_uid, $cron_name, $data)
{
	global $db, $config, $phpbb_root_path, $phpEx, $template, $dt;

	switch($cron_name)
	{
		case 'tracker_delete_dead_peers':
			return $db->sql_escape(serialize(array()));
		break;
		case 'torrent_recount_seed_leech':
			return $data ? $db->sql_escape(serialize($data)) : 0;
		break;
		case 'torrent_recount_seed_leech_speed':
			return $data ? $db->sql_escape(serialize($data)) : 0;
		break;
		case 'torrent_announce_remote_trackers':
			$torrents_hashes=$data;

			$torrents_remote=array();
			if(sizeof($torrents_hashes))
			{
				$r_torr=array('all'=>array(), 'torr'=>array());

				$r_exs=array();
				$torrents_id=array_keys($torrents_hashes);

				$sql='SELECT rt.rtracker_id tracker, rt.user_torrent_zone torrent FROM '.TRACKER_RTRACK_TABLE." rt WHERE rt.rtracker_enabled='1' AND
				(
					(rt.rtracker_remote='1' AND rt.user_torrent_zone='0' AND rt.rtracker_type='s')
						OR
					(".$db->sql_in_set('rt.user_torrent_zone', $torrents_id)." AND rt.rtracker_type='t')
				)";
				$result=$db->sql_query($sql);
				$ra=array();
				while($row_remote=$db->sql_fetchrow($result))
				{
					$ra[$row_remote['tracker']]=$row_remote['torrent'];
					if(!$row_remote['torrent'])
					{
						$r_torr['all'][$row_remote['tracker']]=$torrents_hashes;
					}
					else
					{
						isset($torrents_hashes[$row_remote['torrent']]) ? $r_torr['torr'][$row_remote['tracker']][$row_remote['torrent']]=$torrents_hashes[$row_remote['torrent']] : '';
					}
				}
				$db->sql_freeresult($result);

				$sql='SELECT tracker, torrent, next_announce FROM '.TRACKER_RANNOUNCES_TABLE.' WHERE '.$db->sql_in_set('torrent', $torrents_id);
				$result=$db->sql_query($sql);
				while($row_remote=$db->sql_fetchrow($result))
				{
					if(isset($ra[$row_remote['tracker']]))
					{
						$r_exs[$ra[$row_remote['tracker']].'_'.$row_remote['torrent']][$row_remote['tracker']]=$row_remote;
					}
				}
				$db->sql_freeresult($result);
				if(isset($r_torr['all']))
				{
					foreach($r_torr['all'] as $tr_id => $a_data)
					{
						foreach($a_data as $t_id => $t_hash)
						{
							if(isset($r_exs['0_'.$t_id][$tr_id]))
							{
								if($dt > $r_exs['0_'.$t_id][$tr_id]['next_announce'])
								{
									$torrents_remote[$t_id]=$torrents_hashes[$t_id];
								}
							}
							else
							{
								$torrents_remote[$t_id]=$torrents_hashes[$t_id];
							}
						}
					}
					unset($r_torr['all']);
				}
				if(isset($r_torr['torr']))
				{
					foreach($r_torr['torr'] as $tr_id => $a_data)
					{
						foreach($a_data as $t_id => $t_hash)
						{
							if(isset($r_exs[$t_id.'_'.$t_id][$tr_id]))
							{
								if($dt > $r_exs[$t_id.'_'.$t_id][$tr_id]['next_announce'])
								{
									$torrents_remote[$t_id]=$torrents_hashes[$t_id];
								}
							}
							else
							{
								$torrents_remote[$t_id]=$torrents_hashes[$t_id];
							}
						}
					}
					unset($r_torr['torr']);
				}
				unset($torrents_hashes);
			}

			return $torrents_remote ? $db->sql_escape(serialize(array_map('bin2hex', $torrents_remote))) : 0;
		break;
		case 'torrent_fix_torrents_statuses':
			return $db->sql_escape(serialize(array()));
		break;
		case 'torrent_delete_dead_torrents':
			if($data['forum_id'] && $config['ppkbb_deadtorrents_autodelete'][0] && $config['ppkbb_deadtorrents_autodelete'][1] && $config['ppkbb_deadtorrents_autodelete'][3]!=$data['forum_id'])
			{
				$template->assign_var('RUN_CRON_TASK', '<img src="' . append_sid($phpbb_root_path . 'cron.' . $phpEx, "cron_type=$cron_name&amp;f=" . $data['forum_id']) . '" alt="cron" width="1" height="1" />');

// 				return $db->sql_escape(serialize($data));
			}
			return 0;
		break;
		case 'stat_count_seed_leech':
			return $db->sql_escape(serialize(array()));
		break;
		case 'stat_count_upload_download':
			return $db->sql_escape(serialize(array()));
		break;
		case 'stat_count_session_upload_download':
			return $db->sql_escape(serialize(array()));
		break;
		case 'stat_count_torrents_size':
			return $db->sql_escape(serialize(array()));
		break;
		case 'stat_count_upload_download_speed':
			return $db->sql_escape(serialize(array()));
		break;
		case 'stat_count_torrents':
			return $db->sql_escape(serialize(array()));
		break;
		case 'stat_count_comments':
			return $db->sql_escape(serialize(array()));
		break;
		case 'stat_count_thanks':
			return $db->sql_escape(serialize(array()));
		break;
		case 'stat_count_torrentsseed_usersseed':
			return $db->sql_escape(serialize(array()));
		break;
		case 'stat_count_torrentsleech_usersleech':
			return $db->sql_escape(serialize(array()));
		break;
		case 'user_count_torrents':
			return $data ? $db->sql_escape(serialize($data)) : 0;
		break;
		case 'user_count_comments':
			return $data ? $db->sql_escape(serialize($data)) : 0;
		break;
	}

	return '';
}

?>
