<?php
/**
*
* @package ppkBB3cker
* @version $Id: rtrackfunc.php 1.000 2009-05-29 18:13:00 PPK $
* @copyright (c) 2009 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

function get_user_zone($ip, $name=0)
{
	global $db;

	$ip=check_ip($ip);
	$zone=array();
	$sql_from=$sql_where=$sql_select='';

	if($ip)
	{
		if($name)
		{
			$sql_from=', '. TRACKER_ZONES_NAME.' n';
			$sql_where=" AND a.zone_id=n.id AND n.zenabled='1'";
			$sql_select=', n.zone_name';
		}

		$sql="SELECT a.zone_id{$sql_select} FROM ".TRACKER_ZONES_ADDR." a{$sql_from} WHERE INET_ATON('{$ip}') >= INET_ATON(a.addr_start) AND INET_ATON('{$ip}') <= INET_ATON(a.addr_end){$sql_where} AND a.zenabled='1' LIMIT 1";
		$result=$db->sql_query($sql);
		$zone=$db->sql_fetchrow($result);
		$db->sql_freeresult($result);

		if($zone['zone_id'])
		{
			return $zone;
		}
	}

	return $zone;
}

function get_rem_rtrack($t)
{
	$sql_where='';
	$rem_rtracks_array=array();

	$sql_where.="(rt.rtracker_remote='1' AND rt.user_torrent_zone='0' AND rt.rtracker_type='s')";
	$t ? $sql_where.=" OR (rt.user_torrent_zone='{$t}' AND rt.rtracker_type='t')" : '';

	$sql='SELECT rs.id, rs.rtracker_url rtrack_url FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND rt.rtracker_enabled='1' AND ({$sql_where})";
	$result=$db->sql_query($sql);
// 	$e_urtracks=0;
	while($row=$db->sql_fetchrow($result))
	{
		$rem_rtracks_array[$row['id']]=$row;
	}
	$db->sql_freeresult($result);

	return $rem_rtracks_array;
}

function get_rtrack($ip, $type=1, $users=0, $torrent=0, $forb_rtracks=array())
{
	global $db, $user;

	$rtracks_array=$sql_where=array();
	$user_zone=0;

	if($type==1)
	{
		$sql_where[]="(rt.rtracker_remote!='0' AND rt.user_torrent_zone='0' AND rt.rtracker_type='s')";
	}
	else if($type==2)
	{
		$user_zone=get_user_zone($ip);
		if($user_zone)
		{
			$sql_where[]="(rt.rtracker_remote='0' AND rt.user_torrent_zone='{$user_zone['zone_id']}' AND rt.rtracker_type='u')";
		}
	}
	else if($type==3)
	{
		$sql_where[]="(rt.rtracker_remote!='0' AND rt.user_torrent_zone='0' AND rt.rtracker_type='s')";
		$user_zone=get_user_zone($ip);
		if($user_zone)
		{
			$sql_where[]="(rt.rtracker_remote='0' AND rt.user_torrent_zone='{$user_zone['zone_id']}' AND rt.rtracker_type='u')";
		}
	}
	if($users)
	{
		$sql_where[]="(rt.user_torrent_zone='{$user->data['user_id']}' AND rt.rtracker_type='u')";
	}
	if($torrent)
	{
		$sql_where[]="(rt.user_torrent_zone='{$torrent}' AND rt.rtracker_type='t')";
	}

	if($sql_where)
	{
		$sql='SELECT rs.id, rs.rtracker_url rtrack_url, rtracker_type rtrack_type, user_torrent_zone torrent FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND rt.rtracker_enabled='1' AND (".(implode(' OR ', $sql_where)).") AND rt.rtracker_forb='0'";
		$result=$db->sql_query($sql);
		$e_urtracks=0;
		while($row=$db->sql_fetchrow($result))
		{
			$rtrack_forb=0;
			if(sizeof($forb_rtracks))
			{
				foreach($forb_rtracks as $f)
				{
					if($row['rtrack_type']=='u' && in_array($f['rtrack_forb'], array(2, 3)))
					{
						if($f['forb_type']=='s' && strstr($row['rtrack_url'], $f['rtrack_url']))
						{
							$rtrack_forb=1;
						}
						else if($f['forb_type']=='i' && stristr($row['rtrack_url'], $f['rtrack_url']))
						{
							$rtrack_forb=1;
						}
						else if($f['forb_type']=='r' && preg_match("{$f['rtrack_url']}", $row['rtrack_url']))
						{
							$rtrack_forb=1;
						}
					}
					else if($row['rtrack_type']=='t' && in_array($f['rtrack_forb'], array(1, 3)))
					{
						if($f['forb_type']=='s' && strstr($row['rtrack_url'], $f['rtrack_url']))
						{
							$rtrack_forb=1;
						}
						else if($f['forb_type']=='i' && stristr($row['rtrack_url'], $f['rtrack_url']))
						{
							$rtrack_forb=1;
						}
						else if($f['forb_type']=='r' && preg_match("{$f['rtrack_url']}", $row['rtrack_url']))
						{
							$rtrack_forb=1;
						}
					}
				}
			}
			if(!$rtrack_forb)
			{
				if($row['rtrack_type']=='u')
				{
					$e_urtracks+=1;
					if(!$users || $e_urtracks > $users)
					{
						continue;
					}
				}
				$rtracks_array[$row['id']]=$row;
			}
		}
		$db->sql_freeresult($result);
	}

	return $rtracks_array;
}

function benc_rtrack_url($a, $a_ex=0, $tracker_announce='')
{
	global $user, $config;

	$a_announce=$rtracks_url=array();

	$tracker_announce ? $a[0]['rtrack_url']=$tracker_announce : '';

	if($a)
	{
		$a=array_reverse($a);

		foreach($a as $i => $a_url)
		{
			$rtrack_url=$a_url['rtrack_url'];
			if(!in_array($rtrack_url, $rtracks_url))
			{
				$rtrack_url=str_replace('{YOUR_PASSKEY}', $user->data['user_passkey'], $rtrack_url);
				$rtracks_url[]=$rtrack_url;
				$a_announce[][$i] = $rtrack_url;
			}
		}
	}
	if((($a_ex==1) || ($a_ex==2 && $user->data['is_registered']) || ($a_ex==3 && !$user->data['is_registered'])) && sizeof($a_announce) > 1)
	{
		unset($a_announce[0]);
	}

	return $a_announce;
}

function magnet_rtrack_url($a, $a_ex=0, $tracker_announce='')
{
	global $user, $config;

	$a_announce=$rtracks_url=array();

	$magnet_amp=strpos($tracker_announce, '?')!==false ? '&' : '?';
	$tracker_announce ? $a[0]['rtrack_url']=$tracker_announce.$magnet_amp.'magnet=1' : '';

	if($a)
	{
		$a=array_reverse($a);

		foreach($a as $i => $a_url)
		{
			$rtrack_url=$a_url['rtrack_url'];
			if(!in_array($rtrack_url, $rtracks_url))
			{
				$rtrack_url=str_replace('{YOUR_PASSKEY}', $user->data['user_passkey'], $rtrack_url);
				$rtracks_url[]=$rtrack_url;
				$a_announce[$i] = $rtrack_url;
			}
		}
	}
	if((($a_ex==1) || ($a_ex==2 && $user->data['is_registered']) || ($a_ex==3 && !$user->data['is_registered'])) && sizeof($a_announce) > 1)
	{
		unset($a_announce[0]);
	}

	return implode('&tr=', array_map('urlencode', $a_announce));
}

?>
