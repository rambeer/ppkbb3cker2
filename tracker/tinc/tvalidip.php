<?php
/**
*
* @package ppkBB3cker
* @version $Id: tvalidip.php 1.000 2009-08-13 15:12:00 PPK $
* @copyright (c) 2009 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

define('TRACKER_ZONES_NAME', $table_prefix . 'tracker_zname');
define('TRACKER_ZONES_ADDR', $table_prefix . 'tracker_zaddr');

$tvalidip=t_getcache('tracker_tvalidip');

if($tvalidip===false)
{
	$tvalidip=array();
	include_once("{$tincludedir}tcache.{$phpEx}");

	$sql="SELECT a.addr_start start, a.addr_end end FROM ".TRACKER_ZONES_NAME." n, ".TRACKER_ZONES_ADDR." a WHERE n.zone_type='r' AND n.id=a.zone_id AND n.zenabled='1' AND a.zenabled='1'";
	$result=my_sql_query($sql);
	while($row=mysql_fetch_array($result))
	{
		$tvalidip[]=array($row['start'], $row['end']);
	}
	mysql_free_result($result);

	t_recache('tracker_tvalidip', $tvalidip);
}

unset($tvalidip['blockedip_cachetime']);

if ($tvalidip && !validip($ip, $tvalidip))
{
	err('Invalid or reserved IP');
}

function validip($ip, $a=array())
{
	$ip=sprintf("%u", ip2long($ip));

	if ($ip && $a)
	{
		foreach ($a as $r)
		{
			$min = sprintf("%u", ip2long($r[0]));
			$max = sprintf("%u", ip2long($r[1]));
			if($ip >= $min && $ip <= $max)
			{
				return false;
			}
		}
		return true;
	}
	else
	{
		return true;
	}
}
?>
