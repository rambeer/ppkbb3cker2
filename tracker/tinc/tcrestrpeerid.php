<?php
/**
*
* @package ppkBB3cker
* @version $Id: tcrestrpeerid.php 1.000 2009-11-15 18:07:00 PPK $
* @copyright (c) 2009 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if(!defined('TRACKER_CRESTRICTS_TABLE'))
{
	define('TRACKER_CRESTRICTS_TABLE', $table_prefix . 'tracker_crestricts');
}

$crestrpeerid=t_getcache('tracker_crestrpeerid');

if($crestrpeerid===false)
{
	$crestrpeerid=array();
	include_once("{$tincludedir}tcache.{$phpEx}");

	$sql="SELECT rstart, rtype FROM ".TRACKER_CRESTRICTS_TABLE." WHERE rid='3' AND renabled='1'";
	$result=my_sql_query($sql);
	while($row=mysql_fetch_array($result))
	{
		$crestrpeerid[]=array($row['rstart'], $row['rtype']);
	}
	mysql_free_result($result);

	t_recache('tracker_crestrpeerid', $crestrpeerid);
}

unset($crestrpeerid['crestrpeerid_cachetime']);

if($crestrpeerid)
{
	foreach($crestrpeerid as $peerid)
	{
		if($peerid[1]=='s' && strstr($c_peer_id, $peerid[0]))
		{
			err("Banned User Agent");
		}
		else if($peerid[1]=='i' && stristr($c_peer_id, $peerid[0]))
		{
			err("Banned User Agent");
		}
		else if($peerid[1]=='r' && preg_match("{$peerid[0]}", $c_peer_id))
		{
			err("Banned User Agent");
		}
	}
}
?>
