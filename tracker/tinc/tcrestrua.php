<?php
/**
*
* @package ppkBB3cker
* @version $Id: tcrestrua.php 1.000 2009-11-15 17:20:00 PPK $
* @copyright (c) 2009 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if(!defined('TRACKER_CRESTRICTS_TABLE'))
{
	define('TRACKER_CRESTRICTS_TABLE', $table_prefix . 'tracker_crestricts');
}

$crestrua=t_getcache('tracker_crestrua');

if($crestrua===false)
{
	$crestrua=array();
	include_once("{$tincludedir}tcache.{$phpEx}");

	$sql="SELECT rstart, rtype FROM ".TRACKER_CRESTRICTS_TABLE." WHERE rid='2' AND renabled='1'";
	$result=my_sql_query($sql);
	while($row=mysql_fetch_array($result))
	{
		$crestrua[]=array($row['rstart'], $row['rtype']);
	}
	mysql_free_result($result);

	t_recache('tracker_crestrua', $crestrua);
}

unset($crestrua['crestrua_cachetime']);

if($crestrua)
{
	foreach($crestrua as $ua)
	{
		if($ua[1]=='s' && strstr($agent, $ua[0]))
		{
			err("Banned User Agent");
		}
		else if($ua[1]=='i' && stristr($agent, $ua[0]))
		{
			err("Banned User Agent");
		}
		else if($ua[1]=='r' && preg_match("{$ua[0]}", $agent))
		{
			err("Banned User Agent");
		}
	}
}
?>
