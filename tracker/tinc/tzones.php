<?php
/**
*
* @package ppkBB3cker
* @version $Id: tzones.php 1.000 2009-05-31 15:55:00 PPK $
* @copyright (c) 2008, 2009 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

@define('TRACKER_ZONES_NAME', $table_prefix . 'tracker_zname');
@define('TRACKER_ZONES_ADDR', $table_prefix . 'tracker_zaddr');

if(in_array($config['ppkbb_tczones_enable'][0], array('a', 'w', 'b', 'wb', 'bw')))
{
	$user_zones=get_user_zones($ip, $config['ppkbb_tczones_enable'][0]);
	if(!get_zones_rights($user_zones, $config['ppkbb_tczones_enable'][0]))
	{
		err("Sorry, IP zone restricts, access list: ({$config['ppkbb_tczones_enable'][0]})");
	}
}

function get_user_zones($ip, $type='a', $r=0)
{
	$zones=array();

	if($ip)
	{
		$sql_limit='';

		if($type=='wb' || $type=='bw')
		{
			$zone_type=array('w', 'b');
		}
		else if($type=='w' || $type=='b' || $type=='a')
		{
			$zone_type=array($type);
		}
		if($r)
		{
			$zone_type[]='r';
		}
		else
		{
			$type=='a' || $type=='b' || $type=='w' ? $sql_limit.=' LIMIT 1' : '';
		}

		$sql='SELECT n.id, n.zone_type FROM '.TRACKER_ZONES_ADDR.' a, '.TRACKER_ZONES_NAME." n WHERE INET_ATON('{$ip}') >= INET_ATON(a.addr_start) AND INET_ATON('{$ip}') <= INET_ATON(a.addr_end) AND a.zone_id=n.id AND a.zenabled='1' AND n.zenabled='1' AND n.zone_type IN ('".implode("', '", $zone_type)."'){$sql_limit}";
		$result=my_sql_query($sql);
		while($row=mysql_fetch_array($result))
		{
			$zones[$row['zone_type']]=$row['id'];
		}
		mysql_free_result($result);

	}

	return $zones;
}

function get_zones_rights($zones, $type='a', $r=0)
{
	if($r)
	{
		if(@$zones['r'])
		{
			return false;
		}
	}
	if($type=='w')
	{
		if(@$zones['w'])
		{
			return true;
		}
	}
	else if($type=='b')
	{
		if(@$zones['b'])
		{
			return false;
		}
	}
	else if($type=='wb')
	{
		if(@$zones['w'])
		{
			return true;
		}
		else if(@$zones['b'])
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	else if($type=='bw')
	{
		if(@$zones['b'])
		{
			return false;
		}
		else if(@$zones['w'])
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		if(@$zones['a'])
		{
			return true;
		}
	}

	return false;
}
?>
