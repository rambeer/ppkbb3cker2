<?php
/**
*
* @package ppkBB3cker
* @version $Id: tlog.php 1.000 2018-02-11 12:23:00 PPK $
* @copyright (c) 2018 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$debug_text=array();
$debug_data=debug_backtrace();
foreach($debug_data as $lvl)
{
	foreach($lvl  as $k=>$v)
	{
		if(in_array($k, array('file', 'line', 'function')))
		{
			if($k=='file')
			{
				$root_path = dirname($v);

				$v=str_replace(array($root_path, '\\'), array('[ROOT]', '/'), $v);
			}
			$debug_text[]=($k=='file' ? '<br />' : '').'<strong>'.strtoupper($k).":</strong> {$v}";
		}
	}
}

if(strpos($query, 'info_hash')!==false)
{
	$query=preg_replace_callback(
				"#(info_hash=)'(.*?)'#",
				"info_hash_convert",
			$query);

	$query=preg_replace_callback(
				"#(info_hash IN)\('(.*?)'\)#",
				"info_hash_convert",
			$query);
}

$log_data=mysql_real_escape_string(
serialize(
	array(
		implode("\n", $debug_text),
		$query,
		mysql_errno($c),
		mysql_error($c),
		)
	),
$c);
$userid ? '' : $userid=1;

$sql='INSERT INTO '.LOG_TABLE."(user_id, log_type, log_operation, log_time, log_data, log_ip) VALUES('{$userid}', '2', 'LOG_PPKBBSQL_ERROR', '".time()."', '{$log_data}', '{$_SERVER['REMOTE_ADDR']}')";
my_sql_query($sql, false);

//##############################################################################
function info_hash_convert($matches)
{
	if($matches[1]=='info_hash IN')
	{
		preg_match_all("#'(.*?)'#", $matches[0], $info_hashs);
		if(isset($info_hashs[1]) && $info_hashs[1])
		{
			$hash_array=array();
			foreach($info_hashs[1] as $info_hash)
			{
				$hash_array[]="'".bin2hex($info_hash)."'";
			}
		}

		return $matches[1].'('.implode(', ', $hash_array).')';
	}
	else
	{
		return $matches[1]."'".bin2hex($matches[2])."'";
	}
}
?>
