<?php
/**
*
* @package ppkBB3cker
* @version $Id: tcrestrport.php 1.000 2009-11-15 16:59:00 PPK $
* @copyright (c) 2009 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if(!defined('TRACKER_CRESTRICTS_TABLE'))
{
	define('TRACKER_CRESTRICTS_TABLE', $table_prefix . 'tracker_crestricts');
}

$crestrport=t_getcache('tracker_crestrport');

if($crestrport===false)
{
	$crestrport=array();
	include_once("{$tincludedir}tcache.{$phpEx}");

	$sql="SELECT rstart, rend FROM ".TRACKER_CRESTRICTS_TABLE." WHERE rid='1' AND renabled='1'";
	$result=my_sql_query($sql);
	while($row=mysql_fetch_array($result))
	{
		$crestrport[]=array($row['rstart'], $row['rend']);
	}
	mysql_free_result($result);

	t_recache('tracker_crestrport', $crestrport);
}

unset($crestrport['crestrport_cachetime']);

if($crestrport)
{
	foreach($crestrport as $ports)
	{
		if($port >= $ports[0] && $port <= $ports[1])
		{
			err("Banned port");
		}
	}
}
?>
