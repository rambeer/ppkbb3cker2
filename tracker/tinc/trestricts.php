<?php
/**
*
* @package ppkBB3cker
* @version $Id: trestricts.php 1.000 2009-03-02 19:43:00 PPK $
* @copyright (c) 2009 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$user_ratio=get_ratio($user['user_uploaded'], $user['user_downloaded'], $config['ppkbb_tcratio_start'], $user['user_bonus']);
$trestricts=trestricts_data();

//From includes/functions.php
function get_ratio($up, $down, $skip=0, $bonus=0.000)
{
	global $config;

	$config['ppkbb_tcbonus_value'][0]==-1 || $config['ppkbb_tcbonus_value'][0]==2 ? $bonus=0.000 : '';

	$ratio=0;

	if($skip && $down < $skip)
	{
		$ratio='None.';
	}
	else if(!$up && !$down)
	{
		$ratio='Inf.';
	}
	else if(!$up && $down)
	{
		$ratio='Leech.';
	}
	else if(!$down && $up)
	{
		$ratio='Seed.';
	}
	else
	{
		$ratio=number_format($up / $down, 3, '.', '');
	}
	if($bonus!=0.000 && !in_array($ratio, array('Inf.', 'Seed.', 'Leech.', 'None.')))
	{
		settype($bonus, 'float');
		$ratio=number_format($ratio + $bonus, 3, '.', '');
	}

	return $ratio;
}

//From includes/functions.php
function trestricts_data()
{
	global $tincludedir, $phpEx;

	$trestricts=t_getcache('trestricts');
	if($trestricts===false)
	{
		$trestricts=array('ppkbb_tcwait_time'=>array(), 'ppkbb_tcwait_time2'=>array(), 'ppkbb_tcmaxleech_restr'=>array());

		if(!array_sum($config['ppkbb_tctracker_restricts']))
		{
			return $trestricts;
		}

		$sql='SELECT * FROM '.TRACKER_TRESTRICTS_TABLE." WHERE restrict_enabled='1' ORDER BY restrict_type, restrict_order";
		$result=my_sql_query($sql);
		while($row=mysql_fetch_array($result))
		{
			if(($row['restrict_type']=='ppkbb_tcwait_time' && $config['ppkbb_tctracker_restricts'][0])
				|| ($row['restrict_type']=='ppkbb_tcwait_time2' && $config['ppkbb_tctracker_restricts'][1])
				|| ($row['restrict_type']=='ppkbb_tcmaxleech_restr' && $config['ppkbb_tctracker_restricts'][2])
				)
			{
				$trestricts[$row['restrict_type']][]=array('user_ratio'=>$row['user_ratio'], 'user_updown'=>$row['user_updown'], 'time_count'=>$row['time_count']);
			}
		}
		mysql_free_result($result);
		include_once("{$tincludedir}tcache.{$phpEx}");

		t_recache('trestricts', $trestricts);
	}

	unset($trestricts['trestricts_cachetime']);

	return $trestricts;

}

//From includes/functions.php
function get_trestricts($uploaded, $downloaded, $ratio, $restrict_data, $t='down')
{
	if($restrict_data)
	{
		foreach($restrict_data as $data)
		{

			$ratios=isset($data['user_ratio']) ? $data['user_ratio'] : '';
			$updown=isset($data['user_updown']) ? $data['user_updown'] : 0;
			$timetorrent=isset($data['time_count']) ? $data['time_count'] : 0;
			if(in_array($ratio, array('Inf.', 'Leech.', 'Seed.', 'None.')))
			{
				if($ratios==$ratio)
				{
					if($t=='up')
					{
						if($updown && $uploaded < $updown)
						{
							return $timetorrent;
						}
						else
						{
							return $timetorrent;
						}
					}
					else
					{
						if($updown && $downloaded > $updown)
						{
							return $timetorrent;
						}
						else
						{
							return $timetorrent;
						}
					}
				}
			}
			else
			{
				if(!in_array($ratios, array('Inf.', 'Leech.', 'Seed.', 'None.')))
				{
					$ratios=my_float_val($ratios);

					if($t=='up')
					{
						if($ratios && !$updown)
						{
							if($ratio < $ratios)
							{
								return $timetorrent;
							}
						}
						else if(!$ratios && $updown)
						{
							if($uploaded < $updown)
							{
								return $timetorrent;
							}
						}
						else
						{
							if($ratio < $ratios && $uploaded < $updown)
							{
								return $timetorrent;
							}
						}
					}
					else
					{
						if($ratios && !$updown)
						{
							if($ratio < $ratios)
							{
								return $timetorrent;
							}
						}
						else if(!$ratios && $updown)
						{
							if($downloaded > $updown)
							{
								return $timetorrent;
							}
						}
						else
						{
							if($ratio < $ratios && $downloaded > $updown)
							{
								return $timetorrent;
							}
						}
					}
				}
			}
		}
	}

	return -1;
}
?>
