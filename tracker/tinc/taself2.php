<?php
/**
*
* @package ppkBB3cker
* @version $Id: taself2.php 1.000 2009-02-13 12:02:00 PPK $
* @copyright (c) 2008 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$connectable=0;
if($config['ppkbb_tcignore_connectable']!=1)
{
	$sockres = @fsockopen($ip, $port, $errno, $errstr, 5);
	if ($sockres)
	{
		$connectable=1;
		@fclose($sockres);
	}
}

if($config['ppkbb_tcenable_snatchstat'])
{
	if($user['s_id']==NULL)
	{
		$result=my_sql_query("INSERT INTO ". TRACKER_SNATCHED_TABLE ." (torrent, to_go, userid, startdat, last_action, uploadoffset, downloadoffset, uploaded, downloaded".(sizeof($snatch_add) ? ', '.implode(', ', $snatch_add['k']) : '').") VALUES ('{$torrentid}', '{$left}', '{$userid}', '{$dt}', '{$dt}', '{$upthis}', '{$downthis}', '{$upthis}', '{$downthis}'".(sizeof($snatch_add) ? ", '".implode("', '", $snatch_add['v'])."'" : '').")");
	}
	else
	{
		$result=my_sql_query("UPDATE ". TRACKER_SNATCHED_TABLE ." SET last_action='{$dt}', prev_action='{$attachment['last_action']}'".(sizeof($updatesnatch) ? ', '.implode(', ', $updatesnatch) : '')." WHERE id='{$user['s_id']}'");//torrent='{$torrentid}' AND userid='{$userid}'".($config['ppkbb_tcguests_enabled'][0] ? (IS_GUESTS ? " AND guests='1'" : " AND guests='0'") : '')
	}
}
$result = my_sql_query("UPDATE ". TRACKER_PEERS_TABLE ." SET ip='{$ip}', port='{$port}', last_action='{$dt}', seeder='{$seeder}', agent='{$agent}'".(sizeof($updatepeers) ? ', '.implode(', ', $updatepeers) : '').' WHERE '.($config['ppkbb_tcmultiple_announce'] ? "id={$attachment['id']}" : str_replace('tp.', '', $selfwhere)));

if(mysql_affected_rows($c) && $attachment['seeder']!=$seeder)
{
	if($seeder)
	{
		$updatetorr[]="seeders=seeders+1";
		$updatetorr[]="leechers='".my_int_val($user['leechers']-1)."'";
	}
	else
	{
		$updatetorr[]="leechers=leechers+1";
		$updatetorr[]="seeders='".my_int_val($user['seeders']-1)."'";
	}
}

?>
